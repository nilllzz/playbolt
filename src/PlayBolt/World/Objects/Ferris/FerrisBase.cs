﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Ferris
{
    class FerrisBase : WorldObject
    {
        public FerrisBase(WorldScreen screen, Vector3 position)
            : base(screen, "ferris_base", position)
        { }

        protected override void CreateGeometryInternal()
        {
            // base
            {
                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                    new TextureRectangle(new Rectangle(0, 64, 80, 8), Texture));
                texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(0, 80, 80, 32), Texture));
                texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                    new TextureRectangle(new Rectangle(0, 72, 32, 8), Texture));
                var vertices = CuboidComposer.Create(40f, 2f, 16f, texture);
                Geometry.AddVertices(vertices);
            }
            // bars
            {
                ITextureDefintion getTexture()
                {
                    var texture = new TextureCuboidWrapper();
                    texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back, CuboidSide.Top, CuboidSide.Bottom },
                        new TextureRectangle(new Rectangle(0, 112, 112, 8), Texture));
                    texture.AddSide(new[] { CuboidSide.Left },
                        new TextureRectangle(new Rectangle(0, 112, 1, 1), Texture));
                    texture.AddSide(new[] { CuboidSide.Right },
                        new TextureRectangle(new Rectangle(111, 112, 1, 1), Texture));
                    return texture;
                }

                // bar1
                {
                    var vertices = CuboidComposer.Create(40f, 2f, 2f, getTexture());
                    VertexTransformer.Rotate(vertices, new Vector3(0, 0, MathHelper.PiOver4 * 1.5f));
                    VertexTransformer.Offset(vertices, new Vector3(-10, 19, 6));
                    Geometry.AddVertices(vertices);
                }
                // bar2
                {
                    var vertices = CuboidComposer.Create(40f, 2f, 2f, getTexture());
                    VertexTransformer.Rotate(vertices, new Vector3(0, 0, MathHelper.PiOver4 * 2.5f));
                    VertexTransformer.Offset(vertices, new Vector3(10, 19, 6));
                    Geometry.AddVertices(vertices);
                }
                // bar3
                {
                    var vertices = CuboidComposer.Create(40f, 2f, 2f, getTexture());
                    VertexTransformer.Rotate(vertices, new Vector3(0, 0, MathHelper.PiOver4 * 2.5f));
                    VertexTransformer.Offset(vertices, new Vector3(10, 19, -6));
                    Geometry.AddVertices(vertices);
                }
                // bar4
                {
                    var vertices = CuboidComposer.Create(40f, 2f, 2f, getTexture());
                    VertexTransformer.Rotate(vertices, new Vector3(0, 0, MathHelper.PiOver4 * 1.5f));
                    VertexTransformer.Offset(vertices, new Vector3(-10, 19, -6));
                    Geometry.AddVertices(vertices);
                }
            }
            // middle part
            {
                var vertices = CylinderComposer.Create(3f, 15f, 20,
                    new TextureRectangle(new Rectangle(0, 120, 1, 1), Texture),
                    new TextureRectangle(new Rectangle(0, 120, 32, 32), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(0, MathHelper.PiOver2, 0));
                VertexTransformer.Offset(vertices, new Vector3(0, 38, 0));
                Geometry.AddVertices(vertices);
            }
        }
    }
}
