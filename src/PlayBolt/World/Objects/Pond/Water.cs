﻿using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Pond
{
    class Water : WorldObject
    {
        private float _animationState = 0f;
        private Vector3 _orgPosition;

        public Water(WorldScreen screen, Vector3 position) 
            : base(screen, "pond_water", position)
        {
            IsOpaque = false;
            IsOptimizable = false;
            Alpha = 0.5f;
            _orgPosition = position;
        }

        public override void LoadContent()
        {
            LoadTexture("Textures/World/water.png");
        }

        public override void Update()
        {
            if (_cameraDistance <= 100)
            {
                _animationState += 0.01f;
                if (_animationState >= 1f)
                    _animationState -= 1f;

                _position = _orgPosition + new Vector3(1, 0, 1) * _animationState;
                CreateWorld();
            }

            base.Update();
        }

        protected override void CreateGeometryInternal()
        {
            var vertices = RectangleComposer.Create(100, 80,
                new TextureRectangle(0, 0, 100, 80));
            Geometry.AddVertices(vertices);
        }
    }
}
