﻿using GameDevCommon.Drawing;
using GameDevCommon.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PlayBolt.Content;
using PlayBolt.Scenes.InGame;
using System;
using static Core;

namespace PlayBolt.Scenes.Menu
{
    abstract class StartGameScreen : Screen
    {
        const int MAX_INTRO = (GameController.RENDER_WIDTH / 32) + (GameController.RENDER_HEIGHT / 32);

        protected Screen _preScreen;
        private Texture2D _texture;
        private SpriteBatch _batch;
        private SpriteFont _font;
        private int _backgroundIntro = 0;
        private bool _outro = false;
        private int _selection = 0;
        private float _selectionIntro = 0f;
        
        private string _title, _subTitle;
        protected MicrogameScores _highscoreData;

        protected StartGameScreen(Screen preScreen, string title, string subTitle)
        {
            _preScreen = preScreen;
            _title = title;
            _subTitle = subTitle;
            GetScores();
        }

        protected abstract void GetScores();

        internal override void LoadContent()
        {
            _texture = GetComponent<Resources>().LoadTexture("Textures/MenuTiles.png");
            _batch = new SpriteBatch(Controller.GraphicsDevice);
            _font = Controller.Content.Load<SpriteFont>("Fonts/Font");
        }

        internal override void UnloadContent()
        {
            _texture = null;
            _font = null;
            _batch.Dispose();
        }

        internal override void Draw(GameTime gameTime)
        {
            _preScreen.Draw(gameTime);

            _batch.Begin(SpriteBatchUsage.Default);

            var alpha = _backgroundIntro / (float)MAX_INTRO;
            for (int x = 0; x < GameController.RENDER_WIDTH; x += 32)
            {
                for (int y = 0; y < GameController.RENDER_HEIGHT; y += 32)
                {
                    var i = x / 32 + y / 32;
                    if (i < _backgroundIntro)
                        _batch.Draw(_texture, new Rectangle(x, y, 32, 32), new Rectangle(96, 112, 16, 16), new Color(255, 255, 255, (int)(255 * alpha)));
                }
            }

            const int startY = 70;

            var sizeName = _font.MeasureString(_title) * 4f;
            var sizePlaylist = _font.MeasureString(_subTitle) * 2f;
            _batch.DrawRectangle(new Rectangle(220, startY, (int)sizeName.X + 20, (int)sizeName.Y + 20),
                new Color(255, 255, 255, (int)(200 * alpha)));
            _batch.DrawString(_font, _title, new Vector2(230, startY + 10),
                new Color(0, 0, 0, (int)(255 * alpha)), 0f, Vector2.Zero, 4f, SpriteEffects.None, 0f);
            _batch.DrawRectangle(new Rectangle(220, (int)sizeName.Y + startY + 20, (int)sizePlaylist.X + 20, (int)sizePlaylist.Y + 20),
                new Color(255, 255, 255, (int)(200 * alpha)));
            _batch.DrawString(_font, _subTitle, new Vector2(230, (int)sizeName.Y + startY + 30),
                new Color(0, 0, 0, (int)(255 * alpha)), 0f, Vector2.Zero, 2f, SpriteEffects.None, 0f);

            var buttons = new[] { "Go!", "Bail" };
            for (int i = 0; i < buttons.Length; i++)
            {
                var up = (int)(sizeName.Y + startY + sizePlaylist.Y + 90) + i * 120;
                var c = new Color(255, 255, 255, (int)(200 * alpha));
                if (i == _selection)
                {
                    c = new Color(204, 255, 0, (int)(200 * alpha));
                    var checkeredEnd = (int)(_selectionIntro * 288);
                    for (int x = 0; x < checkeredEnd; x += 32)
                    {
                        for (int y = 0; y < 96; y += 32)
                        {
                            _batch.Draw(_texture, new Rectangle(x + 220, y + up, 32, 32), new Rectangle(112, 96, 16, 16),
                                new Color(255, 255, 255, (int)(80 * alpha)));
                        }
                    }
                    var arrowEnd = (int)(_selectionIntro * 35) + 225;
                    _batch.DrawLine(new Vector2(225, up), new Vector2(arrowEnd, up + 50), c, 5);
                    _batch.DrawLine(new Vector2(arrowEnd, up + 50), new Vector2(225, up + 96), c, 5);
                }
                _batch.DrawRectangle(new Rectangle(220, up, 288, 4), c);
                _batch.DrawRectangle(new Rectangle(220, up + 96, 288, 4), c);
                _batch.DrawRectangle(new Rectangle(220, up, 4, 100), c);
                _batch.DrawRectangle(new Rectangle(504, up, 4, 100), c);

                _batch.DrawString(_font, buttons[i], new Vector2(300, up - 8),
                    c, 0f, Vector2.Zero, 5f, SpriteEffects.None, 0f);
            }

            int startX = (int)(Math.Max(sizeName.X, sizePlaylist.X) + 300);
            if (startX < 550)
                startX = 550;

            _batch.DrawRectangle(new Rectangle(startX, startY, 300, 200), new Color(255, 255, 255, (int)(200 * alpha)));
            _batch.DrawString(_font, "Highscores", new Vector2(startX + 10, startY + 10), new Color(0, 0, 0, (int)(200 * alpha)),
                0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);

            if (_highscoreData.Highscores != null)
            {
                for (int s = 0; s < _highscoreData.Highscores.Length; s++)
                {
                    var score = _highscoreData.Highscores[s];
                    switch (s)
                    {
                        case 0:
                            _batch.Draw(_texture, new Rectangle(startX + 10, startY + s * 40 + 60, 34, 35),
                                new Rectangle(0, 66, 34, 35), new Color(255, 255, 255, (int)(200 * alpha)));
                            break;
                        case 1:
                            _batch.Draw(_texture, new Rectangle(startX + 10, startY + s * 40 + 60, 34, 35),
                                new Rectangle(34, 66, 34, 35), new Color(255, 255, 255, (int)(200 * alpha)));
                            break;
                        case 2:
                            _batch.Draw(_texture, new Rectangle(startX + 10, startY + s * 40 + 60, 34, 35),
                                new Rectangle(68, 66, 34, 35), new Color(255, 255, 255, (int)(200 * alpha)));
                            break;
                    }
                    score.Draw(gameTime, _batch, _font, _texture,
                        new Vector2(startX + 74, startY + s * 40 + 66), Color.Black, (int)(255 * alpha));
                }
            }

            _batch.DrawRectangle(new Rectangle(startX, startY + 250, 300, 100), new Color(255, 255, 255, (int)(200 * alpha)));
            _batch.DrawString(_font, "Personal best", new Vector2(startX + 10, startY + 260), new Color(0, 0, 0, (int)(200 * alpha)),
                0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);

            _highscoreData.OwnHighscore.Draw(gameTime, _batch, _font, _texture,
                new Vector2(startX + 74, startY + 310), Color.Black, (int)(255 * alpha));

            if (!_highscoreData.OwnHighscore.IsEmpty)
            {
                _batch.DrawString(_font, _highscoreData.OwnRank.ToString(), new Vector2(startX + 20, startY + 310),
                    new Color(0, 0, 0, (int)(255 * alpha)));
            }

            _batch.End();
        }

        internal override void Update(GameTime gameTime)
        {
            if (!_outro)
            {
                if (_backgroundIntro < MAX_INTRO)
                {
                    _backgroundIntro += 2;
                    if (_backgroundIntro >= MAX_INTRO)
                        _backgroundIntro = MAX_INTRO;
                }
                else
                {
                    if (_selectionIntro < 1f)
                    {
                        _selectionIntro = MathHelper.Lerp(_selectionIntro, 1f, 0.2f);
                        if (_selectionIntro > 0.999f)
                            _selectionIntro = 1f;
                    }
                }
            }
            else
            {
                _backgroundIntro -= 2;
                if (_backgroundIntro <= 0)
                {
                    UnloadContent();
                    GetComponent<ScreenManager>().SetScreen(_preScreen);
                }
                else
                {
                    _preScreen.Update(gameTime);
                }
            }


            var controls = GetComponent<ControlsHandler>();
            if (controls.DownPressed(PlayerIndex.One))
            {
                _selectionIntro = 0f;
                _selection++;
                if (_selection == 2)
                    _selection = 0;
            }
            if (controls.UpPressed(PlayerIndex.One))
            {
                _selectionIntro = 0f;
                _selection--;
                if (_selection == -1)
                    _selection = 1;
            }
            var keyboard = GetComponent<KeyboardHandler>();
            var gamepad = GetComponent<GamePadHandler>();
            if (keyboard.KeyPressed(Keys.Escape) || keyboard.KeyPressed(Keys.E) ||
                gamepad.ButtonPressed(PlayerIndex.One, Buttons.B))
            {
                _outro = true;
            }
            if (keyboard.KeyPressed(Keys.Enter) || keyboard.KeyPressed(Keys.Space) ||
                gamepad.ButtonPressed(PlayerIndex.One, Buttons.A))
            {
                switch (_selection)
                {
                    case 0:
                        StartGame();
                        break;
                    case 1:
                        _outro = true;
                        break;
                }
            }
        }

        protected abstract void StartGame();
    }
}
