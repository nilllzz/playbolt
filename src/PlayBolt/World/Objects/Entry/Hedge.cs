﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Entry
{
    class Hedge : WorldObject
    {
        private int _size;
        private bool _rotated;

        public Hedge(WorldScreen screen, Vector3 position, bool rotated, int size)
            : base(screen, "entry_hedge", position)
        {
            _rotated = rotated;
            _size = size;
        }

        public override void LoadContent()
        {
            LoadTexture("Textures/World/hedge.png");
        }

        protected override void CreateGeometry()
        {
            var texture = new TextureCuboidWrapper();
            texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                new TextureRectangle(new Rectangle(0, 0, 32 * _size / 2, 32), Texture));
            texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                new TextureRectangle(new Rectangle(0, 0, 32, 32), Texture));
            texture.AddSide(new[] { CuboidSide.Bottom, CuboidSide.Top },
                new TextureRectangle(new Rectangle(0, 32, 32 * _size / 2, 32), Texture));
            var vertices = CuboidComposer.Create(_size, 2f, 2f, texture);
            if (_rotated)
            {
                VertexTransformer.Rotate(vertices, new Vector3(0, MathHelper.PiOver2, 0));
            }
            Geometry.AddVertices(vertices);
        }
    }
}
