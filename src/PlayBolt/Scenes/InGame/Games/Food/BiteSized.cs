﻿using GameDevCommon.Drawing;
using GameDevCommon.Input;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PlayBolt.Scenes.InGame.Games.Food
{
    [MicrogameDescription(3, "Bite sized", "Food", "Bite chunks out of the sandwich until it's gone!")]
    class BiteSized : Microgame
    {
        private class Particle
        {
            public Rectangle Source;
            public float Life;
            public Vector2 Position;
            public Vector2 Velocity;
        }

        public override float CompletionTime => 5f;
        public override string Command => "Munch!";
        public override string Scoretable => "279067";
        public override int TargetScore => 20;
        public override string Thumbnail => "3.png";

        private int _size;
        private int _cursorX, _cursorY;
        private List<Point> _bites = new List<Point>();
        private bool[][] _fields;
        private bool _teethClosing = false;
        private float _teethAnimation = 0f;
        private List<Particle> _particles = new List<Particle>();

        public override void GameStart()
        {
            _size = (int)Difficulty + 1;
            _cursorX = WIDTH / 2 + _random.Next(-200, 200);
            _cursorY = HEIGHT / 2 + _random.Next(-100, 100);
            _fields = new bool[_size * 96 + 128][];
            for (var i = 0; i < _fields.Length; i++)
                _fields[i] = new bool[128];
        }

        public override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteBatchUsage.Default);

            _batch.DrawRectangle(new Rectangle(0, 0, WIDTH, HEIGHT), new Color(226, 238, 238));

            for (var x = 0; x < _size; x++)
            {
                _batch.Draw(_texture, new Rectangle((int)(WIDTH / 2 - _size / 2f * 96 + x * 96), 200, 96, 128),
                    new Rectangle(32, 0, 48, 64), Color.White);
            }
            _batch.Draw(_texture, new Rectangle((int)(WIDTH / 2 - _size / 2f * 96 - 64), 200, 64, 128),
                new Rectangle(0, 0, 32, 64), Color.White);
            _batch.Draw(_texture, new Rectangle((int)(WIDTH / 2 + _size / 2f * 96), 200, 64, 128),
                new Rectangle(80, 0, 32, 64), Color.White);

            foreach (var bite in _bites)
            {
                _batch.DrawRectangle(new Rectangle(bite.X, bite.Y, 128, 128), new Color(226, 238, 238));
            }

            foreach (var particle in _particles)
            {
                _batch.Draw(_texture, new Rectangle((int)particle.Position.X, (int)particle.Position.Y, 6, 6), particle.Source, Color.White);
            }

            _batch.Draw(_texture, new Rectangle(_cursorX, (int)(_cursorY + 32 * _teethAnimation), 128, 64),
                new Rectangle(16, 64, 32, 16), new Color(255, 255, 255, 180));
            _batch.Draw(_texture, new Rectangle(_cursorX, (int)(_cursorY + 64 - 32 * _teethAnimation), 128, 64),
                new Rectangle(16, 80, 32, 16), new Color(255, 255, 255, 180));

            _batch.End();
        }

        public override void Update(GameTime gameTime)
        {
            if (_teethAnimation == 0f && !_teethClosing && ActionPressed())
            {
                var bittenBefore = _fields.Sum(farr => farr.Count(b => b));

                _bites.Add(new Point(_cursorX, _cursorY));
                var x = _cursorX - (int)(WIDTH / 2 - _size / 2f * 96 - 64);
                var y = _cursorY - 200;
                for (var ix = 0; ix < 128; ix++)
                {
                    for (var iy = 0; iy < 128; iy++)
                    {
                        var dx = x + ix;
                        var dy = y + iy;
                        if (dx >= 0 && dy >= 0 && dx < _fields.Length && dy < 128)
                        {
                            _fields[dx][dy] = true;
                        }
                    }
                }
                var bitten = _fields.Sum(farr => farr.Count(b => b));
                if ((float)bitten / (_fields.Length * 128) >= 0.9f)
                {
                    WinState = WinState.Won;
                }

                _teethClosing = true;

                SpawnParticles(bitten - bittenBefore);
            }

            if (_teethClosing && _teethAnimation < 1f)
            {
                _teethAnimation += 0.25f * Speed;
                if (_teethAnimation >= 1f)
                {
                    _teethAnimation = 1f;
                    _teethClosing = false;
                }
            }
            else if (_teethAnimation > 0f)
            {
                _teethAnimation -= 0.25f * Speed;
                if (_teethAnimation <= 0f)
                {
                    _teethAnimation = 0f;
                }
            }

            var speed = (int)(10 * Speed);
            if (DirectionDown(InputDirection.Down))
            {
                _cursorY += speed;
            }
            if (DirectionDown(InputDirection.Up))
            {
                _cursorY -= speed;
            }
            if (DirectionDown(InputDirection.Right))
            {
                _cursorX += speed;
            }
            if (DirectionDown(InputDirection.Left))
            {
                _cursorX -= speed;
            }

            for (var i = 0; i < _particles.Count; i++)
            {
                _particles[i].Position += _particles[i].Velocity * Speed;
                _particles[i].Life -= 0.05f * Speed;
                _particles[i].Velocity.Y += 0.1f;
            }
        }

        private void SpawnParticles(int bites)
        {
            var particleAmount = (int)Math.Floor(bites / 1000f);
            if (bites > 0 && particleAmount == 0)
            {
                particleAmount = 1;
            }

            for (var i = 0; i < particleAmount; i++)
            {
                _particles.Add(new Particle
                {
                    Life = 1,
                    Position = new Vector2(_cursorX + _random.Next(0, 128), _cursorY + _random.Next(0, 128)),
                    Velocity = new Vector2(((float)_random.NextDouble() - 0.5f) * 2, -(float)_random.NextDouble() - 0.5f),
                    Source = new Rectangle(16 + (int)(_random.NextDouble() * 80), 8 + (int)(_random.NextDouble() * 48), 4, 4)
                });
            }
        }

        public override void TimeUp()
        {
            if (WinState == WinState.Default)
                WinState = WinState.Lost;
        }
    }
}
