﻿using GameDevCommon.Drawing;
using GameDevCommon.Input;
using GameDevCommon.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PlayBolt.Content;
using PlayBolt.World;
using System.Linq;
using static Core;

namespace PlayBolt.Scenes.Park
{
    class ParkScreen : WorldScreen
    {
        private readonly ParkCamera _camera;
        private WorldShader _shader;
        private ParkConfig _park;

        public override PerspectiveCamera Camera => _camera;
        public override Shader Shader => _shader;

        public ParkScreen()
        {
            _camera = new ParkCamera();
        }

        internal override void LoadContent()
        {
            base.LoadContent();

            _shader = new WorldShader();
            _park = new ParkConfig();
            LoadLevel();
        }

        private void LoadLevel()
        {
            _objects = ParkConstructor.GetPark(this, _park);
            _objects.ForEach(o => o.LoadContent());
            ((OptimizableRenderObjectCollection)_objects).Optmimize<VertexPositionNormalTexture>();

            var l1 = new PointLight(new Vector3(0, 6, 60), new Color(240, 200, 200), 20f, 1f);
            _shader.AddPointLight(l1);
        }

        internal override void Draw(GameTime gameTime)
        {
            Controller.GraphicsDevice.ResetFull();
            Controller.GraphicsDevice.ClearFull(new Color(20, 0, 80));

            _shader.Prepare(Camera);

            Controller.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            for (var i = 0; i < _objects.OpaqueObjects.Count(); i++)
                _shader.Render(_objects.OpaqueObjects.ElementAt(i));

            Controller.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;
            for (var i = 0; i < _objects.TransparentObjects.Count(); i++)
                _shader.Render(_objects.TransparentObjects.ElementAt(i));
        }

        internal override void Update(GameTime gameTime)
        {
            Camera.Update();

            _objects.Sort();

            var objs = (OptimizableRenderObjectCollection)_objects;
            for (var i = 0; i < objs.OriginalObjects.Count(); i++)
                objs.OriginalObjects.ElementAt(i).Update();

            var keyboard = GetComponent<KeyboardHandler>();
            if (keyboard.KeyPressed(Keys.R))
            {
                GetComponent<Resources>().ClearBuffers();
                LoadLevel();
            }
        }
    }
}
