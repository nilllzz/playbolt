﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using static JoltNet.RequestProvider.Scores.ScoresResponse;

namespace PlayBolt.Scenes.InGame
{
    struct Highscore
    {
        public int Score;
        public string User;
        public bool IsNew;

        public Highscore(DateTime serverTime, ScoreData scoreData)
        {
            var scoreTime = Extensions.GetUtcFromUnixTimestamp(Convert.ToInt32(scoreData.ExtraData));
            Score = scoreData.Sort;
            User = scoreData.User;
            IsNew = (serverTime - scoreTime).TotalHours < 24;
        }

        public bool IsEmpty => Score == 0 || string.IsNullOrEmpty(User);

        public void Draw(GameTime gameTime, SpriteBatch batch, SpriteFont font, Texture2D menuTexture, Vector2 position, Color textColor, int alpha)
        {
            if (!IsEmpty)
            {
                batch.DrawString(font, Score.ToString("D3"), position, new Color(textColor.R, textColor.G, textColor.B, alpha));
                batch.DrawString(font, User.ToString(), position + new Vector2(48, 0), new Color(textColor.R, textColor.G, textColor.B, alpha));
                if (IsNew)
                {
                    batch.Draw(menuTexture,
                        new Rectangle((int)(position.X + 16), (int)(position.Y - 3 - ((gameTime.TotalGameTime.Milliseconds / 500) % 2)), 22, 9),
                        new Rectangle(32, 0, 22, 9),
                        new Color(255, 255, 255, alpha));
                }
            }
            else
            {
                batch.DrawString(font, "- - -    - - -", position, new Color(textColor.R, textColor.G, textColor.B, alpha));
            }
        }
    }
}
