﻿using GameDevCommon.Drawing;
using JoltNet;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using static Core;

namespace PlayBolt.Scenes.InGame
{
    class TrophyManager : IGameComponent
    {
        #region TrophyIDs

        public const int STAREDOWN_ID = 81338;
        public const int PARTICIPATION_ID = 81381;
        public const int AWARD_ID = 81382;
        public const int RIBBON_ID = 81384;
        public const int TROPHY_ID = 81385;
        public const int HIGHESTHONOR_ID = 81386;

        #endregion

        private struct AchievedTrophy
        {
            public int Delay;
            public string Title;
            public string Description;
            public Texture2D Image;
        }

        private SpriteBatch _batch;
        private SpriteFont _font;
        private List<AchievedTrophy> _achievedTrophies = new List<AchievedTrophy>();

        public void Initialize()
        { }

        public void LoadContent()
        {
            _batch = new SpriteBatch(Controller.GraphicsDevice);
            _font = Controller.Content.Load<SpriteFont>("Fonts/Font");
        }
        
        public void Draw(GameTime gameTime)
        {
            if (_achievedTrophies.Count > 0)
            {
                _batch.Begin(SpriteBatchUsage.Default);
                for (int i = 0; i < _achievedTrophies.Count; i++)
                {
                    var trophy = _achievedTrophies[i];
                    var startY = i * 100;
                    var alpha = 255;
                    if (trophy.Delay <= 100)
                        alpha = (int)(alpha * (trophy.Delay / 100f));

                    _batch.DrawRectangle(new Rectangle(GameController.RENDER_WIDTH - 360, startY, 360, 96), new Color(25, 25, 25, alpha));
                    _batch.DrawRectangle(new Rectangle(GameController.RENDER_WIDTH - 352, 8 + startY, 79, 79), new Color(0, 0, 0, alpha));
                    _batch.Draw(trophy.Image, new Rectangle(GameController.RENDER_WIDTH - 350, 10 + startY, 75, 75), new Color(255, 255, 255, alpha));

                    _batch.DrawString(_font, trophy.Title, new Vector2(GameController.RENDER_WIDTH - 260, 10 + startY), new Color(255, 255, 255, alpha),
                        0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                    var desc = trophy.Description;
                    if (desc.Length > 40)
                    {
                        desc = desc.Substring(0, 40) + Environment.NewLine + desc.Substring(40);
                    }
                    _batch.DrawString(_font, desc, new Vector2(GameController.RENDER_WIDTH - 260, 40 + startY), new Color(255, 255, 255, alpha),
                        0f, Vector2.Zero, 0.8f, SpriteEffects.None, 0f);
                }
                _batch.End();
            }
        }

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < _achievedTrophies.Count; i++)
            {
                var trophy = _achievedTrophies[i];
                trophy.Delay--;
                if (trophy.Delay <= 0)
                {
                    _achievedTrophies.RemoveAt(i);
                }
                else
                {
                    _achievedTrophies[i] = trophy;
                }
            }
        }

        private void AddAchievedTrophy(RequestProvider.Trophies.TrophiesResponse.TrophyData trophyData)
        {
            var request = WebRequest.CreateHttp(trophyData.ImageUrl);
            var response = request.GetResponse();
            var responseStream = response.GetResponseStream();

            var memoryStream = new MemoryStream();
            responseStream.CopyTo(memoryStream);
            memoryStream.Position = 0;
            
            var texture = Texture2D.FromStream(Controller.GraphicsDevice, memoryStream);
            _achievedTrophies.Add(new AchievedTrophy
            {
                Delay = 300,
                Title = trophyData.Title,
                Description = trophyData.Description,
                Image = texture
            });
        }

        public void UnlockTrophy(int id)
        {
            Task.Run(async () =>
            {
                var fetchTrophy = RequestProvider.Trophies.Fetch(id, Controller.GameJoltUsername, Controller.GameJoltToken);
                await Controller.GameJoltClient.ExecuteRequestAsync(fetchTrophy);
                if (!fetchTrophy.Response.Trophies[0].IsAchieved)
                {
                    var achieveTrophy = RequestProvider.Trophies.SetAchieved(id, Controller.GameJoltUsername, Controller.GameJoltToken);
                    await Controller.GameJoltClient.ExecuteRequestAsync(achieveTrophy);
                    AddAchievedTrophy(fetchTrophy.Response.Trophies[0]);
                }
            });
        }
    }
}
