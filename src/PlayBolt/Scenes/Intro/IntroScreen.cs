﻿using GameDevCommon.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayBolt.Content;
using PlayBolt.Scenes.Start;
using System;
using System.Threading.Tasks;
using static Core;

namespace PlayBolt.Scenes.Intro
{
    class IntroScreen : Screen
    {
        private const int LOGO_FRAMES = 35;
        private Texture2D[] _frames = new Texture2D[LOGO_FRAMES];
        private int _currentFrame = 0;
        private bool _loaded = false;
        private int _frameDelay = 50;
        private float _fontIntro = 0f;
        private SpriteBatch _batch;
        private SpriteFont _font;
        private Random _random = new Random();
        private int _outro = 50;
        private StartScreen _startScreen;

        internal override void LoadContent()
        {
            for (int i = 0; i < LOGO_FRAMES; i++)
            {
                _frames[i] = GetComponent<Resources>().LoadTexture($"Intro/{i}.png");
            }

            _batch = new SpriteBatch(Controller.GraphicsDevice);
            _font = Controller.Content.Load<SpriteFont>("Fonts/Font");

            Task.Run(() => Load());
        }

        internal override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteBatchUsage.Default);

            _batch.DrawRectangle(new Rectangle(0, 0, GameController.RENDER_WIDTH, GameController.RENDER_HEIGHT), Color.Black);

            var frame = _frames[_currentFrame];
            var color = Color.White;
            if (_currentFrame == 0)
            {
                var alpha = 1f - (_frameDelay / 50f);
                color = new Color(255, 255, 255, (int)(255 * alpha));
            }
            _batch.Draw(frame, new Vector2(GameController.RENDER_WIDTH / 2 - frame.Width / 2,
                GameController.RENDER_HEIGHT / 2 - frame.Height / 2 - 100), color);

            if (_currentFrame > 10)
            {
                var text = "I made this game, don't steal";
                var textSize = _font.MeasureString(text) * 4;
                _batch.DrawString(_font, text,
                    new Vector2(GameController.RENDER_WIDTH / 2 - textSize.X / 2, 500 + (int)(50 * _fontIntro)) + textSize / 2f,
                    new Color(255, 255, 255, (int)(255 * _fontIntro)),
                    _random.Next(-20, 21) / 2500f, textSize / 8f, 4f, SpriteEffects.None, 0f);
            }

            _batch.End();
        }

        internal override void Update(GameTime gameTime)
        {
            if (_currentFrame < LOGO_FRAMES - 1)
            {
                _frameDelay--;
                if (_frameDelay == 0)
                {
                    _frameDelay = 4 + (_currentFrame / 10);
                    _currentFrame++;
                }
            }
            if (_currentFrame > 10)
            {
                if (_fontIntro < 1f)
                {
                    _fontIntro = MathHelper.Lerp(_fontIntro, 1f, 0.05f);
                    if (_fontIntro >= 0.9999f)
                    {
                        _fontIntro = 1f;
                    }
                }
                else
                {
                    _outro--;
                    if (_outro <= 0 && _loaded)
                    {
                        var transitionScreen = new FadeTransitionScreen(this, _startScreen);
                        transitionScreen.LoadContent();
                        GetComponent<ScreenManager>().SetScreen(transitionScreen);
                    }
                }
            }
        }

        private void Load()
        {
            _startScreen = new StartScreen();
            _startScreen.LoadContent();
            _loaded = true;
        }
    }
}
