﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Entry
{
    class Fence : WorldObject
    {
        private int _size;
        private bool _rotated;

        public Fence(WorldScreen screen, Vector3 position, bool rotated, int size)
            : base(screen, "entry_fence", position)
        {
            _rotated = rotated;
            _size = size;
        }

        public override void LoadContent()
        {
            LoadTexture("Textures/World/planks.png");
        }

        protected override void CreateGeometry()
        {
            var texture = new TextureCuboidWrapper();
            texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                new TextureRectangle(new Rectangle(0, 0, 16 * _size / 2, 32), Texture));
            texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right, CuboidSide.Bottom, CuboidSide.Top },
                new TextureRectangle(new Rectangle(0, 0, 1, 1), Texture));
            var vertices = CuboidComposer.Create(_size, 5f, 0.2f, texture);
            if (_rotated)
            {
                VertexTransformer.Rotate(vertices, new Vector3(0, MathHelper.PiOver2, 0));
            }
            Geometry.AddVertices(vertices);
        }
    }
}
