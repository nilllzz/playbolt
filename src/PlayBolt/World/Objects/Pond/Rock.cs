﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Pond
{
    class Rock : WorldObject
    {
        private int _type;

        public Rock(WorldScreen screen, Vector3 position, int type, float rotation)
            : base(screen, "pond_rock", position)
        {
            _type = type;
            _rotation = new Vector3(0, rotation, 0);
        }

        protected override void CreateGeometry()
        {
            Rectangle textureRectangle = default(Rectangle);

            switch (_type)
            {
                case 0:
                    textureRectangle = new Rectangle(112, 112, 9, 10);
                    break;
                case 1:
                    textureRectangle = new Rectangle(121, 112, 7, 9);
                    break;
                case 2:
                    textureRectangle = new Rectangle(128, 112, 7, 7);
                    break;
                case 3:
                    textureRectangle = new Rectangle(135, 112, 5, 5);
                    break;
            }

            var vertices = RectangleComposer.Create(textureRectangle.Width / 10f, textureRectangle.Height / 10f,
                new TextureRectangle(textureRectangle, Texture));
            VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, 0, 0));
            Geometry.AddVertices(vertices);
        }
    }
}
