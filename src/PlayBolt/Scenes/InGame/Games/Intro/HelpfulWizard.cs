﻿using Microsoft.Xna.Framework;

namespace PlayBolt.Scenes.InGame.Games.Intro
{
    //[MicrogameDescription(4, "Helpful Wizard", "Intro", "Solve a problem, but with MAGIC.")]
    class HelpfulWizard : Microgame
    {
        public override float CompletionTime => 5f;
        public override string Command => "Cast!";
        public override string Scoretable => "";
        public override int TargetScore => 20;

        // fire -> enlight camp fire
        // water -> turn off fire
        // electricity -> turn on TV
        // 

        public override void Draw(GameTime gameTime)
        {

        }

        public override void Update(GameTime gameTime)
        {

        }
    }
}
