﻿using static Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameDevCommon;
using GameDevCommon.Drawing;

namespace PlayBolt.Scenes
{
    class FadeTransitionScreen : Screen
    {
        private Screen _currentScreen, _newScreen;
        private RenderTarget2D _overlayTarget;
        private SpriteBatch _batch;
        private float _transition = 0f;

        public FadeTransitionScreen(Screen currentScreen, Screen newScreen)
        {
            _currentScreen = currentScreen;
            _newScreen = newScreen;
        }

        internal override void LoadContent()
        {
            _overlayTarget = new RenderTarget2D(Controller.GraphicsDevice, GameController.RENDER_WIDTH, GameController.RENDER_HEIGHT);
            _batch = new SpriteBatch(Controller.GraphicsDevice);
        }

        internal override void UnloadContent()
        {
            _currentScreen.UnloadContent();
            _overlayTarget.Dispose();
            _batch.Dispose();
        }

        internal override void Draw(GameTime gameTime)
        {
            RenderTargetManager.BeginRenderToTarget(_overlayTarget);
            Controller.GraphicsDevice.Clear(Color.White);

            _currentScreen.Draw(gameTime);

            RenderTargetManager.EndRenderToTarget();

            _newScreen.Draw(gameTime);

            _batch.Begin(SpriteBatchUsage.Default);
            _batch.Draw(_overlayTarget, Vector2.Zero, new Color(255, 255, 255, 255 - (int)(255 * _transition)));
            _batch.End();
        }

        internal override void Update(GameTime gameTime)
        {
            if (_transition < 1f)
            {
                _transition += 0.05f;
                if (_transition >= 1f)
                {
                    _transition = 1f;
                    GetComponent<ScreenManager>().SetScreen(_newScreen);
                    UnloadContent();
                }
            }
        }
    }
}
