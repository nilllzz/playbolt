﻿using GameDevCommon.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using static Core;

namespace PlayBolt.Scenes.InGame.Games.Intro
{
    [MicrogameDescription(1, "Krazy Kars", "Intro", "Jump over the krazy kars to avoid getting run over.")]
    class KrazyKars : Microgame
    {
        private enum Mode
        {
            Normal = 0,
            Jumping = 1,
            StopAndGo = 2,
            Stop = 3,
            GoAway = 4
        }

        public override float CompletionTime => 3.5f;
        public override string Command => "Dodge!";
        public override string Scoretable => "279024";
        public override int TargetScore => 15;

        private int _carId = 0;
        private Mode _mode = Mode.Normal;

        private bool _jumped = false;
        private float _playerY = 0f, _momentum = 0f;
        private float _wheelRotation = 0f;
        private float _carX = WIDTH + 200;
        private float _carMomentum = 0f;
        private float _carY = 0f;
        private bool _carTakenAction = false;
        private float _carWait = 20f;

        private Rectangle GetPlayerRectangle()
            => new Rectangle(50, (int)(HEIGHT - 92 - _playerY - 64), 96, 92);
        
        private Rectangle GetVehicleRectangle()
        {
            switch (_carId)
            {
                case 0:
                    return new Rectangle((int)_carX, (int)(HEIGHT - 210 - _carY), 192, 132);
                case 1:
                    return new Rectangle((int)_carX, (int)(HEIGHT - 190 - _carY), 230, 120);
                case 2:
                    return new Rectangle((int)_carX, (int)(HEIGHT - 170 - _carY), 256, 88);
            }
            return default(Rectangle);
        }

        public override void GameStart()
        {
            _carId = _random.Next(0, 3);

            // Easy: Normal (85%), GoAway (10%), Stop (5%)
            // Medium: Normal (55%), StopAndGo (35%), Stop (10%)
            // Hard: Normal (60%), Jumping (30%), StopAndGo (20%)

            var r = _random.Next(0, 100);
            switch (Difficulty)
            {
                case Difficulty.Easy:
                    if (r < 5)
                        _mode = Mode.Stop;
                    if (r < 15)
                        _mode = Mode.GoAway;
                    break;
                case Difficulty.Medium:
                    if (r < 10)
                        _mode = Mode.Stop;
                    else if (r < 45)
                        _mode = Mode.StopAndGo;
                    break;
                case Difficulty.Hard:
                    if (r < 30)
                        _mode = Mode.Jumping;
                    else if (r < 50)
                        _mode = Mode.StopAndGo;
                    break;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteBatchUsage.Default);

            _batch.DrawRectangle(new Rectangle(0, 0, WIDTH, HEIGHT), new Color(100, 100, 100));
            for (int x = 0; x < WIDTH; x+=32)
            {
                _batch.Draw(_texture, new Rectangle(x, HEIGHT - 64, 32, 64), new Rectangle(0, 32, 16, 32), Color.White);
            }

            _batch.Draw(_texture, GetPlayerRectangle(), new Rectangle(0, 0, 24, 23), Color.White,
                0f, Vector2.Zero, WinState == WinState.Lost ? SpriteEffects.FlipVertically : SpriteEffects.None, 0f);

            DrawCar();

            var vehicleRect = GetVehicleRectangle();

            _batch.Draw(_texture, new Rectangle((int)_carX + 64, (int)(HEIGHT - 80 - _carY), 32, 32), new Rectangle(32, 0, 16, 16), Color.White,
                -_wheelRotation, new Vector2(8, 8), SpriteEffects.None, 0f);
            _batch.Draw(_texture, new Rectangle((int)_carX + vehicleRect.Width - 32, (int)(HEIGHT - 80 - _carY), 32, 32), new Rectangle(32, 0, 16, 16), Color.White,
                -_wheelRotation, new Vector2(8, 8), SpriteEffects.None, 0f);

            _batch.End();
        }

        private void DrawCar()
        {
            var spriteEffects = _carTakenAction && _mode == Mode.GoAway ?
                SpriteEffects.FlipHorizontally : SpriteEffects.None;
            var rect = default(Rectangle);
            switch (_carId)
            {
                case 0:
                    rect = new Rectangle(48, 0, 32, 22);
                    break;
                case 1:
                    rect = new Rectangle(16, 32, 64, 32);
                    break;
                case 2:
                    rect = new Rectangle(16, 64, 64, 22);
                    break;
            }
            _batch.Draw(_texture, GetVehicleRectangle(), rect, Color.White, 0f, Vector2.Zero, spriteEffects, 0f);
        }

        public override void Update(GameTime gameTime)
        {
            UpdateCar();

            if (WinState == WinState.Default)
            {
                if (!_jumped)
                {
                    if (ActionPressed())
                    {
                        _jumped = true;
                        _momentum = 25f;
                    }
                }

                if (_momentum > 0f || _playerY > 0f)
                {
                    _momentum -= 0.9f * Speed;
                    _playerY += _momentum * Speed;
                    if (_playerY < 0f)
                    {
                        _playerY = 0f;
                    }
                }
            }
            else if (WinState == WinState.Lost)
            {
                _playerY -= 6f * Speed;
            }

            if (GetPlayerRectangle().Intersects(GetVehicleRectangle()))
            {
                WinState = WinState.Lost;
            }
        }

        private void UpdateCar()
        {
            switch (_mode)
            {
                case Mode.Normal:
                    _wheelRotation += 0.2f * Speed;
                    _carX -= 10f * Speed;
                    break;
                case Mode.Stop:
                    if (_carX > 230)
                    {
                        _wheelRotation += 0.2f * Speed;
                        _carX -= 10f * Speed;
                    }
                    break;

                case Mode.Jumping:
                    if (_carX < 400 && !_carTakenAction)
                    {
                        _carTakenAction = true;
                        _carMomentum = 18f;
                    }
                    _wheelRotation += 0.2f * Speed;
                    _carX -= 10f * Speed;
                    
                    break;
                case Mode.StopAndGo:
                    if (!_carTakenAction && _carX < 500)
                    {
                        _carTakenAction = true;
                        _carWait = 20f;
                    }
                    if (_carWait > 0f)
                    {
                        _carWait -= 1f * Speed;
                        if (_carWait <= 0f)
                            _carWait = 0f;
                    }
                    else
                    {
                        _wheelRotation += 0.2f * Speed;
                        _carX -= 10f * Speed;
                    }
                    break;
                case Mode.GoAway:
                    if (!_carTakenAction)
                    {
                        _wheelRotation += 0.2f * Speed;
                        _carX -= 10f * Speed;
                    }
                    else
                    {
                        _wheelRotation -= 0.1f * Speed;
                        _carX += 5f * Speed;
                    }
                    if (_carX < 230 && !_carTakenAction)
                    {
                        _carTakenAction = true;
                    }
                    break;
            }
            
            if (_carMomentum > 0f || _carY > 0f)
            {
                _carMomentum -= 0.5f * Speed;
                _carY += _carMomentum * Speed;
                if (_carY < 0f)
                    _carY = 0f;
            }
        }

        public override void TimeUp()
        {
            if (WinState == WinState.Default)
            {
                WinState = WinState.Won;

                // stare down trophy
                // did not jump, the car is a shark and it stopped.
                if (_carId == 1 && _mode == Mode.Stop && !_jumped)
                    GetComponent<TrophyManager>().UnlockTrophy(TrophyManager.STAREDOWN_ID);
            }
        }
    }
}
