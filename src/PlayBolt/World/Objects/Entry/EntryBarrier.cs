﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Entry
{
    class EntryBarrier : WorldObject
    {
        private ParkConfig _park;

        public EntryBarrier(WorldScreen screen, Vector3 position, ParkConfig park)
            : base(screen, "entry_barrier", position)
        {
            _park = park;
            IsOptimizable = false;
        }

        public override void Update()
        {
            CreateWorld();
            base.Update();
        }

        protected override void CreateWorld()
        {
            World = Matrix.CreateTranslation(new Vector3(3, -0.7f, 0))
                * Matrix.CreateRotationZ(_park.EntryBarrierOpened * MathHelper.PiOver2)
                * Matrix.CreateTranslation(new Vector3(-3, 0.7f, 0) + _position);
        }

        protected override void CreateGeometryInternal()
        {
            // barrier
            {
                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back, CuboidSide.Top, CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(16, 48, 32, 4), Texture));
                texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                    new TextureRectangle(new Rectangle(48, 48, 4, 4), Texture));
                var vertices = CuboidComposer.Create(6f, 0.3f, 0.3f, texture);
                VertexTransformer.Offset(vertices, new Vector3(0f, 0.7f, 0));
                Geometry.AddVertices(vertices);
            }
        }
    }
}
