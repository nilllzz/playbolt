﻿using GameDevCommon.Drawing;
using GameDevCommon.Input;
using JoltNet;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PlayBolt.Content;
using PlayBolt.Scenes.InGame;
using PlayBolt.Scenes.InGame.Games;
using System;
using System.Linq;
using System.Threading.Tasks;
using static Core;

namespace PlayBolt.Scenes.Menu
{
    class FeaturedSubScreen : SubScreen
    {
        private Texture2D _texture;
        private SpriteBatch _batch;
        private SpriteFont _font;
        private Microgame[] _featuredGames;
        private MicrogameCard[] _cards;
        private Highscore[][] _scores;
        private RenderTarget2D _descriptionTarget;
        private SpriteBatch _descriptionBatch;

        private int _selection = 0;

        public override void LoadContent(MenuContainerScreen containerScreen)
        {
            base.LoadContent(containerScreen);

            _texture = GetComponent<Resources>().LoadTexture("Textures/MenuTiles.png");
            _batch = new SpriteBatch(Controller.GraphicsDevice);
            _descriptionBatch = new SpriteBatch(Controller.GraphicsDevice);
            _font = Controller.Content.Load<SpriteFont>("Fonts/Font");
            _scores = new[] { new Highscore[10], new Highscore[10], new Highscore[10] };
            _descriptionTarget = new RenderTarget2D(Controller.GraphicsDevice, 270, 20);
        }

        public override void Activate()
        {
            UpdateGames();
        }

        private void UpdateGames()
        {
            Task.Run(async () =>
            {
                if (_featuredGames == null)
                {
                    await GetFeaturedGames();
                }
                await GetScores();
            });
        }

        private async Task GetFeaturedGames()
        {
            var timeRequest = RequestProvider.Time.Get();
            var response = await Controller.GameJoltClient.ExecuteRequestAsync(timeRequest);
            if (response.Success)
            {
                var num = timeRequest.Response.Year * 10000;
                num += timeRequest.Response.Month * 100;
                num += timeRequest.Response.Day;
                var random = new Random(num);
                _featuredGames = GetType().Assembly.GetTypes()
                    .Where(t => Attribute.GetCustomAttribute(t, typeof(MicrogameDescriptionAttribute)) != null)
                    .OrderBy(x => random.Next())
                    .Take(3)
                    .Select(t => (Microgame)Activator.CreateInstance(t))
                    .ToArray();

                _featuredGames[0] = new InGame.Games.Weird.Stitching();

                _cards = _featuredGames.Select((g, i)
                    => new MicrogameCard(g, _texture, SubScreenBounds.Width / 2 - 455 + i * 320, 90)).ToArray();
            }
        }

        private async Task GetScores()
        {
            await Task.WhenAll(_featuredGames.Select((game, i) => Task.Run(async () =>
            {
                _scores[i] = await HighscoreProvider.GetHighscores(game.Scoretable, 10);
            })));
        }

        public override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteBatchUsage.Default);

            _batch.DrawRectangle(SubScreenBounds, Color.White);

            var textSize = _font.MeasureString("Featured Games");
            _batch.DrawString(_font, "Featured Games", new Vector2(SubScreenBounds.Width / 2f - textSize.X, 14), Color.Black,
                0f, Vector2.Zero, 2f, SpriteEffects.None, 0f);

            _batch.DrawRectangle(new Rectangle(SubScreenBounds.Width / 2 - 25, 66, 50, 3), new Color(126, 126, 126));

            if (_cards != null)
            {
                var startX = SubScreenBounds.Width / 2 - 455;
                for (int i = 0; i < _featuredGames.Length; i++)
                {
                    var scores = _scores[i];

                    _cards[i].Draw(_batch, _font, _descriptionBatch, _descriptionTarget, _selection == i);

                    for (int s = 0; s < scores.Length; s++)
                    {
                        var score = scores[s];
                        var y = 0;
                        var color = Color.Black;
                        switch (s)
                        {
                            case 0:
                                _batch.Draw(_texture, new Rectangle(startX + i * 320, 320, 34, 35), new Rectangle(0, 66, 34, 35), Color.White);
                                y = 326;
                                break;
                            case 1:
                                _batch.Draw(_texture, new Rectangle(startX + i * 320, 360, 34, 35), new Rectangle(34, 66, 34, 35), Color.White);
                                y = 366;
                                break;
                            case 2:
                                _batch.Draw(_texture, new Rectangle(startX + i * 320, 400, 34, 35), new Rectangle(68, 66, 34, 35), Color.White);
                                y = 406;
                                break;
                            default:
                                _batch.DrawString(_font, (s + 1).ToString(), new Vector2(startX + i * 322 + 10, 370 + s * 26), Color.Black);
                                y = 370 + s * 26;
                                var c = 80 + s * 10;
                                color = new Color(c, c, c);
                                break;
                        }
                        score.Draw(gameTime, _batch, _font, _texture, new Vector2(startX + i * 322 + 64, y), color, 255);
                    }
                }
            }

            _batch.End();
        }

        public override void Update(GameTime gameTime)
        {
            var keyboard = GetComponent<KeyboardHandler>();
            var gamepad = GetComponent<GamePadHandler>();

            if (_cards != null)
            {
                var controls = GetComponent<ControlsHandler>();
                if (controls.RightPressed(PlayerIndex.One))
                {
                    _selection++;
                    if (_selection == _featuredGames.Length)
                        _selection = 0;
                }
                if (controls.LeftPressed(PlayerIndex.One))
                {
                    _selection--;
                    if (_selection == -1)
                        _selection = _featuredGames.Length - 1;
                }

                for (int i = 0; i < _cards.Length; i++)
                {
                    _cards[i].Update(_selection == i, _font);
                }

                if (keyboard.KeyPressed(Keys.Enter) || keyboard.KeyPressed(Keys.Space) ||
                    gamepad.ButtonPressed(PlayerIndex.One, Buttons.A))
                {
                    var gameScreen = new SingleStartGameScreen(_containerScreen, _featuredGames[_selection]);
                    gameScreen.LoadContent();
                    GetComponent<ScreenManager>().SetScreen(gameScreen);
                }
            }

            if (keyboard.KeyPressed(Keys.Escape) || keyboard.KeyPressed(Keys.E) ||
                gamepad.ButtonPressed(PlayerIndex.One, Buttons.B))
            {
                _containerScreen.ExitSubScreen();
            }
        }
    }
}
