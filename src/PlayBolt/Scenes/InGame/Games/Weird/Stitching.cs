﻿using GameDevCommon.Drawing;
using GameDevCommon.Input;
using Microsoft.Xna.Framework;

namespace PlayBolt.Scenes.InGame.Games.Weird
{
    [MicrogameDescription(9, "Right in the Eye", "Weird", "Thread the needle - literally")]
    class Stitching : Microgame
    {
        public override float CompletionTime => 4f;
        public override string Command => "Fit!";
        public override string Scoretable => "368503";
        public override int TargetScore => 20;
        public override Color CommandColor => Color.White;
        public override string Thumbnail => "9.png";

        private int _targetY = 0;
        private float _x, _y;
        private int _direction = 0;

        private int TargetHeight
        {
            get
            {
                switch (Difficulty)
                {
                    case Difficulty.Easy:
                        return 18;
                    case Difficulty.Medium:
                        return 14;
                    case Difficulty.Hard:
                        return 11;
                }
                return 0;
            }
        }

        public override void GameStart()
        {
            _targetY = _random.Next(100, 350);
            _y = _random.Next(100, 350);
            _x = WIDTH - 320;
        }

        public override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteBatchUsage.Default);

            // background
            _batch.DrawRectangle(Bounds, new Color(40, 10, 40));

            _batch.Draw(_texture,
                new Rectangle(190, _targetY, 16, TargetHeight * 4),
                new Rectangle((int)Difficulty * 4, 0, 4, TargetHeight),
                Color.White);
            _batch.Draw(_texture,
                new Rectangle(194, _targetY + TargetHeight * 4, 8, HEIGHT),
                new Rectangle(1, 0, 2, 1),
                Color.White);

            _batch.Draw(_texture,
                new Rectangle((int)_x, (int)_y, 87 * 4, 36 * 4),
                new Rectangle(12, 0, 87, 36),
                Color.White);

            _batch.End();
        }

        public override void Update(GameTime gameTime)
        {
            if (_x > 200)
            {
                if (DirectionPressed(InputDirection.Up))
                {
                    _direction = -1;
                }
                else if (DirectionPressed(InputDirection.Down))
                {
                    _direction = 1;
                }
            }
            else
            {
                if (WinState == WinState.Default)
                {
                    // determine win/loss
                    var needlePos = _y + 9 * 4;
                    if (needlePos > _targetY && needlePos < _targetY + TargetHeight * 4)
                    {
                        WinState = WinState.Won;
                    }
                    else
                    {
                        WinState = WinState.Lost;
                    }
                }
                _direction = 0;
            }

            if (WinState != WinState.Lost)
            {
                _x -= Speed * 2f;
            }
            _y += Speed * _direction * 2.5f;
        }
    }
}
