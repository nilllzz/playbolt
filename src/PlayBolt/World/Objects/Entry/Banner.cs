﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Entry
{
    class Banner : WorldObject
    {
        public Banner(WorldScreen screen, Vector3 position)
            : base(screen, "entry_banner", position)
        { }

        protected override void CreateGeometryInternal()
        {
            var texture = new TextureCuboidWrapper();
            texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                new TextureRectangle(new Rectangle(256, 16, 32, 48), Texture));
            texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                new TextureRectangle(new Rectangle(256, 16, 1, 48), Texture));
            texture.AddSide(new[] { CuboidSide.Bottom, CuboidSide.Top },
                new TextureRectangle(new Rectangle(256, 16, 32, 1), Texture));
            var vertices = CuboidComposer.Create(2f, 3f, 0.1f, texture);
            VertexTransformer.Rotate(vertices, new Vector3(-0.05f, 0f, 0f));
            Geometry.AddVertices(vertices);
        }
    }
}
