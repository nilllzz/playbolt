﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Arcade
{
    class ArcadeDecorationsAlien2 : WorldObject
    {
        public ArcadeDecorationsAlien2(WorldScreen screen, Vector3 position)
            : base(screen, "arcade_decorations_alien_2", position)
        {
            IsOpaque = false;
            IsOptimizable = false;
        }

        protected override void CreateGeometryInternal()
        {
            var vertices = RectangleComposer.Create(6, 5,
                new TextureRectangle(new Rectangle(604, 48, 12, 10), Texture));
            VertexTransformer.Rotate(vertices, new Vector3(0, -1.8f, MathHelper.PiOver2));
            Geometry.AddVertices(vertices);
        }
    }
}
