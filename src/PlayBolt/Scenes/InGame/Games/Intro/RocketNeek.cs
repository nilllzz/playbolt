﻿using GameDevCommon.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace PlayBolt.Scenes.InGame.Games.Intro
{
    [MicrogameDescription(5, "Rocket Neek", "Intro", "Prop Neek right up so he can make his journey into space.")]
    class RocketNeek : Microgame
    {
        public override float CompletionTime => 4.5f;
        public override string Command => "Launch!";
        public override string Scoretable => "279069";
        public override int TargetScore => 15;

        private class Smoke
        {
            private float _x, _y;
            private float _rotation = 0f;
            private int MoveX;
            public float Delay = 255;

            public Smoke(int x, int y)
            {
                _y = y + 50;
                _x = x;
                MoveX = _random.Next(-2, 3);
            }

            public void Draw(SpriteBatch batch, Texture2D texture)
            {
                batch.Draw(texture, new Rectangle((int)(_x + 16 + 250), (int)(_y + 16), 32, 32), new Rectangle(16, 96, 32, 32),
                    new Color(255, 255, 255, (int)Delay),
                    _rotation, new Vector2(16), SpriteEffects.None, 0f);
            }

            public void Update(float speed)
            {
                Delay -= 10 * speed;
                _rotation += 0.1f * speed;
                _y += 1 * speed;
                _x += MoveX * speed;
            }
        }

        private float _rotation = 0f;
        private float _x, _y;
        private float _moveX = 0f, _moveY = 0f;
        private bool _started = false;
        private List<Smoke> _smoke = new List<Smoke>();
        private List<Rectangle> _barriers = new List<Rectangle>();

        public override void GameStart()
        {
            _x = 175;
            _y = HEIGHT - 32 - 92;
            _rotation = _random.Next(0, 10);

            switch (Difficulty)
            {
                case Difficulty.Easy:
                    if (_random.Next(0, 2) == 0)
                        _barriers.Add(new Rectangle(0, _random.Next(0, 200), 192, 64));
                    else
                        _barriers.Add(new Rectangle(400 - 192, _random.Next(0, 200), 192, 64));
                    break;
                case Difficulty.Medium:
                    _barriers.Add(new Rectangle(0, _random.Next(100, 300), 96, 64));
                    _barriers.Add(new Rectangle(400 - 96, _random.Next(100, 300), 96, 64));
                    break;
                case Difficulty.Hard:
                    _barriers.Add(new Rectangle(0, _random.Next(50, 100), 96, 64));
                    _barriers.Add(new Rectangle(400 - 160, _random.Next(200, 300), 160, 64));
                    break;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteBatchUsage.Default);

            switch (WinState)
            {
                case WinState.Default:
                    _batch.DrawGradient(new Rectangle(250, 0, WIDTH - 500, HEIGHT),
                        new Color(118, 118, 247), new Color(111, 165, 224), false);
                    break;
                case WinState.Won:
                    _batch.DrawGradient(new Rectangle(250, 0, WIDTH - 500, HEIGHT),
                        new Color(113, 232, 133), new Color(113, 232, 221), false);
                    break;
                case WinState.Lost:
                    _batch.DrawGradient(new Rectangle(250, 0, WIDTH - 500, HEIGHT),
                        new Color(240, 166, 186), new Color(113, 232, 221), false);
                    break;
            }

            var rot = (float)Math.Sin(_rotation) * 1.5f;
            _batch.Draw(_texture, new Rectangle((int)(_x + 250 + 25), (int)(_y + 46), 50, 92),
                new Rectangle(0, 0, 50, 92), Color.White,
                rot, new Vector2(25, 46), SpriteEffects.None, 0f);
            
            for (int x = 0; x < 400; x += 32)
            {
                _batch.Draw(_texture, new Rectangle(x + 250, HEIGHT - 64, 32, 64),
                    new Rectangle(0, 96, 16, 32), Color.White);
            }

            foreach (var barrier in _barriers)
            {
                for (int x = 0; x < barrier.Width; x += 32)
                {
                    for (int y = 0; y < barrier.Height; y += 32)
                    {
                        _batch.Draw(_texture, new Rectangle(x + barrier.X + 250, y + barrier.Y, 32, 32),
                            new Rectangle(0, 112, 16, 16), Color.White);
                    }
                    _batch.Draw(_texture, new Rectangle(x + barrier.X + 250, barrier.Y - 32, 32, 32),
                        new Rectangle(0, 96, 16, 16), Color.White);
                }
            }

            _batch.DrawRectangle(new Rectangle(0, 0, 250, HEIGHT),
                new Color(100, 100, 100));

            _batch.DrawRectangle(new Rectangle(WIDTH - 250, 0, 250, HEIGHT),
                new Color(100, 100, 100));

            _batch.End();

            _batch.Begin(SpriteBatchUsage.RealTransparency);

            foreach (var smoke in _smoke)
            {
                smoke.Draw(_batch, _texture);
            }

            _batch.End();
        }

        public override void Update(GameTime gameTime)
        {
            if (!_started)
            {
                _rotation += 0.05f * Speed;
                if (ActionPressed())
                {
                    var moveVal = (float)Math.Sin(_rotation) * 1.5f;
                    _moveY = -(1f - Math.Abs(moveVal) / 2f);
                    _moveX = moveVal / 2f;
                    _started = true;
                }
            }
            else
            {
                if (WinState == WinState.Lost)
                {
                    _y += 5 * Speed;
                    _rotation += 0.1f * Speed;
                }
                else
                {
                    // only visual, does not need to worry about speed
                    if (_random.Next(0, 3) == 0)
                    {
                        _smoke.Add(new Smoke((int)_x, (int)_y));
                    }

                    _x += _moveX * 10f * Speed;
                    _y += _moveY * 10f * Speed;
                    if (_x <= 0)
                    {
                        _moveX *= -1;
                        _rotation -= (float)Math.Asin(1f) * 2;
                        _x = 0;
                    }
                    else if (_x >= 350)
                    {
                        _moveX *= -1;
                        _rotation += (float)Math.Asin(1f) * 2;
                        _x = 350;
                    }

                    if (WinState == WinState.Default)
                    {
                        foreach (var barrier in _barriers)
                        {
                            if (barrier.Intersects(new Rectangle((int)(_x + 10), (int)(_y + 31), 30, 30)))
                            {
                                WinState = WinState.Lost;
                            }
                        }
                    }

                    if (_y <= -20)
                    {
                        WinState = WinState.Won;
                    }
                }

                for (int i = 0; i < _smoke.Count; i++)
                {
                    var smoke = _smoke[i];
                    smoke.Update(Speed);
                    if (smoke.Delay <= 0)
                    {
                        _smoke.Remove(smoke);
                        i--;
                    }
                }
            }
        }

        public override void TimeUp()
        {
            if (WinState == WinState.Default)
            {
                WinState = WinState.Lost;
            }
        }
    }
}
