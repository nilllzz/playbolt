﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PlayBolt.World.Objects.MotionSim
{
    class Arwing : WorldObject
    {
        private static Random _random = new Random();

        private float _orgY;
        private bool _started = false;
        private int _runtime;
        private Vector3 _targetRotation;

        public Arwing(WorldScreen screen, Vector3 position)
            : base(screen, "motionsim_arwing", position)
        {
            IsOptimizable = false;
            _orgY = position.Y;
        }

        public override void Update()
        {
            if (!_started)
            {
                if (_random.Next(0, 400) == 0)
                {
                    _started = true;
                    _runtime = 1000;
                    _targetRotation = Vector3.Zero;
                }
            }
            else
            {
                if (_position.Y - _orgY < 6.9f && _runtime > 0)
                {
                    _position.Y = MathHelper.Lerp(_position.Y, _orgY + 7f, 0.03f);
                }
                else
                {
                    _runtime--;
                    if (_targetRotation != _rotation)
                    {
                        _rotation.X = MathHelper.Lerp(_rotation.X, _targetRotation.X, 0.05f);
                        if (Math.Abs(_rotation.X - _targetRotation.X) < 0.005f)
                            _rotation.X = _targetRotation.X;
                        _rotation.Y = MathHelper.Lerp(_rotation.Y, _targetRotation.Y, 0.05f);
                        if (Math.Abs(_rotation.Y - _targetRotation.Y) < 0.005f)
                            _rotation.Y = _targetRotation.Y;
                        _rotation.Z = MathHelper.Lerp(_rotation.Z, _targetRotation.Z, 0.05f);
                        if (Math.Abs(_rotation.Z - _targetRotation.Z) < 0.005f)
                            _rotation.Z = _targetRotation.Z;
                    }
                    else
                    {
                        if (_runtime <= 0)
                        {
                            _runtime = 0;
                            if (_rotation != Vector3.Zero)
                            {
                                _targetRotation = Vector3.Zero;
                            }
                            else
                            {
                                if (_position.Y - _orgY > 0f)
                                {
                                    _position.Y -= 0.05f;
                                }
                                else
                                {
                                    _started = false;
                                }
                            }
                        }
                        else
                        {
                            if (_random.Next(0, 40) == 0)
                            {
                                _targetRotation = new Vector3(
                                    ((float)_random.NextDouble() - 0.5f) * MathHelper.PiOver4,
                                    ((float)_random.NextDouble() - 0.5f) * MathHelper.PiOver2,
                                    ((float)_random.NextDouble() - 0.5f) * MathHelper.PiOver4
                                );
                            }
                        }
                    }
                }
                CreateWorld();
            }
            base.Update();
        }

        protected override void CreateGeometryInternal()
        {
            var vertices = new List<VertexPositionNormalTexture>();
            var sideVertices = new List<VertexPositionNormalTexture>();

            // front top
            {
                vertices.AddRange(TriComposer.Create(
                    new Vector3(0, 0, 0.5f),
                    new Vector3(-0.2f, 0.5f, -1.5f),
                    new Vector3(0.2f, 0.5f, -1.5f),
                    new TextureRectangle(new Rectangle(464, 64, 1, 1), Texture)));
            }
            // window
            {
                vertices.AddRange(RectangleComposer.Create(new[]
                {
                    new Vector3(-0.2f, 0.8f, -1.8f),
                    new Vector3(0.2f, 0.8f, -1.8f),
                    new Vector3(-0.2f, 0.5f, -1.5f),
                    new Vector3(0.2f, 0.5f, -1.5f)
                }, new TextureRectangle(new Rectangle(464, 65, 1, 1), Texture)));
            }
            // rear (window)
            {
                vertices.AddRange(TriComposer.Create(
                    new Vector3(-0.2f, 0.8f, -1.8f),
                    new Vector3(0.2f, 0.8f, -1.8f),
                    new Vector3(0f, 0.35f, -2.4f),
                    new TextureRectangle(new Rectangle(464, 64, 1, 1), Texture)));
            }
            // exhaust
            {
                vertices.AddRange(TriComposer.Create(
                    new Vector3(0f, 0.35f, -2.4f),
                    new Vector3(-0.6f, -0.1f, -1.8f),
                    new Vector3(0.6f, -0.1f, -1.8f),
                    new TextureRectangle(new Rectangle(472, 0, 32, 32), Texture)));
            }
            // exhaust bottom
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(0.6f, -0.1f, -1.8f),
                    new Vector3(-0.6f, -0.1f, -1.8f),
                    new Vector3(0, -0.25f, -1.8f),
                    new TextureRectangle(new Rectangle(472, 0, 1, 1), Texture)));
            }


            // up
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(0, 0, 0.5f),
                    new Vector3(0.2f, 0.5f, -1.5f),
                    new Vector3(0.6f, -0.1f, -1.8f),
                    new TextureRectangle(new Rectangle(465, 64, 1, 1), Texture)));
            }
            // down
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(0, 0, 0.5f),
                    new Vector3(0.6f, -0.1f, -1.8f),
                    new Vector3(0, -0.25f, -1.8f),
                    new TextureRectangle(new Rectangle(467, 64, 1, 1), Texture)));
            }
            // back
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(0.2f, 0.5f, -1.5f),
                    new Vector3(0.2f, 0.8f, -1.8f),
                    new Vector3(0.6f, -0.1f, -1.8f),
                    new TextureRectangle(new Rectangle(466, 64, 1, 1), Texture)));
            }
            // rear
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(0.2f, 0.8f, -1.8f),
                    new Vector3(0.6f, -0.1f, -1.8f),
                    new Vector3(0f, 0.35f, -2.4f),
                    new TextureRectangle(new Rectangle(466, 64, 1, 1), Texture)));
            }
            // wing 1
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(0.6f, -0.1f, -1.8f),
                    new Vector3(0.7f, -0.15f, -0.6f),
                    new Vector3(1.2f, 1f, -2.6f),
                    new TextureRectangle(new Rectangle(464, 32, 16, 32), Texture)));
            }
            // wing 2
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(0.7f, -0.15f, -0.6f),
                    new Vector3(1.2f, 1f, -2.6f),
                    new Vector3(1f, 0f, -1.8f),
                    new TextureRectangle(new Rectangle(464, 32, 16, 32), Texture)));
            }
            // wing back
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(1.2f, 1f, -2.6f),
                    new Vector3(1f, 0f, -1.8f),
                    new Vector3(0.6f, -0.1f, -1.8f),
                    new TextureRectangle(new Rectangle(464, 32, 16, 32), Texture)));
            }
            // wing bottom
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(1f, 0f, -1.8f),
                    new Vector3(0.6f, -0.1f, -1.8f),
                    new Vector3(0.7f, -0.15f, -0.6f),
                    new TextureRectangle(new Rectangle(464, 32, 16, 32), Texture)));
            }
            // flap top
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(0.6f, -0.1f, -1.8f),
                    new Vector3(1f, -0.15f, -1.9f),
                    new Vector3(1.2f, -0.5f, -2.9f),
                    new TextureRectangle(new Rectangle(465, 64, 1, 1), Texture)));
            }
            // flap bottom
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(0.6f, -0.1f, -1.8f),
                    new Vector3(1f, -0.35f, -1.9f),
                    new Vector3(1.2f, -0.5f, -2.9f),
                    new TextureRectangle(new Rectangle(468, 64, 1, 1), Texture)));
            }
            // flap side
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(1f, -0.15f, -1.9f),
                    new Vector3(1f, -0.35f, -1.9f),
                    new Vector3(1.2f, -0.5f, -2.9f),
                    new TextureRectangle(new Rectangle(467, 64, 1, 1), Texture)));
            }
            // flap front
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(1f, -0.15f, -1.9f),
                    new Vector3(1f, -0.35f, -1.9f),
                    new Vector3(0.6f, -0.1f, -1.8f),
                    new TextureRectangle(new Rectangle(467, 64, 1, 1), Texture)));
            }
            // flap add
            {
                sideVertices.AddRange(TriComposer.Create(
                    new Vector3(1.2f, -0.5f, -2.9f),
                    new Vector3(1.1f, -0.325f, -2.4f),
                    new Vector3(1.2f, -0.325f, -2.4f),
                    new TextureRectangle(new Rectangle(465, 64, 1, 1), Texture)));
            }

            vertices.AddRange(sideVertices);
            vertices.AddRange(sideVertices.Select(v =>
            {
                return new VertexPositionNormalTexture
                {
                    Position = new Vector3
                    {
                        X = v.Position.X * -1f,
                        Y = v.Position.Y,
                        Z = v.Position.Z
                    },
                    Normal = v.Normal,
                    TextureCoordinate = v.TextureCoordinate
                };
            }));

            var vArr = vertices.ToArray();
            VertexTransformer.Rotate(vArr, new Vector3(0, -MathHelper.PiOver2, 0));
            VertexTransformer.Scale(vArr, new Vector3(7));
            Geometry.AddVertices(vArr);
        }
    }
}
