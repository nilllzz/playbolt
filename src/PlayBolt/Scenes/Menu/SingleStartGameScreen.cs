﻿using PlayBolt.Scenes.InGame;
using PlayBolt.Scenes.InGame.Games;
using System.Threading.Tasks;
using static Core;

namespace PlayBolt.Scenes.Menu
{
    class SingleStartGameScreen : StartGameScreen
    {
        private Microgame _game;

        public SingleStartGameScreen(Screen preScreen, Microgame game)
            : base(preScreen, game.Name, "Single")
        {
            _game = game;
        }
        
        protected override void GetScores()
        {
            Task.Run(async () => _highscoreData = await HighscoreProvider.GetFullScoreData(_game.Scoretable, 3));
        }

        protected override void StartGame()
        {
            UnloadContent();
            var screen = new SingleInGameScreen(_preScreen, _game);
            screen.LoadContent();
            GetComponent<ScreenManager>().SetScreen(screen);
        }
    }
}
