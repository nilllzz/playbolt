﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Ferris
{
    class FerrisWheel : WorldObject
    {
        public FerrisWheel(WorldScreen screen, Vector3 position)
            : base(screen, "ferris_wheel", position)
        {
            IsOptimizable = false;
        }

        public override void Update()
        {
            _rotation.Z -= 0.002f;
            CreateWorld();

            base.Update();
        }

        protected override void CreateGeometryInternal()
        {
            for (int i = 0; i < 16; i++)
            {
                var rot = MathHelper.PiOver4 / 2f * i;
                // supports
                {
                    if (i % 2 == 0)
                    {
                        var texture = new TextureCuboidWrapper();
                        texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                            new TextureRectangle(new Rectangle(0, 152, 112, 8), Texture));
                        texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                            new TextureRectangle(new Rectangle(0, 152, 1, 1), Texture));
                        texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                            new TextureRectangle(new Rectangle(112, 152, 8, 8), Texture));
                        var vertices = CuboidComposer.Create(30f, 1f, 1f, texture);
                        VertexTransformer.Offset(vertices, new Vector3(18, 0, 0));
                        VertexTransformer.Rotate(vertices, new Vector3(0, 0, rot));
                        Geometry.AddVertices(vertices);
                    }
                    else
                    {
                        var texture = new TextureCuboidWrapper();
                        texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                            new TextureRectangle(new Rectangle(0, 152, 1, 1), Texture));
                        texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                            new TextureRectangle(new Rectangle(0, 152, 1, 1), Texture));
                        texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                            new TextureRectangle(new Rectangle(112, 152, 8, 8), Texture));
                        var vertices = CuboidComposer.Create(30f, 0.5f, 0.5f, texture);
                        VertexTransformer.Offset(vertices, new Vector3(18, 0, 0));
                        VertexTransformer.Rotate(vertices, new Vector3(0, 0, rot));
                        Geometry.AddVertices(vertices);
                    }
                }
                // end bars
                {
                    var vertices = CuboidComposer.Create(0.5f, 13.4f, 0.5f,
                        new TextureRectangle(new Rectangle(0, 152, 1, 1), Texture));
                    VertexTransformer.Rotate(vertices, new Vector3(0, 0, 0));
                    VertexTransformer.Offset(vertices, new Vector3(33.25f, 0, 0));
                    VertexTransformer.Rotate(vertices, new Vector3(0, 0, rot));
                    Geometry.AddVertices(vertices);
                }
            }
        }
    }
}
