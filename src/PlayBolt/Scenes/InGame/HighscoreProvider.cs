﻿using JoltNet;
using PlayBolt.Scenes.InGame.Games;
using System.Threading.Tasks;
using static Core;

namespace PlayBolt.Scenes.InGame
{
    struct MicrogameScores
    {
        public int OwnRank;
        public Highscore OwnHighscore;
        public Highscore[] Highscores;
    }

    static class HighscoreProvider
    {
        private static async Task<Highscore[]> GetScoresInternal(string scoreTable, int limit,
            GameJoltRequest<RequestProvider.Time.TimeResponse> timeRequest, IGameJoltRequest userScoreRequest)
        {
            if (timeRequest == null)
                timeRequest = RequestProvider.Time.Get();

            var highscoresRequest = RequestProvider.Scores.Fetch(scoreTable, limit);
            IGameJoltRequest[]  requests;
            if (userScoreRequest == null)
                requests = new IGameJoltRequest[] { highscoresRequest, timeRequest };
            else
                requests = new IGameJoltRequest[] { highscoresRequest, userScoreRequest, timeRequest };

            await Controller.GameJoltClient.ExecuteRequestsAsync(requests, GameJoltApiRequestFeature.ParallelProcessing);

            var serverTime = Extensions.GetUtcFromUnixTimestamp(timeRequest.Response.Timestamp);
            var highscores = new Highscore[limit];
            for (int i = 0; i < limit; i++)
            {
                if (highscoresRequest.Response.Scores.Length > i)
                {
                    var scoreData = highscoresRequest.Response.Scores[i];
                    highscores[i] = new Highscore(serverTime, scoreData);
                }
                else
                {
                    highscores[i] = default(Highscore);
                }
            }

            return highscores;
        }

        public static async Task<Highscore[]> GetHighscores(string scoreTable, int limit)
        {
            var highscores = await GetScoresInternal(scoreTable, limit, null, null);
            return highscores;
        }

        public static async Task<MicrogameScores> GetFullScoreData(string scoreTable, int limit)
        {
            var timeRequest = RequestProvider.Time.Get();
            var userScoreRequest = RequestProvider.Scores.Fetch(scoreTable, Controller.GameJoltUsername, Controller.GameJoltToken);

            var highscores = await GetScoresInternal(scoreTable, limit, timeRequest, userScoreRequest);
            var serverTime = Extensions.GetUtcFromUnixTimestamp(timeRequest.Response.Timestamp);

            var data = new MicrogameScores()
            {
                Highscores = highscores
            };

            if (userScoreRequest.Response.Scores.Length > 0)
            {
                var ownRankRequest = RequestProvider.Scores.FetchRank(userScoreRequest.Response.Scores[0].Sort);
                await Controller.GameJoltClient.ExecuteRequestAsync(ownRankRequest);
                data.OwnHighscore = new Highscore(serverTime, userScoreRequest.Response.Scores[0]);
                data.OwnRank = ownRankRequest.Response.Rank;
            }
            else
            {
                data.OwnHighscore = default(Highscore);
                data.OwnRank = -1;
            }

            return data;
        }
    }
}
