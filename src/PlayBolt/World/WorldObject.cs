﻿using GameDevCommon.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayBolt.Content;
using System;
using System.IO;
using static Core;

namespace PlayBolt.World
{
    abstract class WorldObject : Base3DObject<VertexPositionNormalTexture>, IComparable
    {
        private const bool LOAD_GEOMETRY_FROM_FILE = false;
        private const bool SAVE_GEOMETRY_TO_FILE = false;

        private string _name;
        protected float _cameraDistance;
        protected WorldScreen _parentScreen;
        protected Vector3 _position, _rotation;

        public Vector3 Position => _position;
        public Vector3 Rotation => _rotation;

        public WorldObject(WorldScreen screen, string name)
        {
            _parentScreen = screen;
            _name = name;
        }

        public WorldObject(WorldScreen screen, string name, Vector3 position)
        {
            _parentScreen = screen;
            _name = name;
            _position = position;
        }

        public override void LoadContent()
        {
            LoadTexture("Textures/World/main.png");
        }

        protected void LoadTexture(Color color)
        {
            var texture = new Texture2D(Controller.GraphicsDevice, 1, 1);
            texture.SetData(new[] { color });
            texture.Name = color.GetHashCode().ToString();
            Texture = texture;
            base.LoadContent();
        }

        protected void LoadTexture(string texture)
        {
            Texture = GetComponent<Resources>().LoadTexture(texture);
            base.LoadContent();
        }

        protected override void CreateWorld()
        {
            World = Matrix.CreateFromYawPitchRoll(_rotation.Y, _rotation.X, _rotation.Z)
                * Matrix.CreateTranslation(_position);
            base.CreateWorld();
        }

        protected override void CreateGeometry()
        {
            if (LOAD_GEOMETRY_FROM_FILE)
            {
                GetComponent<Resources>().LoadGeometry(Geometry, "Geometry/" + _name + ".geo");
            }
            else
            {
                CreateGeometryInternal();
                if (SAVE_GEOMETRY_TO_FILE)
                {
                    GeometrySerializer.Save(Geometry,
                        Path.Combine(Environment.CurrentDirectory, "Content", "Geometry", _name + ".geo"));
                }
            }
        }

        protected virtual void CreateGeometryInternal() { }

        public override void Update()
        {
            base.Update();
            _cameraDistance = Math.Abs(Vector3.Distance(_position, _parentScreen.Camera.Position));
        }

        public virtual int CompareTo(object other)
        {
            if (other is WorldObject g)
            {
                if (g._cameraDistance < _cameraDistance)
                    return -1;
                else
                    return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}
