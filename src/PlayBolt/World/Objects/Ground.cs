﻿using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace PlayBolt.World.Objects
{
    class Ground : WorldObject
    {
        private Dictionary<Point, float> _heights = new Dictionary<Point, float>();

        public Ground(WorldScreen screen)
            : base(screen, "ground")
        { }

        public override void LoadContent()
        {
            LoadTexture("Textures/World/floor.png");
        }

        const int sizeX = 300;
        const int sizeY = 300;
        const int tileSize = 10;

        private void PopulateHeights()
        {
            void addHeight(int x, int y, float height)
            {
                var p = new Point(x, y);
                if (!_heights.ContainsKey(p))
                    _heights.Add(p, height);
            }

            // exit ramp
            {
                for (int x = 40; x <= 80; x += tileSize)
                {
                    for (int y = 240; y <= 280; y += tileSize)
                    {
                        addHeight(x, y, 2f);
                    }
                }
            }
            // parking lot
            {
                for (int x = 80; x <= 230; x += tileSize)
                {
                    for (int y = 210; y <= 280; y += tileSize)
                    {
                        addHeight(x, y, 1f);
                    }
                }
            }

            // ferris wheel
            {
                for (int x = 220; x <= 270; x += tileSize)
                {
                    for (int y = 140; y <= 180; y += tileSize)
                    {
                        addHeight(x, y, 8f);
                    }
                }
                for (int x = 210; x <= 280; x += tileSize)
                {
                    for (int y = 140; y <= 190; y += tileSize)
                    {
                        addHeight(x, y, 4f);
                    }
                }
            }
            // lake
            {
                for (int x = 30; x <= 50; x += tileSize)
                {
                    for (int y = 170; y <= 180; y += tileSize)
                    {
                        addHeight(x, y, -10f);
                    }
                }
                addHeight(50, 160, -7f);
                for (int x = 20; x <= 60; x += tileSize)
                {
                    for (int y = 160; y <= 190; y += tileSize)
                    {
                        addHeight(x, y, -5f);
                    }
                }
                addHeight(50, 150, -7f);
                addHeight(50, 140, -7f);
                addHeight(50, 130, -7f);
                addHeight(60, 130, -7f);
                addHeight(70, 130, -7f);
                addHeight(80, 130, -7f);
                addHeight(90, 130, -7f);
                addHeight(100, 130, -12f);
            }
            // mountain
            {
                // outer ring
                for (int y = 0; y < 8; y++)
                {
                    addHeight(90, 30 + y * 10, 5);
                }
                addHeight(100, 110, 5);
                addHeight(110, 120, 5);
                addHeight(120, 120, 5);
                addHeight(130, 120, 5);
                addHeight(140, 120, 4);
                addHeight(150, 120, 5);
                addHeight(160, 120, 3);
                addHeight(170, 120, 5);
                addHeight(180, 110, 6);
                addHeight(190, 110, 5);
                addHeight(200, 100, 5);
                addHeight(200, 90, 5);
                addHeight(200, 80, 5);
                addHeight(200, 70, 5);
                addHeight(200, 60, 5);
                addHeight(200, 50, 5);
                addHeight(190, 40, 5);
                addHeight(180, 30, 5);
                for (int x = 100; x < 180; x+=10)
                {
                    addHeight(x, 30, 5);
                }

                // second ring
                addHeight(100, 40, 15f);
                addHeight(100, 50, 12f);
                addHeight(100, 60, 14f);
                addHeight(100, 70, 11f);
                addHeight(100, 80, 12f);
                addHeight(100, 90, 16f);
                addHeight(100, 100, 12f);
                addHeight(110, 100, 12f);
                addHeight(110, 110, 13f);
                addHeight(120, 110, 17f);
                addHeight(130, 110, 14f);
                addHeight(140, 110, 11f);
                addHeight(150, 110, 11f);
                addHeight(160, 110, 11f);
                addHeight(170, 110, 11f);
                addHeight(140, 100, 11f);
                addHeight(150, 100, 11f);
                addHeight(160, 100, 11f);
                addHeight(170, 100, 11f);
                addHeight(180, 100, 14f);
                addHeight(190, 100, 15f);
                addHeight(190, 90, 15f);
                addHeight(190, 80, 14f);
                addHeight(190, 70, 16f);
                addHeight(190, 60, 10f);
                addHeight(190, 50, 16f);
                addHeight(180, 60, 18f);
                addHeight(180, 50, 16f);
                addHeight(180, 40, 15f);
                addHeight(170, 40, 12f);
                addHeight(160, 40, 14f);
                addHeight(150, 40, 14f);
                addHeight(140, 40, 17f);
                addHeight(130, 40, 15f);
                addHeight(120, 40, 14f);
                addHeight(110, 40, 17f);

                // third ring
                var r = new Random(0);
                addHeight(110, 50, r.Next(25, 35));
                addHeight(110, 60, r.Next(25, 35));
                addHeight(110, 70, r.Next(25, 35));
                addHeight(110, 80, r.Next(25, 35));
                addHeight(110, 90, r.Next(25, 35));
                addHeight(120, 90, r.Next(25, 35));
                addHeight(120, 100, r.Next(25, 35));
                addHeight(130, 90, r.Next(25, 35));
                addHeight(130, 100, 50f);
                addHeight(140, 90, r.Next(25, 35));
                addHeight(150, 90, r.Next(25, 35));
                addHeight(160, 90, r.Next(25, 35));
                addHeight(170, 90, r.Next(25, 35));
                addHeight(180, 90, r.Next(25, 35));
                addHeight(180, 80, r.Next(25, 35));
                addHeight(180, 70, r.Next(25, 35));
                addHeight(170, 70, r.Next(25, 35));
                addHeight(170, 60, r.Next(25, 35));
                addHeight(170, 50, r.Next(25, 35));
                addHeight(160, 50, r.Next(25, 35));
                addHeight(150, 50, r.Next(25, 35));
                addHeight(140, 50, r.Next(25, 35));
                addHeight(130, 50, r.Next(25, 35));
                addHeight(120, 50, r.Next(25, 35));

                // fourth ring
                addHeight(120, 60, 45f);
                addHeight(120, 70, 42f);
                addHeight(120, 80, 42f);
                addHeight(130, 60, 46f);
                addHeight(130, 70, 30f);
                addHeight(130, 80, 45f);
                addHeight(140, 60, 47f);
                addHeight(140, 70, 30f);
                addHeight(140, 80, 44f);
                addHeight(150, 60, 45f);
                addHeight(150, 70, 30f);
                addHeight(150, 80, 44f);
                addHeight(160, 60, 43f);
                addHeight(160, 70, 45f);
                addHeight(160, 80, 45f);
            }

            // fill rest
            {
                for (int x = 0; x <= sizeX; x += tileSize)
                {
                    for (int y = 0; y <= sizeY; y += tileSize)
                    {
                        addHeight(x, y, 0f);
                    }
                }
            }
        }

        protected override void CreateGeometryInternal()
        {
            PopulateHeights();

            Vector3 getVertexPos(int x, int y)
                => new Vector3(x - sizeX / 2f, _heights[new Point(x, y)], y - sizeY / 2f);

            for (int x = 0; x < sizeX; x += tileSize)
            {
                for (int y = 0; y < sizeY; y += tileSize)
                {
                    Geometry.AddVertices(RectangleComposer.Create(new[]
                    {
                        getVertexPos(x, y),
                        getVertexPos(x + tileSize, y),
                        getVertexPos(x, y + tileSize),
                        getVertexPos(x + tileSize, y + tileSize),
                    }, new TextureRectangle(x / (float)sizeX, y / (float)sizeY, tileSize / (float)sizeX, tileSize / (float)sizeY)));
                }
            }
        }
    }
}
