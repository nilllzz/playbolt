﻿using Microsoft.Xna.Framework;

namespace PlayBolt
{
    internal abstract class Screen
    {
        internal virtual void LoadContent() { }
        internal virtual void UnloadContent() { }
        internal virtual void Activate() { }
        internal virtual void Deactivate() { }
        internal abstract void Draw(GameTime gameTime);
        internal abstract void Update(GameTime gameTime);
        internal virtual void DeselectedWindow() { }
    }
}
