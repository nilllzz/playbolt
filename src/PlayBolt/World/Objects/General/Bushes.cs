﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.General
{
    class Bushes : WorldObject
    {
        private int _size;

        public Bushes(WorldScreen screen, Vector3 position, bool rotated, int size)
            : base(screen, "general_bushes", position)
        {
            _rotation = new Vector3(0, rotated ? MathHelper.PiOver2 : 0, 0);
            _size = size;
            IsOpaque = false;
        }

        protected override void CreateGeometry()
        {
            ITextureDefintion texture = null;
            switch (_size)
            {
                case 1:
                    texture = new TextureRectangle(new Rectangle(208, 48, 32, 32), Texture);
                    break;
                case 2:
                    texture = new TextureRectangle(new Rectangle(176, 80, 64, 32), Texture);
                    break;
                case 3:
                    texture = new TextureRectangle(new Rectangle(80, 80, 96, 32), Texture);
                    break;
                case 4:
                    texture = new TextureRectangle(new Rectangle(80, 48, 128, 32), Texture);
                    break;
            }

            var vertices = RectangleComposer.Create(_size * 10, 10, texture);
            VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, 0, 0));
            Geometry.AddVertices(vertices);
        }
    }
}
