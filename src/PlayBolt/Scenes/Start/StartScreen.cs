﻿using GameDevCommon.Drawing;
using GameDevCommon.Input;
using GameDevCommon.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PlayBolt.Scenes.InGame;
using PlayBolt.Scenes.Menu;
using PlayBolt.World;
using System.Linq;
using static Core;

namespace PlayBolt.Scenes.Start
{
    class StartScreen : WorldScreen
    {
        private StartCamera _camera;
        private Shader _shader;
        private SpriteFont _font;
        private SpriteBatch _batch;
        private int _selection = 0;
        private ParkConfig _park;
        private float _outro = 1f;
        private bool _selected = false;

        public override PerspectiveCamera Camera => _camera;
        public override Shader Shader => _shader;

        public StartScreen()
        {
            _camera = new StartCamera();
        }

        internal override void LoadContent()
        {
            base.LoadContent();

            _shader = new BasicShader();
            _park = new ParkConfig();
            LoadLevel();

            _font = Controller.Content.Load<SpriteFont>("Fonts/Font");
            _batch = new SpriteBatch(Controller.GraphicsDevice);
        }

        private void LoadLevel()
        {
            _objects = ParkConstructor.GetPark(this, _park);
            _objects.ForEach(o => o.LoadContent());
            ((OptimizableRenderObjectCollection)_objects).Optmimize<VertexPositionNormalTexture>();
        }

        internal override void Draw(GameTime gameTime)
        {
            Controller.GraphicsDevice.ResetFull();
            Controller.GraphicsDevice.ClearFull(Color.SkyBlue);

            _shader.Prepare(Camera);

            Controller.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            for (int i = 0; i < _objects.OpaqueObjects.Count(); i++)
                _shader.Render(_objects.OpaqueObjects.ElementAt(i));

            Controller.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;
            for (int i = 0; i < _objects.TransparentObjects.Count(); i++)
                _shader.Render(_objects.TransparentObjects.ElementAt(i));

            _batch.Begin(SpriteBatchUsage.Default);

            _batch.DrawString(_font, ">",
                new Vector2(GameController.RENDER_WIDTH - 430, GameController.RENDER_HEIGHT - 180 + 60 * _selection), Color.Black,
                0f, Vector2.Zero, 2f, SpriteEffects.None, 0f);
            _batch.DrawString(_font, "Start Game",
                new Vector2(GameController.RENDER_WIDTH - 400, GameController.RENDER_HEIGHT - 180), Color.Black,
                0f, Vector2.Zero, 2f, SpriteEffects.None, 0f);
            _batch.DrawString(_font, "Play Demo",
                new Vector2(GameController.RENDER_WIDTH - 400, GameController.RENDER_HEIGHT - 120), Color.Black,
                0f, Vector2.Zero, 2f, SpriteEffects.None, 0f);

            _batch.DrawRectangle(new Rectangle(0, 0, GameController.RENDER_WIDTH, GameController.RENDER_HEIGHT), new Color(255, 255, 255, (int)(255 * (1f - _outro))));

            _batch.End();
        }

        internal override void Update(GameTime gameTime)
        {
            Camera.Update();

            _objects.Sort();

            var objs = (OptimizableRenderObjectCollection)_objects;
            for (int i = 0; i < objs.OriginalObjects.Count(); i++)
                objs.OriginalObjects.ElementAt(i).Update();

            if (!_selected)
            {
                var controls = GetComponent<ControlsHandler>();
                if (controls.DownPressed(PlayerIndex.One))
                {
                    _selection++;
                    if (_selection == 2)
                        _selection = 0;
                }
                if (controls.UpPressed(PlayerIndex.One))
                {
                    _selection--;
                    if (_selection == -1)
                        _selection = 1;
                }
                var keyboard = GetComponent<KeyboardHandler>();
                var gamepad = GetComponent<GamePadHandler>();
                if (keyboard.KeyPressed(Keys.Enter) ||
                    keyboard.KeyPressed(Keys.Space) ||
                    gamepad.ButtonPressed(PlayerIndex.One, Buttons.A))
                {
                    _selected = true;
                }
            }
            else
            {
                _outro = MathHelper.Lerp(_outro, 0f, 0.05f);
                if (_outro <= 0.002f)
                {
                    _outro = 0f;
                    FadeTransitionScreen transitionScreen = null;
                    switch (_selection)
                    {
                        case 0:
                            var gjScreen = new GameJoltLoginScreen();
                            gjScreen.LoadContent();
                            transitionScreen = new FadeTransitionScreen(this, gjScreen);
                            break;
                        case 1:
                            var gameScreen = new DemoInGameScreen(this);
                            gameScreen.LoadContent();
                            transitionScreen = new FadeTransitionScreen(this, gameScreen);
                            break;
                    }
                    transitionScreen.LoadContent();
                    GetComponent<ScreenManager>().SetScreen(transitionScreen);
                }
                _camera.Position -= new Vector3(-0.2f, -0.1f, 1) * (1f - _outro) * 0.05f;
                _park.EntryBarrierOpened = 1f - _outro;
            }
        }
    }
}
