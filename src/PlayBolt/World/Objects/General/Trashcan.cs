﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.General
{
    class Trashcan : WorldObject
    {
        public Trashcan(WorldScreen screen, Vector3 position)
            : base(screen, "general_trashcan", position)
        { }

        protected override void CreateGeometryInternal()
        {
            var texture = new TextureCuboidWrapper();
            texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right, CuboidSide.Front, CuboidSide.Back },
                new TextureRectangle(new Rectangle(112, 0, 16, 16), Texture));
            texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                new TextureRectangle(new Rectangle(128, 0, 16, 16), Texture));
            var vertices = CuboidComposer.Create(1f, 1.3f, 1f, texture);
            Geometry.AddVertices(vertices);
        }
    }
}
