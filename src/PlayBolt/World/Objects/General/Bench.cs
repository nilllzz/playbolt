﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.General
{
    class Bench : WorldObject
    {
        public Bench(WorldScreen screen, Vector3 position, float rotation)
            : base(screen, "general_bench", position)
        {
            _rotation = new Vector3(0, rotation, 0);
        }

        protected override void CreateGeometryInternal()
        {
            // seat
            {
                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(144, 0, 32, 16), Texture));
                texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back, CuboidSide.Left, CuboidSide.Right },
                    new TextureRectangle(new Rectangle(144, 0, 32, 1), Texture));

                var vertices = CuboidComposer.Create(3f, 0.2f, 1.5f, texture);
                Geometry.AddVertices(vertices);
            }
            // back
            {
                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(144, 4, 32, 12), Texture));
                texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back, CuboidSide.Left, CuboidSide.Right },
                    new TextureRectangle(new Rectangle(144, 0, 32, 1), Texture));

                var vertices = CuboidComposer.Create(3f, 0.2f, 1f, texture);
                VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver4 * 1.7f, 0, 0));
                VertexTransformer.Offset(vertices, new Vector3(0, 1f, -1f));
                Geometry.AddVertices(vertices);
            }
            // supports
            {
                var vertices = CuboidComposer.Create(0.1f, 0.1f, 0.84f,
                    new TextureRectangle(new Rectangle(144, 3, 1, 1), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver4 * 1.7f, 0, 0));
                VertexTransformer.Offset(vertices, new Vector3(-1f, 0.3f, -0.8f));
                Geometry.AddVertices(vertices);
                VertexTransformer.Offset(vertices, new Vector3(2f, 0f, 0f));
                Geometry.AddVertices(vertices);
            }
            // feet
            {
                ITextureDefintion getTexture()
                {
                    var texture = new TextureCuboidWrapper();
                    texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                        new TextureRectangle(new Rectangle(176, 8, 4, 4), Texture));
                    texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back, CuboidSide.Left, CuboidSide.Right },
                        new TextureRectangle(new Rectangle(176, 0, 4, 8), Texture));
                    return texture;
                }
                // 1
                {
                    var vertices = CuboidComposer.Create(0.2f, 1f, 0.2f, getTexture());
                    VertexTransformer.Offset(vertices, new Vector3(-1.3f, -0.5f, -0.5f));
                    Geometry.AddVertices(vertices);
                }
                // 2
                {
                    var vertices = CuboidComposer.Create(0.2f, 1f, 0.2f, getTexture());
                    VertexTransformer.Offset(vertices, new Vector3(1.3f, -0.5f, -0.5f));
                    Geometry.AddVertices(vertices);
                }
                // 3
                {
                    var vertices = CuboidComposer.Create(0.2f, 1f, 0.2f, getTexture());
                    VertexTransformer.Offset(vertices, new Vector3(-1.3f, -0.5f, 0.5f));
                    Geometry.AddVertices(vertices);
                }
                // 4
                {
                    var vertices = CuboidComposer.Create(0.2f, 1f, 0.2f, getTexture());
                    VertexTransformer.Offset(vertices, new Vector3(1.3f, -0.5f, 0.5f));
                    Geometry.AddVertices(vertices);
                }
            }
        }
    }
}
