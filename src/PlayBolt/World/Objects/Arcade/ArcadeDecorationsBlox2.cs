﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Arcade
{
    class ArcadeDecorationsBlox2 : WorldObject
    {
        public ArcadeDecorationsBlox2(WorldScreen screen, Vector3 position)
            : base(screen, "arcade_decorations_blox_2", position)
        {
            IsOpaque = false;
            IsOptimizable = false;
        }

        protected override void CreateGeometryInternal()
        {
            var vertices = RectangleComposer.Create(4, 6,
                new TextureRectangle(new Rectangle(576, 112, 16, 24), Texture));
            VertexTransformer.Rotate(vertices, new Vector3(0, -0.5f, MathHelper.PiOver2));
            Geometry.AddVertices(vertices);
        }
    }
}
