﻿using GameDevCommon.Rendering;

namespace PlayBolt.World
{
    abstract class WorldScreen : Screen
    {
        public abstract PerspectiveCamera Camera { get; }
        public abstract Shader Shader { get; }

        protected RenderObjectCollection<I3DObject> _objects;
    }
}
