﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.General
{
    class Lamppost : WorldObject
    {
        public Lamppost(WorldScreen screen, Vector3 position)
            : base(screen, "general_lamppost", position)
        { }

        protected override void CreateGeometryInternal()
        {
            // lower base
            {
                var vertices = CuboidComposer.Create(1f, 0.2f, 1f,
                    new TextureRectangle(new Rectangle(80, 0, 1, 1), Texture));
                Geometry.AddVertices(vertices);
            }
            // upper base
            {
                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right, CuboidSide.Front, CuboidSide.Back },
                    new TextureRectangle(new Rectangle(80, 0, 1, 1), Texture));
                texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(96, 0, 16, 16), Texture));
                var vertices = CuboidComposer.Create(0.75f, 0.5f, 0.75f, texture);
                VertexTransformer.Offset(vertices, new Vector3(0, 0.2f, 0));
                Geometry.AddVertices(vertices);
            }
            // post
            {
                var vertices = CuboidComposer.Create(0.3f, 5f, 0.3f,
                    new TextureRectangle(new Rectangle(80, 0, 1, 1), Texture));
                VertexTransformer.Offset(vertices, new Vector3(0, 2.5f, 0));
                Geometry.AddVertices(vertices);
            }
            // lamp
            {
                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right, CuboidSide.Front, CuboidSide.Back },
                    new TextureRectangle(new Rectangle(80, 0, 16, 16), Texture));
                texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(96, 0, 16, 16), Texture));
                var vertices = CuboidComposer.Create(new[]
                {
                    new Vector3(-0.75f, 7f, -0.75f),
                    new Vector3(-0.75f, 7f, 0.75f),
                    new Vector3(0.75f, 7f, 0.75f),
                    new Vector3(0.75f, 7f, -0.75f),

                    new Vector3(-0.5f, 5f, -0.5f),
                    new Vector3(-0.5f, 5f, 0.5f),
                    new Vector3(0.5f, 5f, 0.5f),
                    new Vector3(0.5f, 5f, -0.5f),
                }, texture);
                Geometry.AddVertices(vertices);
            }
        }
    }
}
