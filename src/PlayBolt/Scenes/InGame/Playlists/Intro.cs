﻿namespace PlayBolt.Scenes.InGame.Playlists
{
    class Intro : Playlist
    {
        public override int Id => 0;
        public override string Name => "Intro";
        public override string Description => "Intro games. Start off simple with a variety of themes.";
        public override string Scoreboard => "282032";
    }
}
