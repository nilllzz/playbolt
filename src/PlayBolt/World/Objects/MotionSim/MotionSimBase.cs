﻿using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.MotionSim
{
    class MotionSimBase : WorldObject
    {
        public MotionSimBase(WorldScreen screen, Vector3 position)
            : base(screen, "motionsim_base", position)
        { }

        protected override void CreateGeometryInternal()
        {
            var texture = new TextureCuboidWrapper();
            texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back, CuboidSide.Left, CuboidSide.Right },
                new TextureRectangle(new Rectangle(400, 64, 64, 8), Texture));
            texture.AddSide(new[] { CuboidSide.Bottom, CuboidSide.Top },
                new TextureRectangle(new Rectangle(400, 0, 64, 64), Texture));
            var vertices = CuboidComposer.Create(32, 4, 32, texture);
            Geometry.AddVertices(vertices);
        }
    }
}
