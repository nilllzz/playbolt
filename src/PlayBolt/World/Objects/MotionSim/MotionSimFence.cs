﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.MotionSim
{
    class MotionSimFence : WorldObject
    {
        public MotionSimFence(WorldScreen screen, Vector3 position, bool rotated)
            : base(screen, "motionsim_fence", position)
        {
            _rotation = new Vector3(0, rotated ? MathHelper.PiOver2 : 0, 0);
        }

        protected override void CreateGeometryInternal()
        {
            // upright planks
            {
                ITextureDefintion getTexture()
                {
                    var texture = new TextureCuboidWrapper();
                    texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                        new TextureRectangle(new Rectangle(464, 0, 8, 32), Texture));
                    texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                        new TextureRectangle(new Rectangle(464, 0, 1, 32), Texture));
                    texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                        new TextureRectangle(new Rectangle(464, 0, 8, 1), Texture));
                    return texture;
                }

                // left plank
                {
                    var vertices = CuboidComposer.Create(0.5f, 2f, 0.1f, getTexture());
                    VertexTransformer.Offset(vertices, new Vector3(-1f, 0f, 0f));
                    Geometry.AddVertices(vertices);
                }
                // right plank
                {
                    var vertices = CuboidComposer.Create(0.5f, 2f, 0.1f, getTexture());
                    VertexTransformer.Offset(vertices, new Vector3(1f, 0f, 0f));
                    Geometry.AddVertices(vertices);
                }
            }
            // connecting planks
            {
                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                    new TextureRectangle(new Rectangle(400, 72, 64, 8), Texture));
                texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                    new TextureRectangle(new Rectangle(400, 72, 1, 8), Texture));
                texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(400, 72, 64, 1), Texture));

                var vertices = CuboidComposer.Create(4f, 0.5f, 0.1f, texture);
                VertexTransformer.Offset(vertices, new Vector3(0f, 0.5f, -0.1f));
                Geometry.AddVertices(vertices);
            }
        }
    }
}
