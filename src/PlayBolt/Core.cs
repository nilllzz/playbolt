﻿using Microsoft.Xna.Framework;
using PlayBolt;
using System;
using System.Linq;

class Core
{
    internal static GameController Controller { get; private set; }

    internal static T GetComponent<T>() where T : IGameComponent
        => Controller.ComponentManager.GetComponent<T>();

    [STAThread]
    public static void Main(string[] args)
    {
        using (Controller = new GameController())
            Controller.Run();
    }
}

