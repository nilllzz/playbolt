﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Arcade
{
    class ArcadeDecorationsAlien1 : WorldObject
    {
        public ArcadeDecorationsAlien1(WorldScreen screen, Vector3 position)
            : base(screen, "arcade_decorations_alien_1", position)
        {
            IsOpaque = false;
            IsOptimizable = false;
        }

        protected override void CreateGeometryInternal()
        {
            var vertices = RectangleComposer.Create(6, 5,
                new TextureRectangle(new Rectangle(592, 48, 12, 10), Texture));
            VertexTransformer.Rotate(vertices, new Vector3(0, -0.9f, MathHelper.PiOver2));
            Geometry.AddVertices(vertices);
        }
    }
}
