﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Backdrop
{
    class Sunset : WorldObject
    {
        public Sunset(WorldScreen screen, Vector3 position)
            : base(screen, "backdrop_sunset", position)
        {
            IsOptimizable = false;
            IsOpaque = false;
        }

        public override void LoadContent()
        {
            LoadTexture("Textures/World/sunset.png");
        }

        protected override void CreateGeometryInternal()
        {
            var vertices = RectangleComposer.Create(1800, 1000);
            VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, 0, 0));
            Geometry.AddVertices(vertices);
        }
    }
}
