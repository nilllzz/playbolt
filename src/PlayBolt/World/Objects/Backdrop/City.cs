﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Backdrop
{
    class City : WorldObject
    {
        public City(WorldScreen screen, Vector3 position)
            : base(screen, "backdrop_city", position)
        {
            IsOptimizable = false;
            IsOpaque = false;
        }

        public override void LoadContent()
        {
            LoadTexture("Textures/World/city.png");
        }

        protected override void CreateGeometryInternal()
        {
            var vertices = RectangleComposer.Create(512, 256);
            VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, 0, 0));
            Geometry.AddVertices(vertices);
        }
    }
}
