﻿using GameDevCommon.Input;
using JoltNet;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PlayBolt.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Core;

namespace PlayBolt.Scenes.InGame.Games
{
    abstract class Microgame
    {
        protected const int WIDTH = 900, HEIGHT = 500;
        private const string COMPLETED_GAMES_KEY = "completed_games";

        private static List<int> _completedGames;
        protected static Random _random = new Random();

        public static async Task LoadCompletedGames()
        {
            var storageRequest = RequestProvider.Storage.Fetch(COMPLETED_GAMES_KEY, Controller.GameJoltUsername, Controller.GameJoltToken);
            await Controller.GameJoltClient.ExecuteRequestAsync(storageRequest);

            if (!storageRequest.Response.Success)
                _completedGames = new List<int>();
            else
            {
                _completedGames = storageRequest.Response.Data.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(str => Convert.ToInt32(str)).ToList();
            }
        }

        public async Task CompletedGame()
        {
            if (!_completedGames.Contains(Id))
            {
                IGameJoltRequest request;
                if (_completedGames.Count == 0)
                {
                    request = RequestProvider.Storage.Set(COMPLETED_GAMES_KEY, Id.ToString(), Controller.GameJoltUsername, Controller.GameJoltToken);
                }
                else
                {
                    request = RequestProvider.Storage.Update(COMPLETED_GAMES_KEY, StorageUpdateOperation.Append,
                        "," + Id.ToString(), Controller.GameJoltUsername, Controller.GameJoltToken);
                }
                await Controller.GameJoltClient.ExecuteRequestAsync(request);

                // update local table
                _completedGames.Add(Id);
                // unlock trophies:
                switch (_completedGames.Count)
                {
                    case 1:
                        GetComponent<TrophyManager>().UnlockTrophy(TrophyManager.PARTICIPATION_ID);
                        break;
                    case 10:
                        GetComponent<TrophyManager>().UnlockTrophy(TrophyManager.AWARD_ID);
                        break;
                    case 50:
                        GetComponent<TrophyManager>().UnlockTrophy(TrophyManager.RIBBON_ID);
                        break;
                    case 100:
                        GetComponent<TrophyManager>().UnlockTrophy(TrophyManager.TROPHY_ID);
                        break;
                    default:
                        if (_completedGames.Count == GetType().Assembly.GetTypes()
                            .Where(t => Attribute.GetCustomAttribute(t, typeof(MicrogameDescriptionAttribute)) != null).Count())
                        {
                            GetComponent<TrophyManager>().UnlockTrophy(TrophyManager.HIGHESTHONOR_ID);
                        }
                        break;
                }
            }
        }

        public bool IsCompleted => _completedGames.Contains(Id);

        private MicrogameDescriptionAttribute GetDescriptor()
            => (MicrogameDescriptionAttribute)Attribute.GetCustomAttribute(GetType(), typeof(MicrogameDescriptionAttribute));

        public int Id => GetDescriptor().Id;
        public string Name => GetDescriptor().Name;
        public string Description => GetDescriptor().Description;
        public string Playlist => GetDescriptor().Playlist;

        public Difficulty Difficulty { get; set; }
        public float Speed { get; set; } = 1f;
        public WinState WinState { get; protected set; } = WinState.Default;

        public abstract float CompletionTime { get; }
        public abstract string Command { get; }
        public virtual Color CommandColor { get; } = Color.Black;
        public abstract string Scoretable { get; }
        public abstract int TargetScore { get; }
        public virtual string Thumbnail => "TEMP.png";

        protected Rectangle Bounds => new Rectangle(0, 0, WIDTH, HEIGHT);

        protected SpriteBatch _batch;
        protected Texture2D _texture;

        public Microgame Create()
            => (Microgame)Activator.CreateInstance(GetType());

        public virtual void LoadContent()
        {
            _batch = new SpriteBatch(Controller.GraphicsDevice);
            _texture = GetComponent<Resources>().LoadTexture("Textures/Games/Resources/" + Id.ToString() + ".png", false);
        }

        public virtual void UnloadContent()
        {
            _batch.Dispose();
            _texture.Dispose();
        }

        protected bool ActionDown()
        {
            var keyboard = GetComponent<KeyboardHandler>();
            var gamepad = GetComponent<GamePadHandler>();
            return keyboard.KeyDown(Keys.Enter) ||
                keyboard.KeyDown(Keys.Space) ||
                gamepad.ButtonDown(PlayerIndex.One, Buttons.A);
        }

        protected bool ActionPressed()
        {
            var keyboard = GetComponent<KeyboardHandler>();
            var gamepad = GetComponent<GamePadHandler>();
            return keyboard.KeyPressed(Keys.Enter) ||
                keyboard.KeyPressed(Keys.Space) ||
                gamepad.ButtonPressed(PlayerIndex.One, Buttons.A);
        }

        protected bool DirectionDown(InputDirection direction)
        {
            var controls = GetComponent<ControlsHandler>();
            var inputs = new[] { InputDirectionType.ArrowKeys, InputDirectionType.DPad, InputDirectionType.LeftThumbStick, InputDirectionType.WASD };
            switch (direction)
            {
                case InputDirection.Up:
                    return controls.UpDown(PlayerIndex.One, inputs);
                case InputDirection.Left:
                    return controls.LeftDown(PlayerIndex.One, inputs);
                case InputDirection.Down:
                    return controls.DownDown(PlayerIndex.One, inputs);
                case InputDirection.Right:
                    return controls.RightDown(PlayerIndex.One, inputs);
                default:
                    throw new NotImplementedException();
            }
        }

        protected bool DirectionPressed(InputDirection direction)
        {
            var controls = GetComponent<ControlsHandler>();
            var inputs = new[] { InputDirectionType.ArrowKeys, InputDirectionType.DPad, InputDirectionType.LeftThumbStick, InputDirectionType.WASD };
            switch (direction)
            {
                case InputDirection.Up:
                    return controls.UpPressed(PlayerIndex.One, inputs);
                case InputDirection.Left:
                    return controls.LeftPressed(PlayerIndex.One, inputs);
                case InputDirection.Down:
                    return controls.DownPressed(PlayerIndex.One, inputs);
                case InputDirection.Right:
                    return controls.RightPressed(PlayerIndex.One, inputs);
                default:
                    throw new NotImplementedException();
            }
        }

        public abstract void Draw(GameTime gameTime);
        public abstract void Update(GameTime gameTime);
        public virtual void TimeUp() { }
        public virtual void GameStart() { }
    }
}
