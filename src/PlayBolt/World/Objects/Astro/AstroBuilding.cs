﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Astro
{
    class AstroBuilding : WorldObject
    {
        public AstroBuilding(WorldScreen screen, Vector3 position)
            : base(screen, "astro_building", position)
        { }

        protected override void CreateGeometryInternal()
        {
            // building
            {
                var vertices = CylinderComposer.Create(26, 10, 20,
                    new TextureRectangle(new Rectangle(336, 0, 32, 32), Texture),
                    new TextureRectangle(new Rectangle(320, 32, 16, 16), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(0, 0, MathHelper.PiOver2));
                Geometry.AddVertices(vertices);
            }
            // entrance
            {
                // left pole
                {
                    var texture = new TextureCuboidWrapper();
                    texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                        new TextureRectangle(new Rectangle(336, 32, 32, 64), Texture));
                    texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back, CuboidSide.Top, CuboidSide.Bottom },
                        new TextureRectangle(new Rectangle(336, 32, 1, 1), Texture));
                    var vertices = CuboidComposer.Create(4, 8, 4, texture);
                    VertexTransformer.Rotate(vertices, new Vector3(0, MathHelper.Pi, 0));
                    VertexTransformer.Offset(vertices, new Vector3(25, -1, 4));
                    Geometry.AddVertices(vertices);
                }
                // right pole
                {
                    var texture = new TextureCuboidWrapper();
                    texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                        new TextureRectangle(new Rectangle(336, 32, 32, 64), Texture));
                    texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back, CuboidSide.Top, CuboidSide.Bottom },
                        new TextureRectangle(new Rectangle(336, 32, 1, 1), Texture));
                    var vertices = CuboidComposer.Create(4, 8, 4, texture);
                    VertexTransformer.Rotate(vertices, new Vector3(0, MathHelper.Pi, 0));
                    VertexTransformer.Offset(vertices, new Vector3(25, -1, -4));
                    Geometry.AddVertices(vertices);
                }
                // door
                {
                    var vertices = CuboidComposer.Create(1, 6, 4,
                        new TextureRectangle(new Rectangle(368, 48, 32, 48), Texture));
                    VertexTransformer.Rotate(vertices, new Vector3(0, MathHelper.Pi, 0));
                    VertexTransformer.Offset(vertices, new Vector3(26, -2, 0));
                    Geometry.AddVertices(vertices);
                }
                // above door
                {
                    var texture = new TextureCuboidWrapper();
                    texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                        new TextureRectangle(new Rectangle(368, 32, 32, 16), Texture));
                    texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                        new TextureRectangle(new Rectangle(368, 32, 32, 1), Texture));
                    texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                        new TextureRectangle(new Rectangle(336, 32, 1, 1), Texture));
                    var vertices = CuboidComposer.Create(4, 2, 4, texture);
                    VertexTransformer.Offset(vertices, new Vector3(25, 2, 0));
                    Geometry.AddVertices(vertices);
                }
            }
        }
    }
}
