﻿namespace PlayBolt.Scenes.InGame.Games
{
    enum Difficulty
    {
        Easy = 0,
        Medium = 1,
        Hard = 2
    }
}
