﻿using System;

namespace PlayBolt.Scenes.InGame.Games
{
    class MicrogameDescriptionAttribute : Attribute
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string Playlist { get; private set; }

        public MicrogameDescriptionAttribute(int id, string name, string playlist, string description)
        {
            Id = id;
            Name = name;
            Description = description;
            Playlist = playlist;
        }
    }
}
