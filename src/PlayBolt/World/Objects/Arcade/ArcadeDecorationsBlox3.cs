﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Arcade
{
    class ArcadeDecorationsBlox3 : WorldObject
    {
        public ArcadeDecorationsBlox3(WorldScreen screen, Vector3 position)
            : base(screen, "arcade_decorations_blox_3", position)
        {
            IsOpaque = false;
            IsOptimizable = false;
        }

        protected override void CreateGeometryInternal()
        {
            var vertices = RectangleComposer.Create(6, 4,
                new TextureRectangle(new Rectangle(592, 128, 24, 16), Texture));
            VertexTransformer.Rotate(vertices, new Vector3(0, -MathHelper.PiOver4, MathHelper.PiOver2));
            Geometry.AddVertices(vertices);
        }
    }
}
