﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;
using System;

namespace PlayBolt.World.Objects.Pond
{
    class Bridge : WorldObject
    {
        public Bridge(WorldScreen screen, Vector3 position)
            : base(screen, "pond_bridge", position)
        { }

        protected override void CreateGeometryInternal()
        {
            for (int i = -5; i < 6; i++)
            {
                var y = (1f - Math.Abs(i / 5f)) * 2;
                // floor boards
                {
                    var texture = new TextureCuboidWrapper();
                    texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                        new TextureRectangle(new Rectangle(240, 0, 16, 5), Texture));
                    texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back, CuboidSide.Left, CuboidSide.Right },
                        new TextureRectangle(new Rectangle(240, 0, 16, 1), Texture));
                    var vertices = CuboidComposer.Create(5f, 0.2f, 1.4f, texture);
                    VertexTransformer.Offset(vertices, new Vector3(0, y, i * 2));
                    Geometry.AddVertices(vertices);
                }
                // sides
                {
                    ITextureDefintion getTexture()
                    {
                        var texture = new TextureCuboidWrapper();
                        texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                            new TextureRectangle(new Rectangle(176, 8, 4, 4), Texture));
                        texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back, CuboidSide.Left, CuboidSide.Right },
                            new TextureRectangle(new Rectangle(176, 0, 4, 8), Texture));
                        return texture;
                    }

                    // left
                    {
                        var vertices = CuboidComposer.Create(0.2f, 14f, 0.2f, getTexture());
                        VertexTransformer.Offset(vertices, new Vector3(-2.35f, -3f + y, i * 2));
                        Geometry.AddVertices(vertices);
                    }
                    // right
                    {
                        var vertices = CuboidComposer.Create(0.2f, 14f, 0.2f, getTexture());
                        VertexTransformer.Offset(vertices, new Vector3(2.35f, -3f + y, i * 2));
                        Geometry.AddVertices(vertices);
                    }
                }
            }

        }
    }
}
