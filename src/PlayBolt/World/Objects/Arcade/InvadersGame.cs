﻿using GameDevCommon;
using GameDevCommon.Drawing;
using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayBolt.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using static Core;

namespace PlayBolt.World.Objects.Arcade
{
    class InvadersGame : WorldObject
    {
        private static Random _random = new Random();

        private SpriteFont _font;
        private Texture2D _texture;
        private SpriteBatch _batch;
        private RenderTarget2D _target;

        private class Enemy
        {
            public int X, Y;
            public bool Direction;
        }
        private class Bullet
        {
            public bool Hostile;
            public int X, Y;
        }
        private class Shield
        {
            public Shield()
            {
                Active = new bool[10, 10];
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        Active[i, j] = true;
                    }
                }
            }

            public bool[,] Active;
        }
        private class Explosion
        {
            public int X, Y;
            public int Stage = 0;
            public int Delay = 5;
            public void Update()
            {
                Delay--;
                if (Delay == 0)
                {
                    Delay = 5;
                    Stage++;
                }
            }
        }
        private List<Enemy> _enemies = new List<Enemy>();
        private List<Bullet> _bullets = new List<Bullet>();
        private List<Shield> _shields = new List<Shield>();
        private List<Explosion> _explosions = new List<Explosion>();
        private int _enemyMoveDelay = 10, _enemySprite = 0, _playerX = 60, _score = 0,
            _playerMoveDelay = 4, _playerShootDelay = 20, _playerTargetX = 60, _gameOverDelay = 200;
        private float _starOffset = 0f;
        private bool _gameOver = false, _nextDirection = false;

        public InvadersGame(WorldScreen screen, Vector3 position)
            : base(screen, "arcade_invaders", position)
        {
            IsOptimizable = false;
        }

        public override void LoadContent()
        {
            _font = Controller.Content.Load<SpriteFont>("Fonts/Font");
            _texture = GetComponent<Resources>().LoadTexture("Textures/World/main.png");
            _target = RenderTargetManager.CreateRenderTarget(128, 256);
            _batch = new SpriteBatch(Controller.GraphicsDevice);

            SetupNewGame();

            LoadTexture(new Color(170, 170, 170));
        }

        private void SetupNewGame()
        {
            _playerTargetX = 60;
            _gameOverDelay = 200;
            _gameOver = false;
            _enemies.Clear();
            _bullets.Clear();
            _shields.Clear();
            _explosions.Clear();
            _score = 0;
            _nextDirection = false;

            for (int y = 0; y < 4; y++)
            {
                for (int x = 0; x < 4; x++)
                {
                    _enemies.Add(new Enemy { X = x * 20 + 28, Y = y - 1, Direction = _nextDirection });
                }
                _nextDirection = !_nextDirection;
            }
            _shields.AddRange(new[] { new Shield(), new Shield(), new Shield() });
        }

        public override void Update()
        {
            if (_cameraDistance < 100f)
            {
                // update
                _starOffset += 0.5f;
                if (_starOffset >= 32f)
                {
                    _starOffset = 0f;
                }

                // enemy movement
                _enemyMoveDelay--;
                if (_enemyMoveDelay <= 0)
                {
                    _enemyMoveDelay = 13 - (_enemies.Max(e => e.Y) / 2);
                    _enemySprite++;
                    if (!_gameOver)
                    {
                        var turn = false;
                        foreach (var enemy in _enemies)
                        {
                            if (enemy.Y >= 0)
                            {
                                var x = 1;
                                if (!enemy.Direction)
                                    x = -1;
                                enemy.X += x;
                                if (enemy.X == 0 || enemy.X + 12 == 128)
                                {
                                    turn = true;
                                    foreach (var lineEnemy in _enemies)
                                    {
                                        lineEnemy.Direction = !lineEnemy.Direction;
                                    }
                                }
                            }
                        }
                        if (turn)
                        {
                            foreach (var enemy in _enemies)
                            {
                                enemy.Y++;
                            }
                            for (int x = 0; x < 4; x++)
                            {
                                _enemies.Add(new Enemy { X = x * 20 + 28, Y = -1, Direction = _nextDirection });
                            }
                            _nextDirection = !_nextDirection;
                        }
                    }
                }

                if (!_gameOver)
                {
                    if (_enemies.Where(e => e.Y >= 0).Count() == 0)
                    {
                        _score += 10000;
                        for (int x = 0; x < 4; x++)
                        {
                            _enemies.Add(new Enemy { X = x * 20 + 28, Y = 0, Direction = _nextDirection });
                        }
                    }

                    // enemy shoot
                    foreach (var enemy in _enemies)
                    {
                        if (enemy.Y >= 0)
                        {
                            if (_random.Next(0, 1000) == 0)
                            {
                                _bullets.Add(new Bullet
                                {
                                    Hostile = true,
                                    X = enemy.X + 6,
                                    Y = enemy.Y * 18 + 10
                                });
                            }
                        }
                    }

                    // player movement
                    _playerMoveDelay--;
                    if (_playerMoveDelay == 0)
                    {
                        if (_playerTargetX > _playerX)
                        {
                            _playerX++;
                        }
                        else if (_playerTargetX < _playerX)
                        {
                            _playerX--;
                        }

                        if (_playerTargetX == _playerX)
                        {
                            _playerMoveDelay = 100;
                            _playerTargetX = _random.Next(5, 116);
                        }
                        else
                        {
                            _playerMoveDelay = 2;
                        }
                    }

                    // player shooting
                    _playerShootDelay--;
                    if (_playerShootDelay == 0)
                    {
                        _playerShootDelay = _random.Next(10, 50);
                        _bullets.Add(new Bullet
                        {
                            Hostile = false,
                            X = _playerX + 3,
                            Y = 230
                        });
                    }


                    // bullets
                    for (int i = 0; i < _bullets.Count; i++)
                    {
                        var bullet = _bullets[i];
                        if (!bullet.Hostile)
                            bullet.Y--;
                        else
                            bullet.Y++;

                        if (bullet.Y < 0 || bullet.Y >= 256)
                        {
                            _bullets.Remove(bullet);
                            i--;
                        }
                        else
                        {
                            bool hitTarget = false;
                            for (int j = 0; j < _shields.Count; j++)
                            {
                                var shield = _shields[j];
                                var startX = 58 + ((j - 1) * 38);
                                var startY = 200;
                                if (new Rectangle(startX, startY, shield.Active.GetLength(0), shield.Active.GetLength(1)).Contains(bullet.X, bullet.Y))
                                {
                                    if (shield.Active[bullet.X - startX, bullet.Y - startY])
                                    {
                                        shield.Active[bullet.X - startX, bullet.Y - startY] = false;
                                        _bullets.Remove(bullet);
                                        i--;
                                        hitTarget = true;
                                        if (!bullet.Hostile)
                                        {
                                            _score += 10;
                                        }
                                    }
                                    break;
                                }
                            }

                            if (!hitTarget)
                            {
                                if (!bullet.Hostile)
                                {
                                    for (int j = 0; j < _enemies.Count; j++)
                                    {
                                        var enemy = _enemies[j];
                                        if (enemy.Y >= 0 && new Rectangle(enemy.X, enemy.Y * 18, 12, 10).Contains(bullet.X, bullet.Y))
                                        {
                                            _enemies.Remove(enemy);
                                            _bullets.Remove(bullet);
                                            _explosions.Add(new Explosion
                                            {
                                                X = enemy.X - 2,
                                                Y = enemy.Y * 18 - 3
                                            });
                                            i--;
                                            _score += enemy.Y * 50 + 50;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    if (new Rectangle(_playerX, 230, 7, 9).Contains(bullet.X, bullet.Y))
                                    {
                                        _bullets.Remove(bullet);
                                        i--;
                                        _gameOver = true;
                                        _explosions.Add(new Explosion
                                        {
                                            X = _playerX - 4,
                                            Y = 228
                                        });
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    // player intersects with enemy / enemy goes off screen
                    {
                        foreach (var enemy in _enemies)
                        {
                            var y = enemy.Y * 18;
                            if (y >= 256 || new Rectangle(enemy.X, y, 12, 10).Intersects(new Rectangle(_playerX, 230, 7, 9)))
                            {
                                _gameOver = true;
                                _explosions.Add(new Explosion
                                {
                                    X = _playerX - 4,
                                    Y = 228
                                });
                            }
                        }
                    }
                }
                else
                {
                    _gameOverDelay--;
                    if (_gameOverDelay == 0)
                    {
                        SetupNewGame();
                    }
                }

                for (int i = 0; i < _explosions.Count; i++)
                {
                    var explosion = _explosions[i];
                    explosion.Update();
                    if (explosion.Stage == 4)
                    {
                        _explosions.Remove(explosion);
                        i--;
                    }
                }

                // draw
                RenderTargetManager.BeginRenderToTarget(_target);
                Controller.GraphicsDevice.Clear(Color.Transparent);
                _batch.Begin(SpriteBatchUsage.Default);

                for (int x = 0; x < 128; x += 32)
                {
                    for (int y = -32; y < 256; y += 32)
                    {
                        _batch.Draw(_texture, new Rectangle(x, (int)(y + _starOffset), 32, 32), new Rectangle(592, 16, 32, 32), Color.White);
                    }
                }

                foreach (var enemy in _enemies)
                {
                    _batch.Draw(_texture, new Rectangle(enemy.X, enemy.Y * 18, 12, 10),
                        new Rectangle(592 + ((_enemySprite % 2) * 12), 0, 12, 10), Color.White);
                }

                for (int i = 0; i < _shields.Count; i++)
                {
                    var startX = 58 + ((i - 1) * 38);
                    var shield = _shields[i];
                    for (int x = 0; x < shield.Active.GetLength(0); x++)
                    {
                        for (int y = 0; y < shield.Active.GetLength(1); y++)
                        {
                            if (shield.Active[x, y])
                            {
                                _batch.DrawRectangle(new Rectangle(x + startX, 200 + y, 1, 1), new Color(0, 218, 0));
                            }
                        }
                    }
                }

                foreach (var bullet in _bullets)
                {
                    var color = new Color(255, 0, 0);
                    if (!bullet.Hostile)
                    {
                        color = new Color(0, 218, 0);
                    }
                    _batch.DrawRectangle(new Rectangle(bullet.X, bullet.Y, 1, 1), color);
                }

                foreach (var explosion in _explosions)
                {
                    _batch.Draw(_texture, new Rectangle(explosion.X, explosion.Y, 16, 16),
                        new Rectangle(576, 64 - explosion.Stage * 16, 16, 16), Color.White);
                }

                _batch.DrawString(_font, _score.ToString("D8"), new Vector2(0, 245), Color.White,
                    0f, Vector2.Zero, 0.6f, SpriteEffects.None, 0f);

                if (!_gameOver)
                {
                    _batch.Draw(_texture, new Rectangle(_playerX, 230, 7, 9),
                        new Rectangle(584, 0, 7, 9), Color.White);
                }
                else
                {
                    _batch.DrawString(_font, "GAME OVER", new Vector2(16, 120), Color.Red);
                }

                _batch.End();
                RenderTargetManager.EndRenderToTarget();
                Texture = _target;
            }
            base.Update();
        }

        protected override void CreateGeometryInternal()
        {
            var vertices = RectangleComposer.Create(6.4f, 9.6f, new TextureRectangle(0, 0, 1, 1));
            VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, -MathHelper.PiOver2, 0));
            Geometry.AddVertices(vertices);
        }
    }
}
