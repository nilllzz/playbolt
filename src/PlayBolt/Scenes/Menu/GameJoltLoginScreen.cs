﻿using GameDevCommon.Drawing;
using GameDevCommon.Input;
using JoltNet;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PlayBolt.Content;
using PlayBolt.Scenes.InGame;
using PlayBolt.Scenes.InGame.Games;
using PlayBolt.Scenes.InGame.Playlists;
using System.Threading.Tasks;
using static Core;

namespace PlayBolt.Scenes.Menu
{
    class GameJoltLoginScreen : Screen
    {
        private class Textbox
        {
            public string Text = "";
            public int Cursor;
            public Vector2 Position;
            public bool Mask;

            public void Draw(GameTime gameTime, SpriteBatch batch, SpriteFont font, bool selected)
            {
                var text = Text;
                if (Mask)
                {
                    text = "";
                    for (int i = 0; i < Text.Length; i++)
                        text += '*';
                }

                var x = (int)Position.X;
                var y = (int)Position.Y;
                batch.DrawRectangle(new Rectangle(x - 2, y - 2, 204, 36),
                    selected ? new Color(204, 255, 0) : new Color(47, 127, 111));
                batch.DrawRectangle(new Rectangle(x, y, 200, 32), new Color(220, 220, 220));
                batch.DrawString(font, text, Position + new Vector2(4), Color.Black);

                if (selected && gameTime.TotalGameTime.Milliseconds > 500)
                {
                    var preTextSize = font.MeasureString(text.Substring(0, Cursor));
                    batch.DrawRectangle(new Rectangle(x + 4 + (int)(preTextSize.X), y + 2, 2, 28), Color.Black);
                }
            }

            public void Update()
            {
                var controls = GetComponent<ControlsHandler>();
                if (controls.LeftPressed(PlayerIndex.One, InputDirectionType.ArrowKeys))
                {
                    Cursor--;
                    if (Cursor < 0)
                        Cursor = 0;
                }
                if (controls.RightPressed(PlayerIndex.One, InputDirectionType.ArrowKeys))
                {
                    Cursor++;
                    if (Cursor > Text.Length)
                        Cursor = Text.Length;
                }

                var keyboard = GetComponent<KeyboardHandler>();
                if (keyboard.KeyPressed(Keys.End))
                    Cursor = Text.Length;
                if (keyboard.KeyPressed(Keys.Home))
                    Cursor = 0;
                if (Cursor < Text.Length && keyboard.KeyPressed(Keys.Delete))
                {
                    Text = Text.Substring(0, Cursor) + Text.Substring(Cursor + 1);
                }
                if (keyboard.KeyPressed(Keys.V) && (keyboard.KeyDown(Keys.LeftControl) || keyboard.KeyDown(Keys.RightControl)))
                {
                    // TODO clipboard
                }
            }

            public void TextInput(char c)
            {
                if (c == '\t' || c == '\n' || c == '\r')
                    return;
                if (c == '\b')
                {
                    if (Cursor > 0)
                    {
                        Text = Text.Substring(0, Cursor - 1) + Text.Substring(Cursor);
                        Cursor--;
                    }
                }
                else
                {
                    Text = Text.Insert(Cursor, c.ToString());
                    Cursor++;
                }
            }
        }

        private Texture2D _texture, _background, _mascot, _mascot2, _gjlogo;
        private SpriteBatch _batch;
        private SpriteFont _font;
        private Textbox _username, _password;
        private int _selection = 0;
        private bool _loading = false, _loggedIn = false;
        private string _statusMessage = "";

        public GameJoltLoginScreen()
        {
            _username = new Textbox
            {
                Position = new Vector2(GameController.RENDER_WIDTH / 2 - 100, 300),
                Text = "nilllzz"
            };
            _password = new Textbox
            {
                Position = new Vector2(GameController.RENDER_WIDTH / 2 - 100, 400),
                Mask = true,
                Text = "4ea00f"
            };

            Controller.Window.TextInput += Window_TextInput;
        }

        internal override void LoadContent()
        {
            _texture = GetComponent<Resources>().LoadTexture("Textures/MenuTiles.png");
            _background = GetComponent<Resources>().LoadTexture("Textures/Menu/Background.png");
            _mascot = GetComponent<Resources>().LoadTexture("Textures/Menu/Mascot.png");
            _mascot2 = GetComponent<Resources>().LoadTexture("Textures/Menu/Mascot2.png");
            _gjlogo = GetComponent<Resources>().LoadTexture("Textures/Menu/GJLogo.png");
            _batch = new SpriteBatch(Controller.GraphicsDevice);
            _font = Controller.Content.Load<SpriteFont>("Fonts/Font");
        }

        internal override void UnloadContent()
        {
            _texture = null;
            _background = null;
            _mascot = null;
            _mascot2 = null;
            _gjlogo = null;
            _batch.Dispose();
        }

        internal override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteBatchUsage.RealTransparency);

            _batch.DrawRectangle(new Rectangle(0, 0, GameController.RENDER_WIDTH, GameController.RENDER_HEIGHT), new Color(28, 28, 28));

            for (int x = 0; x < GameController.RENDER_WIDTH; x += 500)
            {
                for (int y = 0; y < GameController.RENDER_HEIGHT; y += 500)
                {
                    _batch.Draw(_background, new Rectangle(x, y, 500, 500), Color.White);
                }
            }

            _batch.Draw(_gjlogo, new Rectangle(GameController.RENDER_WIDTH / 2 - 246, 100, 492, 54), Color.White);

            _batch.DrawString(_font, "Username:", new Vector2(GameController.RENDER_WIDTH / 2 - 100, 270), Color.White);
            _username.Draw(gameTime, _batch, _font, _selection == 0 && !_loading);
            _batch.DrawString(_font, "Token:", new Vector2(GameController.RENDER_WIDTH / 2 - 100, 370), Color.White);
            _password.Draw(gameTime, _batch, _font, _selection == 1 && !_loading);

            if (_selection == 2 && !_loading)
            {
                _batch.DrawRectangle(new Rectangle(GameController.RENDER_WIDTH / 2 - 100, 500, 200, 48), new Color(204, 255, 0));
                _batch.DrawString(_font, "Log in", new Vector2(GameController.RENDER_WIDTH / 2 - 90, 512), Color.Black);
            }
            else
            {
                _batch.DrawRectangle(new Rectangle(GameController.RENDER_WIDTH / 2 - 100, 500, 200, 48), new Color(99, 127, 121));
                _batch.DrawString(_font, "Log in", new Vector2(GameController.RENDER_WIDTH / 2 - 90, 512), Color.White);
            }

            _batch.Draw(_mascot, new Rectangle(GameController.RENDER_WIDTH - 380, GameController.RENDER_HEIGHT - 260, 356, 260), Color.White);
            _batch.Draw(_mascot2, new Rectangle(30, GameController.RENDER_HEIGHT - 260, 96, 260), Color.White);

            if (_loading || _statusMessage != "")
            {
                var textureX = 112;
                if (_statusMessage != "")
                    textureX = 96;

                for (int x = 0; x < GameController.RENDER_WIDTH; x += 32)
                {
                    for (int y = 0; y < GameController.RENDER_HEIGHT; y += 32)
                    {
                        _batch.Draw(_texture, new Rectangle(x, y, 32, 32), new Rectangle(textureX, 112, 16, 16), Color.White);
                    }
                }

                var text = _statusMessage;
                if (text == "")
                    text = "Loading...";
                var textSize = _font.MeasureString(text) * 2f;
                _batch.DrawString(_font, text, new Vector2(GameController.RENDER_WIDTH / 2, 200) - textSize / 2f, Color.White,
                    0f, Vector2.Zero, 2f, SpriteEffects.None, 0f);
            }

            _batch.End();
        }


        private void Window_TextInput(object sender, TextInputEventArgs e)
        {
            if (_selection == 0)
                _username.TextInput(e.Character);
            else if (_selection == 1)
                _password.TextInput(e.Character);
        }

        internal override void Update(GameTime gameTime)
        {
            if (_loading)
                return;

            var controls = GetComponent<ControlsHandler>();
            var keyboard = GetComponent<KeyboardHandler>();
            if (_statusMessage != "")
            {
                if (keyboard.KeyPressed(Keys.Space) || keyboard.KeyPressed(Keys.Enter))
                {
                    _statusMessage = "";
                }
                return;
            }

            if (_loggedIn)
            {
                var screen = new MenuContainerScreen();
                screen.LoadContent();
                var transitionScreen = new FadeTransitionScreen(this, screen);
                transitionScreen.LoadContent();
                GetComponent<ScreenManager>().SetScreen(transitionScreen);
            }

            if (_selection == 0)
                _username.Update();
            else if (_selection == 1)
                _password.Update();

            var tabPressed = keyboard.KeyPressed(Keys.Tab);
            var tabInvert = tabPressed && (keyboard.KeyDown(Keys.LeftShift) || keyboard.KeyDown(Keys.RightShift));

            if (controls.UpPressed(PlayerIndex.One, InputDirectionType.ArrowKeys) || (tabPressed && tabInvert))
            {
                _selection--;
                if (_selection < 0)
                    _selection = 2;
            }
            if (controls.DownPressed(PlayerIndex.One, InputDirectionType.ArrowKeys) || (tabPressed && !tabInvert))
            {
                _selection++;
                if (_selection > 2)
                    _selection = 0;
            }

            if ((_selection == 2 && keyboard.KeyPressed(Keys.Space)) || keyboard.KeyPressed(Keys.Enter))
            {
                Login();
            }
        }

        private void Login()
        {
            _loading = true;
            Task.Run(async () =>
            {
                Controller.GameJoltUsername = _username.Text;
                Controller.GameJoltToken = _password.Text;
                
                var authRequest = RequestProvider.Users.Authorize(Controller.GameJoltUsername, Controller.GameJoltToken);
                var response = await Controller.GameJoltClient.ExecuteRequestAsync(authRequest);
                if (response.Success)
                {
                    if (authRequest.Response.Success)
                    {
                        await Microgame.LoadCompletedGames();
                        await Playlist.LoadCompletedPlaylists();
                        await MixMixInGameScreen.LoadCompletedChallenge();
                        _loggedIn = true;
                    }
                    else
                    {
                        _statusMessage = "Wrong username or token";
                    }
                }
                else
                {
                    _statusMessage = "Failed to connect to server";
                }

                _loading = false;
            });
        }
    }
}
