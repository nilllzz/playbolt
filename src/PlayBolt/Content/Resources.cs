﻿using GameDevCommon.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static Core;

namespace PlayBolt.Content
{
    internal class Resources : IGameComponent
    {
        private Dictionary<string, Texture2D> _textures;
        private Dictionary<string, Song> _songs;
        private Dictionary<string, Geometry<VertexPositionNormalTexture>> _geometries;
        private Dictionary<string, Effect> _effects;

        public void Initialize()
        {
            _textures = new Dictionary<string, Texture2D>();
            _songs = new Dictionary<string, Song>();
            _geometries = new Dictionary<string, Geometry<VertexPositionNormalTexture>>();
            _effects = new Dictionary<string, Effect>();
        }

        public void ClearBuffers()
        {
            for (int i = 0; i < _textures.Count; i++)
                _textures.Values.ElementAt(i).Dispose();
            _textures.Clear();
            for (int i = 0; i < _songs.Count; i++)
                _songs.Values.ElementAt(i).Dispose();
            _songs.Clear();
            for (int i = 0; i < _geometries.Count; i++)
                _geometries.Values.ElementAt(i).Dispose();
            _geometries.Clear();
            for (int i = 0; i < _effects.Count; i++)
                _effects.Values.ElementAt(i).Dispose();
            _effects.Clear();
        }

        private static string GetResourcePath(string resourcePath)
        {
            return Path.Combine(Environment.CurrentDirectory, Controller.Content.RootDirectory, resourcePath);
        }

        internal Texture2D LoadTexture(string texturePath, bool cache = true)
        {
            lock (_textures)
            {
                if (!_textures.TryGetValue(texturePath, out var texture))
                {
                    var path = GetResourcePath(texturePath);

                    using (var stream = new FileStream(path, FileMode.Open))
                    {
                        texture = Texture2D.FromStream(Controller.GraphicsDevice, stream);
                        texture.Name = texturePath;
                        if (cache)
                            _textures.Add(texturePath, texture);
                    }
                }

                return texture;
            }
        }

        internal Song LoadSong(string songPath)
        {
            lock (_songs)
            {
                if (!_songs.TryGetValue(songPath, out var song))
                {
                    var path = "Content\\" + songPath.Replace("/", "\\");
                    song = Song.FromUri(songPath, new Uri(path, UriKind.Relative));
                    _songs.Add(songPath, song);
                }

                return song;
            }
        }

        internal Effect LoadEffect(string effectPath)
        {
            lock (_effects)
            {
                if (!_effects.TryGetValue(effectPath, out var effect))
                {
                    var path = GetResourcePath(effectPath);
                    using (var stream = new FileStream(path, FileMode.Open))
                    {
                        var buffer = new byte[stream.Length];
                        stream.Read(buffer, 0, buffer.Length);
                        effect = new Effect(Controller.GraphicsDevice, buffer);
                    }
                    _effects.Add(effectPath, effect);
                }

                return effect;
            }
        }

        internal void LoadGeometry(Geometry<VertexPositionNormalTexture> target, string geoPath)
        {
            lock (_geometries)
            {
                if (!_geometries.TryGetValue(geoPath, out var source))
                {
                    var path = GetResourcePath(geoPath);
                    source = new Geometry<VertexPositionNormalTexture>();
                    GeometrySerializer.Load(source, path);
                    _geometries.Add(geoPath, source);
                }

                source.CopyTo(target);
            }
        }
    }
}
