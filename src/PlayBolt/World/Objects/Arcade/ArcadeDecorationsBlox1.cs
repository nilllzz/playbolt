﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Arcade
{
    class ArcadeDecorationsBlox1 : WorldObject
    {
        public ArcadeDecorationsBlox1(WorldScreen screen, Vector3 position)
            : base(screen, "arcade_decorations_blox_1", position)
        { }

        protected override void CreateGeometryInternal()
        {
            var vertices = RectangleComposer.Create(2, 8,
                new TextureRectangle(new Rectangle(576, 80, 8, 32), Texture));
            VertexTransformer.Rotate(vertices, new Vector3(0, 0.7f, MathHelper.PiOver2));
            Geometry.AddVertices(vertices);
        }
    }
}
