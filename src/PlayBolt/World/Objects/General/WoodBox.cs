﻿using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.General
{
    class WoodBox : WorldObject
    {
        public WoodBox(WorldScreen screen, Vector3 position, float rotation)
            : base(screen, "general_woodenbox", position)
        {
            _rotation = new Vector3(0, rotation, 0);
        }

        protected override void CreateGeometryInternal()
        {
            var texture = new TextureCuboidWrapper();
            texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right, CuboidSide.Front, CuboidSide.Back },
                new TextureRectangle(new Rectangle(192, 0, 16, 16), Texture));
            texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                new TextureRectangle(new Rectangle(208, 0, 16, 16), Texture));
            var vertices = CuboidComposer.Create(2f, texture);
            Geometry.AddVertices(vertices);
        }
    }
}
