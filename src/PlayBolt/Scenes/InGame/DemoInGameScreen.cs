﻿using GameDevCommon.Drawing;
using Microsoft.Xna.Framework;
using PlayBolt.Scenes.InGame.Games;
using PlayBolt.Scenes.InGame.Games.Food;
using PlayBolt.Scenes.InGame.Games.Intro;
using PlayBolt.Scenes.Intro;
using System;
using System.Linq;
using static Core;

namespace PlayBolt.Scenes.InGame
{
    class DemoInGameScreen : InGameScreen
    {
        private static Random _random = new Random();
        private Type[] _games;

        public DemoInGameScreen(Screen preScreen)
            : base(preScreen)
        {
            _isDemo = true;
            _games = new[]
            {
                // TODO: fill with 15 games
                typeof(KrazyKars),
                typeof(RocketNeek),
                typeof(BananaMuncher),
                typeof(Tasting),
                typeof(Globular),
                typeof(Vacuum),
                typeof(BiteSized),
            };
        }

        protected override void AfterGame()
        {
            if (_activeGame.WinState == WinState.Lost)
            {
                LoseLife();
            }
            if (_lifes > 0)
            {
                _score++;
            }
        }

        protected override Microgame GetNextGame()
        {
            var gameType = _games
                    .Where(t => Attribute.GetCustomAttribute(t, typeof(MicrogameDescriptionAttribute)) != null)
                    .OrderBy(x => _random.Next())
                    .First();
            var game = (Microgame)Activator.CreateInstance(gameType);
            return game;
        }

        protected override bool CompletedChallenge() => false;

        protected override void PostScore()
        { }

        protected override void Restart()
        { }

        protected override void DrawBackground()
        {
            _batch.DrawRectangle(new Rectangle(0, 0, GameController.RENDER_WIDTH, GameController.RENDER_HEIGHT), new Color(41, 41, 41));
        }

        protected override void Quit()
        {
            var screen = new IntroScreen();
            screen.LoadContent();
            var transitionScreen = new FadeTransitionScreen(this, screen);
            transitionScreen.LoadContent();
            GetComponent<ScreenManager>().SetScreen(transitionScreen);
        }
    }
}
