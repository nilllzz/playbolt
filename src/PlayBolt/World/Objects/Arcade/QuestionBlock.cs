﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Arcade
{
    class QuestionBlock : WorldObject
    {
        public QuestionBlock(WorldScreen screen, Vector3 position)
             : base(screen, "arcade_question_block", position)
        { }

        protected override void CreateGeometryInternal()
        {
            var texture = new TextureCuboidWrapper();
            texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                new TextureRectangle(new Rectangle(640, 128, 16, 16), Texture));
            texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right, CuboidSide.Front, CuboidSide.Back },
                new TextureRectangle(new Rectangle(624, 128, 16, 16), Texture));
            var vertices = CuboidComposer.Create(4, 4, 4, texture);
            VertexTransformer.Rotate(vertices, new Vector3(0, -MathHelper.PiOver4, 0));
            Geometry.AddVertices(vertices);
        }
    }
}
