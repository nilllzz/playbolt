﻿using GameDevCommon.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace PlayBolt.Scenes.InGame.Games.Food
{
    [MicrogameDescription(2, "Tasting", "Food", "Clench some thirst, but don't overdo it!")]
    class Tasting : Microgame
    {
        public override float CompletionTime => 5f;
        public override string Command => "Serve!";
        public override string Scoretable => "279066";
        public override int TargetScore => 25;
        public override string Thumbnail => "2.png";

        private float _targetFill, _fill;
        private bool _bottleOpened = false;
        private float _corkRotation = 0f, _corkPosition = 0f;

        public override void GameStart()
        {
            _targetFill = (float)_random.NextDouble() * 0.8f + 0.2f;
        }

        private int GetFillState()
        {
            var diff = Math.Abs(_targetFill - _fill);
            switch (Difficulty)
            {
                case Difficulty.Easy:
                    if (diff > 0.2)
                        return 0;
                    else if (diff > 0.1)
                        return 1;
                    else
                        return 2;
                case Difficulty.Medium:
                    if (diff > 0.12)
                        return 0;
                    else if (diff > 0.07)
                        return 1;
                    else
                        return 2;
                case Difficulty.Hard:
                    if (diff <= 0.07)
                        return 2;
                    else
                        return 0;
            }

            return 0;
        }

        public override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteBatchUsage.Default);

            // glass
            _batch.Draw(_texture, new Rectangle(400, 300, 64, 136), new Rectangle(0, 0, 16, 34), Color.White);

            // bottle
            if (!_bottleOpened)
                _batch.Draw(_texture, new Rectangle(500, 200, 64, 160), new Rectangle(32, 0, 16, 40), Color.White);
            else
            {
                // cork
                _batch.Draw(_texture, new Rectangle((int)(500 + _corkPosition / 2f), (int)(200 + _corkPosition), 16, 20),
                    new Rectangle(38, 0, 4, 5), Color.White,
                    _corkRotation, new Vector2(2), SpriteEffects.None, 0f);
                if (_fill < 1f && ActionDown())
                {
                    _batch.DrawLine(new Vector2(453, 230), new Vector2(453, 375), new Color(96, 46, 63), 10);
                    _batch.Draw(_texture, new Rectangle(470, 260, 64, 140), new Rectangle(32, 5, 16, 35), Color.White,
                        -MathHelper.PiOver4 * 3, Vector2.Zero, SpriteEffects.None, 0f);
                }
                else
                {
                    _batch.Draw(_texture, new Rectangle(500, 220, 64, 140), new Rectangle(32, 5, 16, 35), Color.White);
                }
            }

            // drink
            var drinkSize = (int)(76 * _fill);
            var drinkTextureSize = (int)(19 * _fill);
            _batch.Draw(_texture, new Rectangle(400, 376 - drinkSize, 64, drinkSize), new Rectangle(16, 19 - drinkTextureSize, 16, drinkTextureSize), Color.White);

            // bubble
            _batch.Draw(_texture, new Rectangle(50, 200, 280, 170), new Rectangle(0, 47, 28, 17), Color.White);
            switch (GetFillState())
            {
                case 0:
                    _batch.Draw(_texture, new Rectangle(140, 230, 90, 90), new Rectangle(48, 20, 9, 9), Color.White);
                    break;
                case 1:
                    _batch.Draw(_texture, new Rectangle(140, 230, 90, 90), new Rectangle(48, 0, 10, 10), Color.White);
                    break;
                case 2:
                    _batch.Draw(_texture, new Rectangle(135, 230, 110, 90), new Rectangle(48, 10, 12, 10), Color.White);
                    break;
            }

            _batch.End();
        }

        public override void Update(GameTime gameTime)
        {
            if (_fill < 1f && ActionDown())
            {
                if (!_bottleOpened)
                {
                    _bottleOpened = true;
                }
                
                _fill += 0.01f * Speed;
                if (_fill >= 1f)
                    _fill = 1f;
            }

            if (_bottleOpened)
            {
                _corkRotation += 0.1f * Speed;
                _corkPosition += 6f * Speed;
            }
        }

        public override void TimeUp()
        {
            if (GetFillState() > 0)
                WinState = WinState.Won;
            else
                WinState = WinState.Lost;
        }
    }
}
