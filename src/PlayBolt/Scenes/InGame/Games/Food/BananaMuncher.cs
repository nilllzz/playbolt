﻿using GameDevCommon.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace PlayBolt.Scenes.InGame.Games.Food
{
    [MicrogameDescription(6, "Banana Muncher", "Food", "Munch em down, you savage.")]
    class BananaMuncher : Microgame
    {
        public override float CompletionTime => 4;
        public override string Command => "Munch!";
        public override Color CommandColor => new Color(255, 240, 181);
        public override string Scoretable => "284661";
        public override int TargetScore => 30;
        
        private class Particle
        {
            private static Dictionary<int, int> _stateHeights = new Dictionary<int, int>()
            {
                { 0, 13 },
                { 1, 25 },
                { 2, 25 },
                { 3, 25 },
                { 4, 27 },
                { 5, 32 },
                { 6, 40 },
                { 7, 56 },
                { 8, 56 },
                { 9, 71 },
                { 10, 76 },
                { 11, 76 },
            };

            private float _x, _y;
            private float _dirX, _dirY;
            private int _size;
            private Color _c;

            public float LifeTime;
            public Rectangle Bounds
                => new Rectangle((int)_x, (int)_y, _size, _size);
            public Color Color
                => new Color(_c.R, _c.G, _c.B, (int)(LifeTime * 255));

            public Particle(int stage)
            {
                LifeTime = 1f;
                _x = _random.Next(WIDTH / 2 - 32, WIDTH / 2 + 32);
                _y = -_random.Next(80, 96) + _stateHeights[stage] * 2 + HEIGHT / 2;
                _dirX = _random.Next(-10, 10) / 10f;
                _dirY = 0f;
                _size = _random.Next(2, 6);

                switch (_random.Next(0, 3))
                {
                    case 0:
                        _c = new Color(255, 232, 140);
                        break;
                    case 1:
                        _c = new Color(243, 208, 64);
                        break;
                    case 2:
                        _c = new Color(203, 166, 11);
                        break;
                }
            }

            public void Update(float speed)
            {
                if (LifeTime > 0f)
                {
                    LifeTime -= 0.01f * speed;
                    if (LifeTime <= 0f)
                        LifeTime = 0f;
                }

                _dirY += 0.1f;

                _x += _dirX * speed;
                _y += _dirY * speed;
            }
        }

        // 0 full banana, 12 no banana
        // easy: 6 bites
        // medium: 9 bites
        // hard: 12 bites
        private static Dictionary<Difficulty, int[]> _stageDefs = new Dictionary<Difficulty, int[]>()
        {
            { Difficulty.Easy, new[] { 0, 1, 2, 3, 7, 10, 12 } },
            { Difficulty.Medium, new[] { 0, 1, 2, 3, 5, 6, 7, 9, 10, 12 } },
            { Difficulty.Hard, new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 } },
        };

        private int _stageIndex = 0;
        private float _shake = 0f;
        private List<Particle> _particles = new List<Particle>();

        private int CurrentStage
            => _stageDefs[Difficulty][_stageIndex];

        public override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteBatchUsage.Font);

            _batch.Draw(_texture, new Rectangle(-2, -2, WIDTH + 4, HEIGHT + 4), new Rectangle(96, 288, 171, 96), Color.White);

            _batch.End();

            _batch.Begin(SpriteBatchUsage.Default);

            var x = (int)(WIDTH / 2f - 96);
            var y = (int)(HEIGHT / 2f - 96);
            var texX = CurrentStage;
            var texY = 0;
            while (texX > 3)
            {
                texX -= 4;
                texY++;
            }

            if (CurrentStage > 2)
            {
                _batch.Draw(_texture, new Rectangle(x - 16, y + 170, 192, 192),
                    new Rectangle(0, 288, 96, 96), Color.White,
                    0f, Vector2.Zero, SpriteEffects.FlipVertically, 0);
            }
            if (CurrentStage < 12)
            {
                var rot = (float)(Math.Sin(_shake * 30) * 0.08);
                _batch.Draw(_texture, new Rectangle(x + 96, y + 96, 192, 192),
                    new Rectangle(texX * 96, texY * 96, 96, 96), Color.White,
                    rot, new Vector2(48), SpriteEffects.None, 0f);
            }

            foreach (var particle in _particles)
            {
                _batch.DrawRectangle(particle.Bounds, particle.Color);
            }

            _batch.End();
        }

        public override void Update(GameTime gameTime)
        {
            if (WinState == WinState.Default)
            {
                if (ActionPressed())
                {
                    _particles.AddRange(new[]
                    {
                        new Particle(CurrentStage),
                        new Particle(CurrentStage),
                        new Particle(CurrentStage)
                    });

                    _stageIndex++;
                    _shake = 1f;
                    if (_stageIndex == _stageDefs[Difficulty].Length - 1)
                    {
                        WinState = WinState.Won;
                    }
                }
            }

            for (int i = 0; i < _particles.Count; i++)
            {
                _particles[i].Update(Speed);
                if (_particles[i].LifeTime == 0f)
                {
                    _particles.RemoveAt(i);
                    i--;
                }
            }

            if (_shake > 0f)
            {
                _shake -= (0.1f * Speed);
                if (_shake <= 0f)
                {
                    _shake = 0f;
                }
            }
        }

        public override void TimeUp()
        {
            if (WinState == WinState.Default)
            {
                WinState = WinState.Lost;
            }
        }
    }
}
