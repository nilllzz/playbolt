﻿using System;

namespace PlayBolt
{
    static class Extensions
    {
        public static DateTime GetUtcFromUnixTimestamp(int timestamp)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return dateTime.AddSeconds(timestamp);
        }
    }
}
