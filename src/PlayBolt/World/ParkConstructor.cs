﻿using GameDevCommon.Rendering;
using Microsoft.Xna.Framework;
using PlayBolt.World.Objects;
using PlayBolt.World.Objects.Arcade;
using PlayBolt.World.Objects.Astro;
using PlayBolt.World.Objects.Entry;
using PlayBolt.World.Objects.Ferris;
using PlayBolt.World.Objects.General;
using PlayBolt.World.Objects.Gift;
using PlayBolt.World.Objects.MotionSim;
using PlayBolt.World.Objects.ParkingLot;
using PlayBolt.World.Objects.Pond;

namespace PlayBolt.World
{
    static class ParkConstructor
    {
        public static OptimizableRenderObjectCollection GetPark(WorldScreen screen, ParkConfig park)
        {
            var objects = new OptimizableRenderObjectCollection
            {
                new Ground(screen),

                // parking lot
                new BlueCar(screen, new Vector3(0, 0, 140), 0f),

                // outer fencing
                // front
                new Hedge(screen, new Vector3(75, 1, 55), false, 138),
                new Fence(screen, new Vector3(75, 4.5f, 55), false, 138),
                new Hedge(screen, new Vector3(-75, 1, 55), false, 138),
                new Fence(screen, new Vector3(-75, 4.5f, 55), false, 138),
                new Banner(screen, new Vector3(7.5f, 4f, 55.2f)),
                new Banner(screen, new Vector3(-7.5f, 4f, 55.2f)),
                new Bushes(screen, new Vector3(26, 7, 54.5f), false, 4),
                new Bushes(screen, new Vector3(60, 7, 54.4f), false, 4),
                new Bushes(screen, new Vector3(94, 7, 54.5f), false, 4),
                new Bushes(screen, new Vector3(128, 7, 54.4f), false, 3),
                new Bushes(screen, new Vector3(-26, 7, 54.5f), false, 4),
                new Bushes(screen, new Vector3(-60, 7, 54.4f), false, 4),
                new Bushes(screen, new Vector3(-94, 7, 54.5f), false, 4),
                new Bushes(screen, new Vector3(-128, 7, 54.4f), false, 3),
                // right
                new Hedge(screen, new Vector3(145, 1, -45), true, 200),
                new Fence(screen, new Vector3(145, 4.5f, -45), true, 200),
                new Bushes(screen, new Vector3(144.5f, 7, 35), true, 4),
                new Bushes(screen, new Vector3(144.25f, 7, 1), true, 4),
                new Bushes(screen, new Vector3(144.5f, 7, -33), true, 4),
                new Bushes(screen, new Vector3(144.25f, 7, -67), true, 4),
                new Bushes(screen, new Vector3(144.5f, 7, -101), true, 4),
                new Bushes(screen, new Vector3(144.25f, 7, -130), true, 3),
                // left
                new Hedge(screen, new Vector3(-145, 1, -45), true, 200),
                new Fence(screen, new Vector3(-145, 4.5f, -45), true, 200),
                new Bushes(screen, new Vector3(-144.5f, 7, 35), true, 4),
                new Bushes(screen, new Vector3(-144.25f, 7, 1), true, 4),
                new Bushes(screen, new Vector3(-144.5f, 7, -33), true, 4),
                new Bushes(screen, new Vector3(-144.25f, 7, -67), true, 4),
                new Bushes(screen, new Vector3(-144.5f, 7, -101), true, 4),
                new Bushes(screen, new Vector3(-144.25f, 7, -130), true, 3),
                // back
                new Hedge(screen, new Vector3(0, 1, -144), false, 288),
                new Fence(screen, new Vector3(0, 4.5f, -144), false, 288),
                new Bushes(screen, new Vector3(124, 7, -143.5f), false, 4),
                new Bushes(screen, new Vector3(90, 7, -143.25f), false, 4),
                new Bushes(screen, new Vector3(56, 7, -143.5f), false, 4),
                new Bushes(screen, new Vector3(22, 7, -143.25f), false, 4),
                new Bushes(screen, new Vector3(-12, 7, -143.5f), false, 4),
                new Bushes(screen, new Vector3(-46, 7, -143.25f), false, 4),
                new Bushes(screen, new Vector3(-80, 7, -143.5f), false, 4),
                new Bushes(screen, new Vector3(-114, 7, -143.5f), false, 4),
                new Bushes(screen, new Vector3(-134, 7, -143.25f), false, 2),
                
                // entrance
                new Entrance(screen, new Vector3(0, 0, 55)),
                new EntryBarrier(screen, new Vector3(0, 1.3f, 55), park),
                
                // ferris mountain
                new FerrisBase(screen, new Vector3(100, 9, 0)),
                new FerrisWheel(screen, new Vector3(100, 47, -3)),
                new FerrisWheel(screen, new Vector3(100, 47, 3)),
                new Hedge(screen, new Vector3(119, 9, 20), true, 20),
                new Hedge(screen, new Vector3(98, 9, 29), false, 40),
                new Bench(screen, new Vector3(110, 9, 26), MathHelper.Pi),
                new Bench(screen, new Vector3(100, 9, 26), MathHelper.Pi),
                new Bench(screen, new Vector3(90, 9, 26), MathHelper.Pi),
                new Lamppost(screen, new Vector3(95, 8, 26)),
                new Lamppost(screen, new Vector3(105, 8, 26)),
                new Trashcan(screen, new Vector3(115, 8.65f, 26)),
                new Trashcan(screen, new Vector3(85, 8.65f, 26)),
                new Bushes(screen, new Vector3(98, 10, 29), false, 4),
                new Bushes(screen, new Vector3(119, 10, 20), true, 2),

                // gift shop
                new GiftShop(screen, new Vector3(-40, 0, 50)),
                new WoodBox(screen, new Vector3(-53, 1, 45), 0),
                new WoodBox(screen, new Vector3(-54, 3, 44.5f), -0.4f),
                new WoodBox(screen, new Vector3(-55.5f, 1, 44), 0),
                new WoodBox(screen, new Vector3(-54, 1, 40), 0.5f),
                new SpinningLogo(screen, new Vector3(-40, 20, 42)),

                // pond
                new Water(screen, new Vector3(-90, -2f, 10)),
                new Bridge(screen, new Vector3(-71, 0.5f, -20)),
                new Rock(screen, new Vector3(-80, 0.5f, 4), 0, MathHelper.PiOver2),
                new Rock(screen, new Vector3(-80, 0.45f, 6), 1, MathHelper.PiOver2),
                new Rock(screen, new Vector3(-80, 0.35f, 8), 2, MathHelper.PiOver2),
                new Rock(screen, new Vector3(-80, 0.25f, 12), 3, MathHelper.PiOver2),

                // astrodome
                new AstroDome(screen, new Vector3(-110, 11f, -55)),
                new AstroBuilding(screen, new Vector3(-110, 5f, -55)),

                // motion simulator
                new MotionSimBase(screen, new Vector3(110, 2, -40)),
                new Arwing(screen, new Vector3(102, 7.5f, -40)),
                new MotionSimBackPanel(screen, new Vector3(126, 16, -40)),

                // arcade
                new ArcadeBuilding(screen, new Vector3(110, 5, -90)),
                new ArcadeDecorationsBlox1(screen, new Vector3(110 - 4.1f, 10, -106)),
                new ArcadeDecorationsBlox2(screen, new Vector3(110 - 4.15f, 10, -102)),
                new ArcadeDecorationsBlox3(screen, new Vector3(110 - 4.15f, 4, -100)),
                new ArcadeDecorationsAlien1(screen, new Vector3(110 - 4.15f, 10, -80)),
                new ArcadeDecorationsAlien2(screen, new Vector3(110 - 4.15f, 11, -73)),
                new QuestionBlock(screen, new Vector3(110, 12, -90)),
                new BloxGame(screen, new Vector3(110 - 4.05f, 5, -104)),
                new InvadersGame(screen, new Vector3(110 - 4.05f, 5, -76)),
            };

            for (var x = 96; x <= 124; x += 4)
            {
                objects.Add(new MotionSimFence(screen, new Vector3(x, 5, -24.1f), false));
                objects.Add(new MotionSimFence(screen, new Vector3(x, 5, -55.9f), false));
            }

            objects.AddRange(FerrisCabin.Generate(screen, new Vector3(100, 44, 0)));

            return objects;
        }
    }
}
