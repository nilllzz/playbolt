﻿using JoltNet;
using PlayBolt.Scenes.InGame.Games;
using System;
using System.Linq;
using System.Threading.Tasks;
using static Core;

namespace PlayBolt.Scenes.InGame
{
    class MixMixInGameScreen : InGameScreen
    {
        private static bool _completedChallenge = false;
        private const string CHALLENGE_KEY = "mixmix_completed";
        private static Random _random = new Random();

        public static async Task LoadCompletedChallenge()
        {
            var storageRequest = RequestProvider.Storage.Fetch(CHALLENGE_KEY, Controller.GameJoltUsername, Controller.GameJoltToken);
            await Controller.GameJoltClient.ExecuteRequestAsync(storageRequest);
            _completedChallenge = storageRequest.Response.Success;
        }

        public MixMixInGameScreen(Screen preScreen)
            : base(preScreen)
        {
            _lifes = 4;
            _speed = 1f;
        }

        protected override void AfterGame()
        {
            if (_activeGame.WinState == WinState.Lost)
            {
                LoseLife();
            }
            if (_lifes > 0)
            {
                _score++;
            }
        }

        protected override bool CompletedChallenge()
        {
            if (_score >= 100 && !_completedChallenge)
            {
                Task.Run(async () => await CompleteChallenge());
                return true;
            }
            else
            {
                return false;
            }
        }

        private async Task CompleteChallenge()
        {
            var storageRequest = RequestProvider.Storage.Set(CHALLENGE_KEY, "0", Controller.GameJoltUsername, Controller.GameJoltToken);
            await Controller.GameJoltClient.ExecuteRequestAsync(storageRequest);
        }

        protected override Microgame GetNextGame()
        {
            var gameType = GetType().Assembly.GetTypes()
                    .Where(t => Attribute.GetCustomAttribute(t, typeof(MicrogameDescriptionAttribute)) != null)
                    .OrderBy(x => _random.Next())
                    .First();
            var game = (Microgame)Activator.CreateInstance(gameType);
            return game;
        }

        protected override void PostScore()
        {
            Task.Run(async () =>
            {
                var tableId = "281593";
                var oldScoreRequest = RequestProvider.Scores.Fetch(tableId, Controller.GameJoltUsername, Controller.GameJoltToken);
                var oldRank = -1;
                await Controller.GameJoltClient.ExecuteRequestAsync(oldScoreRequest);
                if (oldScoreRequest.Response.Scores.Length > 0)
                {
                    var oldRankRequest = RequestProvider.Scores.FetchRank(oldScoreRequest.Response.Scores[0].Sort);
                    await Controller.GameJoltClient.ExecuteRequestAsync(oldRankRequest);
                    oldRank = oldRankRequest.Response.Rank;
                }

                if ((oldScoreRequest.Response.Scores.Length > 0 &&
                    oldScoreRequest.Response.Scores[0].Sort < _score) ||
                    oldScoreRequest.Response.Scores.Length == 0)
                {
                    var timeRequest = RequestProvider.Time.Get();
                    await Controller.GameJoltClient.ExecuteRequestAsync(timeRequest);
                    var addScoreRequest = RequestProvider.Scores.Add(tableId, _score, _score.ToString(),
                        Controller.GameJoltUsername, Controller.GameJoltToken,
                        timeRequest.Response.Timestamp.ToString());
                    await Controller.GameJoltClient.ExecuteRequestAsync(addScoreRequest);
                }

                _highscoreData = await HighscoreProvider.GetFullScoreData(tableId, 3);

                if (oldRank == -1 || oldScoreRequest.Response.Scores[0].Sort < _score)
                {
                    _newHighscore = true;
                }
                else if (oldRank > _highscoreData.OwnRank)
                {
                    _newHighscore = true;
                    _rankDifference = oldRank - _highscoreData.OwnRank;
                }

                _receivedScores = true;
            });
        }

        protected override void Restart()
        {
            // unload current game screen, then load new one.
            UnloadContent();

            var screen = new MixMixInGameScreen(_preScreen);
            screen.LoadContent();
            GetComponent<ScreenManager>().SetScreen(screen);
        }

        protected override void DrawBackground()
        {

        }
    }
}
