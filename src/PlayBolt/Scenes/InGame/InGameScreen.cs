﻿using GameDevCommon.Drawing;
using GameDevCommon.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PlayBolt.Content;
using PlayBolt.Scenes.InGame.Games;
using PlayBolt.Scenes.Intro;
using System;
using System.Linq;
using static Core;

namespace PlayBolt.Scenes.InGame
{
    abstract class InGameScreen : Screen
    {
        private struct LifeBlock
        {
            public Vector2 Position;
            public int TextureIndex;
            public Color Color;
        }

        private enum ScreenStage
        {
            DisplayScore,
            PlayGame,
            Lost,
            LostLife
        }

        protected Screen _preScreen;
        private Texture2D _texture, _awardRibbonTexture;
        private Color[] _lifeTextureData;
        private Color[] _rainbowColors;
        protected SpriteBatch _batch;
        private SpriteFont _font, _fontItalic;
        private RenderTarget2D _gameTarget;
        private Random _random = new Random();
        private LifeBlock[] _lostLifeData;

        private float _lifeAnimation = 0f;
        private ScreenStage _stage = ScreenStage.DisplayScore;
        private int _displayScoreDelay = 80;
        private float _displayScoreOutro = 0f;
        private float _playGameIntro = 0f;
        private float _commandDelay = 0f;
        private float _lostGameIntro = 0f;
        protected float _gameOverIntro = 0f;
        protected bool _receivedScores = false;
        private int _selection = 0;
        private float _selectionIntro = 0f;
        private bool _closing = false;
        private float _closingIntro = 0f;
        private bool _speedUp = false;
        private float _speedUpIntro1 = 0f, _speedUpIntro2 = 0f;
        private float _lostLifeDelay = 0f;
        private float _lostLifeShakeIntro = 0f;
        private float _lostLifeOutro = 0f;
        private bool _showAward = false;
        private float _awardIntro = 0f, _awardTextIntro = 0f;
        private float _newHighscoreAnimation = 0f;
        private float _pausedIntro = 0f;
        private int _pausedAnimation = 0;
        private float _explosionIntro = 0f;

        protected float _completionTime = 0f, _totalCompletionTime = 0f;
        protected Microgame _activeGame;
        protected int _lifes = 4;
        protected int _score = 0;
        protected float _speed = 1f;
        protected Difficulty _difficulty = Difficulty.Easy;
        protected MicrogameScores _highscoreData;
        protected int _rankDifference = -1;
        protected bool _newHighscore = false;
        protected bool _isDemo = false;
        protected bool _isPaused = false;

        public InGameScreen(Screen preScreen)
        {
            _preScreen = preScreen;
        }

        internal override void LoadContent()
        {
            _texture = GetComponent<Resources>().LoadTexture("Textures/MenuTiles.png");
            _awardRibbonTexture = GetComponent<Resources>().LoadTexture("Textures/Menu/AwardRibbon.png");
            _batch = new SpriteBatch(Controller.GraphicsDevice);
            _font = Controller.Content.Load<SpriteFont>("Fonts/Font");
            _fontItalic = Controller.Content.Load<SpriteFont>("Fonts/FontItalic");
            _gameTarget = new RenderTarget2D(Controller.GraphicsDevice, 900, 500, false, SurfaceFormat.Color, DepthFormat.Depth24Stencil8);
            _rainbowColors = GetComponent<Resources>().LoadTexture("Textures/Menu/RainbowColors.png").GetData();
        }
        
        private void LoadLifeData()
        {
            var lifeTexture = GetComponent<Resources>().LoadTexture("Textures/Games/Life.png");
            var lifeWidth = lifeTexture.Width * 16;

            if (_lifeTextureData == null)
                _lifeTextureData = lifeTexture.GetData();

            var startX = GameController.RENDER_WIDTH / 2 - lifeWidth / 2;
            _lostLifeData = _lifeTextureData.Select((c, i) => new LifeBlock
            {
                Color = c,
                TextureIndex = i,
                Position = new Vector2((i % lifeTexture.Width) * 16 + startX, (float)Math.Floor((double)i / lifeTexture.Width) * 16 + 200)
            }).Where(b => b.Color.A == 255).ToArray();
        }

        internal override void UnloadContent()
        {
            _texture = null;
            _awardRibbonTexture = null;
            _batch.Dispose();
            _font = null;
            _fontItalic = null;
            _rainbowColors = null;
            _gameTarget.Dispose();
            _activeGame?.UnloadContent();
        }

        internal override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteBatchUsage.Default);

            DrawBackground();
            DrawGame(gameTime, _playGameIntro);
            DrawLifes();
            DrawBomb();
            DrawCommand();
            DrawOverlay();

            // score
            {
                _batch.DrawString(_font, _score.ToString("D3"),
                    new Vector2(1000 - _displayScoreOutro * 480, 70 + _displayScoreOutro * 240 - 250 * _lostGameIntro),
                    new Color((int)(_displayScoreOutro * 255), (int)(_displayScoreOutro * 255), (int)(_displayScoreOutro * 255)),
                    0f, Vector2.Zero, 4f + _displayScoreOutro * 4, SpriteEffects.None, 0f);

                if (_newHighscore)
                {
                    var str = "NEW HIGHSCORE!";
                    var offset = 0f;
                    for (int i = 0; i < str.Length; i++)
                    {
                        var index = (int)((str.Length * 10 - i * 10) + 100 * _newHighscoreAnimation);
                        while (index > 99)
                            index -= 100;
                        var color = _rainbowColors[index];
                        _batch.DrawString(_font, str[i].ToString(),
                            new Vector2(GameController.RENDER_WIDTH / 2 - 140 + offset, 210),
                            new Color(color.R, color.G, color.B, (int)(255 * _lostGameIntro)),
                            0f, Vector2.Zero, 2f, SpriteEffects.None, 0f);
                        offset += _font.MeasureString(str[i].ToString()).X * 2;
                    }
                }

                // highscores
                if (_stage == ScreenStage.Lost)
                {
                    var gameOverText = "Game Over ".Substring(0, (int)(Math.Floor(_gameOverIntro * 10)));
                    var gameOverScale = 3f + (1 - _lostGameIntro) * 4f;
                    var gameOverSize = _font.MeasureString("Game Over") * gameOverScale;
                    _batch.DrawString(_font, gameOverText,
                        new Vector2(GameController.RENDER_WIDTH / 2 - gameOverSize.X / 2, 100 - _lostGameIntro * 64),
                        new Color(255, 93, 39),
                        0f, Vector2.Zero, gameOverScale, SpriteEffects.None, 0f);

                    if (_gameOverIntro == 1f && _displayScoreOutro == 1f)
                    {
                        int startY = 240;
                        if (_newHighscore)
                            startY += 36;
                        int startX = GameController.RENDER_WIDTH / 2 - 350;

                        _batch.DrawRectangle(new Rectangle(startX, startY, 300, 200), new Color(255, 255, 255, (int)(200 * _lostGameIntro)));
                        _batch.DrawString(_font, "Highscores", new Vector2(startX + 10, startY + 10), new Color(0, 0, 0, (int)(200 * _lostGameIntro)),
                            0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);

                        _batch.DrawRectangle(new Rectangle(startX, startY + 250, 300, 100), new Color(255, 255, 255, (int)(200 * _lostGameIntro)));
                        _batch.DrawString(_font, "Personal best", new Vector2(startX + 10, startY + 260), new Color(0, 0, 0, (int)(200 * _lostGameIntro)),
                            0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);

                        if (_receivedScores)
                        {
                            for (int s = 0; s < _highscoreData.Highscores.Length; s++)
                            {
                                var score = _highscoreData.Highscores[s];
                                switch (s)
                                {
                                    case 0:
                                        _batch.Draw(_texture, new Rectangle(startX + 10, startY + s * 40 + 60, 34, 35),
                                            new Rectangle(0, 66, 34, 35), new Color(255, 255, 255, (int)(200 * _lostGameIntro)));
                                        break;
                                    case 1:
                                        _batch.Draw(_texture, new Rectangle(startX + 10, startY + s * 40 + 60, 34, 35),
                                            new Rectangle(34, 66, 34, 35), new Color(255, 255, 255, (int)(200 * _lostGameIntro)));
                                        break;
                                    case 2:
                                        _batch.Draw(_texture, new Rectangle(startX + 10, startY + s * 40 + 60, 34, 35),
                                            new Rectangle(68, 66, 34, 35), new Color(255, 255, 255, (int)(200 * _lostGameIntro)));
                                        break;
                                }
                                score.Draw(gameTime, _batch, _font, _texture,
                                    new Vector2(startX + 74, startY + s * 40 + 66), Color.Black, (int)(255 * _lostGameIntro));
                            }

                            _highscoreData.OwnHighscore.Draw(gameTime, _batch, _font, _texture,
                                new Vector2(startX + 74, startY + 310), Color.Black, (int)(255 * _lostGameIntro));

                            if (!_highscoreData.OwnHighscore.IsEmpty)
                            {
                                _batch.DrawString(_font, _highscoreData.OwnRank.ToString(), new Vector2(startX + 20, startY + 310),
                                    new Color(0, 0, 0, (int)(255 * _lostGameIntro)));
                            }
                            if (_rankDifference > 0)
                            {
                                _batch.DrawString(_font, "+" + _rankDifference.ToString(), new Vector2(startX + 26, startY + 296),
                                    new Color(47, 127, 111, (int)(255 * _lostGameIntro)));
                            }
                        }
                    }
                }
            }

            // speedup
            {
                if (_speedUp)
                {
                    var textSize = _fontItalic.MeasureString("+SPEED UP") * 4;
                    var left = (((1 - _speedUpIntro1) + _speedUpIntro2) / 2f) * (GameController.RENDER_WIDTH + textSize.X) - textSize.X;
                    _batch.DrawString(_fontItalic, "+SPEED UP", new Vector2(left, 500), new Color(49, 214, 255),
                        0f, Vector2.Zero, 4f, SpriteEffects.None, 0f);
                }
            }

            // lost life
            {
                if (_stage == ScreenStage.LostLife)
                {
                    foreach (var block in _lostLifeData)
                    {
                        _batch.DrawRectangle(new Rectangle((int)block.Position.X, (int)block.Position.Y, 16, 16),
                            new Color(block.Color.R, block.Color.G, block.Color.B, (int)(255 * _lostLifeDelay)));
                    }
                }
            }

            DrawPaused();

            // buttons
            {
                if (_stage == ScreenStage.Lost)
                {
                    var buttons = new[] { "Retry", "Bail" };
                    for (int i = 0; i < buttons.Length; i++)
                    {
                        var up = 240 + i * 120;
                        if (_newHighscore)
                            up += 36;
                        var left = GameController.RENDER_WIDTH / 2 + 50;
                        var c = new Color(255, 255, 255, (int)(200 * _lostGameIntro));
                        if (i == _selection && _receivedScores)
                        {
                            c = new Color(204, 255, 0, (int)(200 * _lostGameIntro));
                            var checkeredEnd = (int)(_selectionIntro * 288);
                            for (int x = 0; x < checkeredEnd; x += 32)
                            {
                                for (int y = 0; y < 96; y += 32)
                                {
                                    _batch.Draw(_texture, new Rectangle(x + left, y + up, 32, 32), new Rectangle(112, 96, 16, 16),
                                        new Color(255, 255, 255, (int)(80 * _lostGameIntro)));
                                }
                            }
                            var arrowEnd = (int)(_selectionIntro * 35) + left + 5;
                            _batch.DrawLine(new Vector2(left + 5, up), new Vector2(arrowEnd, up + 50), c, 5);
                            _batch.DrawLine(new Vector2(arrowEnd, up + 50), new Vector2(left + 5, up + 96), c, 5);
                        }
                        _batch.DrawRectangle(new Rectangle(left, up, 288, 4), c);
                        _batch.DrawRectangle(new Rectangle(left, up + 96, 288, 4), c);
                        _batch.DrawRectangle(new Rectangle(left, up, 4, 100), c);
                        _batch.DrawRectangle(new Rectangle(left + 284, up, 4, 100), c);

                        _batch.DrawString(_font, buttons[i], new Vector2(left + 50, up - 8),
                            c, 0f, Vector2.Zero, 5f, SpriteEffects.None, 0f);
                    }
                }
            }

            DrawAwardRibbon();
            DrawClosingOverlay();

            _batch.End();
        }

        protected abstract void DrawBackground();

        private void DrawLifes()
        {
            _batch.DrawLine(new Vector2(0, -10), new Vector2(GameController.RENDER_WIDTH + 80, 80), new Color(204, 255, 0), 110);

            var lifeAnimation = (float)Math.Sin(_lifeAnimation / 10f);

            for (int x = 0; x < _lifes; x++)
            {
                _batch.Draw(_texture, new Rectangle(GameController.RENDER_WIDTH / 2 + x * 120 - 180, 80, 72, (int)(72 + lifeAnimation * 30)),
                    new Rectangle(32, 9, 18, 18), Color.White,
                    0.4f * lifeAnimation, new Vector2(9, 9), SpriteEffects.None, 0f);
            }
        }

        protected virtual void DrawGame(GameTime gameTime, float intro)
        {
            // default implementation for Demo and Single screens.
            // override in all other game screens!

            _batch.DrawRectangle(new Rectangle(GameController.RENDER_WIDTH / 2 - 450, 180, 900, 500), new Color(25, 25, 25));

            if (_stage == ScreenStage.PlayGame)
            {
                var currentTarget = (RenderTarget2D)Controller.GraphicsDevice.GetRenderTargets()[0].RenderTarget;
                Controller.GraphicsDevice.SetRenderTarget(_gameTarget);
                Controller.GraphicsDevice.Clear(Color.White);

                _activeGame.Draw(gameTime);

                Controller.GraphicsDevice.SetRenderTarget(currentTarget);

                var size = 0.2f + (0.8f * intro);
                _batch.Draw(_gameTarget,
                    new Rectangle(GameController.RENDER_WIDTH / 2 - (int)(450 * size), 430 - (int)(250 * size),
                    (int)(900 * size), (int)(500 * size)), Color.White);
            }

            // doors
            var doorColor = Color.Black;
            if (_activeGame != null && (_stage == ScreenStage.DisplayScore || _stage == ScreenStage.LostLife))
            {
                var multiplier = Math.Abs(gameTime.TotalGameTime.Milliseconds - 500) / 500f;
                if (_activeGame.WinState == WinState.Won)
                    doorColor = new Color((int)(18 * multiplier), (int)(49 * multiplier), (int)(43 * multiplier));
                else if (_activeGame.WinState == WinState.Lost)
                    doorColor = new Color((int)(32 * multiplier), 0, 0);
            }
            var doorProgress = (int)(450 * intro);
            _batch.DrawRectangle(new Rectangle(GameController.RENDER_WIDTH / 2 - 450, 180, 450 - doorProgress, 500),
                doorColor);
            _batch.DrawRectangle(new Rectangle(GameController.RENDER_WIDTH / 2 + doorProgress, 180, 450 - doorProgress, 500),
                doorColor);
        }

        private void DrawBomb()
        {
            const int startX = GameController.RENDER_WIDTH / 2 - 470;
            const int startY = 650;
            if (_completionTime > 0f)
            {
                var timeValue = _completionTime / _totalCompletionTime;

                _batch.Draw(_texture, new Rectangle(startX, startY, 64, 64), new Rectangle(64, 16, 16, 16), Color.White);
                _batch.Draw(_texture, new Rectangle(startX + 64, startY + 44, (int)(800 * timeValue), 16),
                    new Rectangle(79, 27, 1, 4), Color.White);

                var secondsLeft = (int)Math.Ceiling(_completionTime / 60 * _speed);
                if (secondsLeft <= 3 && secondsLeft > 0)
                {
                    Color color = Color.White;
                    switch (secondsLeft)
                    {
                        case 3:
                            color = new Color(255, 250, 164);
                            break;
                        case 2:
                            color = new Color(255, 180, 39);
                            break;
                        case 1:
                            color = new Color(236, 69, 0);
                            break;
                    }
                    var textSize = (3f - secondsLeft) * 1.5f;
                    _batch.DrawString(_font, secondsLeft.ToString(), new Vector2(startX - 32, startY)
                        - new Vector2(textSize * 10, textSize * 14), color,
                        0f, Vector2.Zero, 3f + textSize, SpriteEffects.None, 0f);
                }
            }
            if (_explosionIntro > 0f)
            {
                var explosionSize = (int)(256 * (1f - _explosionIntro)) + 256;
                _batch.Draw(_texture, new Rectangle(startX + 32, startY + 32, explosionSize, explosionSize),
                    new Rectangle(96, 16, 32, 32), new Color(255, 255, 255, (int)(_explosionIntro * 255)),
                    _explosionIntro * 2f, new Vector2(16), SpriteEffects.None, 0f);
            }
        }

        private void DrawCommand()
        {
            if (_commandDelay > 0f)
            {
                var scale = 8f - 4f * _playGameIntro;
                var commandSize = _font.MeasureString(_activeGame.Command) * scale;
                var c = _activeGame.CommandColor;
                _batch.DrawString(_font, _activeGame.Command,
                    new Vector2(GameController.RENDER_WIDTH / 2 - commandSize.X / 2f, 260),
                    new Color(c.R, c.G, c.B, (int)(_playGameIntro * 255)), 0f, Vector2.Zero, scale, SpriteEffects.None, 0f);
            }
        }

        private void DrawOverlay()
        {
            if (_stage == ScreenStage.Lost || _isPaused)
            {
                for (int x = 0; x < GameController.RENDER_WIDTH; x += 64)
                {
                    for (int y = 0; y < GameController.RENDER_HEIGHT; y += 64)
                    {
                        _batch.Draw(_texture, new Rectangle(x, y, 64, 64),
                            new Rectangle(112, 112, 16, 16),
                            new Color(255, 255, 255, (int)(255 * _lostGameIntro)));
                    }
                }
            }
        }

        private void DrawPaused()
        {
            if (_isPaused || _pausedIntro > 0f)
            {
                for (int x = 0; x < GameController.RENDER_WIDTH; x += 64)
                {
                    for (int y = 0; y < GameController.RENDER_HEIGHT; y += 64)
                    {
                        _batch.Draw(_texture, new Rectangle(x, y, 64, 64),
                            new Rectangle(96, 112, 16, 16),
                            new Color(255, 255, 255, (int)(220 * _pausedIntro)));
                    }

                    _batch.Draw(_texture, new Rectangle(x, _pausedAnimation, 64, 64),
                        new Rectangle(96, 112, 16, 16),
                        new Color(255, 255, 255, (int)(20 * _pausedIntro)));
                }

                _batch.DrawString(_font, "PAUSED", new Vector2(140, 32),
                    new Color(255, 255, 255, (int)(255 * _pausedIntro)),
                    0f, Vector2.Zero, 3f, SpriteEffects.None, 0f);

                _batch.DrawRectangle(new Rectangle(100, 46, 6, 40), new Color(255, 255, 255, (int)(255 * _pausedIntro)));
                _batch.DrawRectangle(new Rectangle(80, 46, 6, 40), new Color(255, 255, 255, (int)(255 * _pausedIntro)));

                _batch.DrawRectangle(new Rectangle(50, 100, GameController.RENDER_WIDTH - 100, 4),
                    new Color(255, 255, 255, (int)(200 * _pausedIntro)));
                _batch.DrawRectangle(new Rectangle(50, GameController.RENDER_HEIGHT - 54, GameController.RENDER_WIDTH - 100, 4),
                    new Color(255, 255, 255, (int)(200 * _pausedIntro)));
                _batch.DrawRectangle(new Rectangle(50, 100, 4, GameController.RENDER_HEIGHT - 150),
                    new Color(255, 255, 255, (int)(200 * _pausedIntro)));
                _batch.DrawRectangle(new Rectangle(GameController.RENDER_WIDTH - 54, 100, 4, GameController.RENDER_HEIGHT - 150),
                    new Color(255, 255, 255, (int)(200 * _pausedIntro)));

                var buttons = _isDemo ? 
                    new[] { "Continue", "Bail" } :
                    new[] { "Continue", "Retry", "Bail" };
                for (int i = 0; i < buttons.Length; i++)
                {
                    const int width = 448;
                    var up = 200 + i * 120;
                    var left = GameController.RENDER_WIDTH / 2 - width / 2;
                    var c = new Color(255, 255, 255, (int)(200 * _pausedIntro));
                    if (i == _selection)
                    {
                        c = new Color(204, 255, 0, (int)(200 * _pausedIntro));
                        var checkeredEnd = (int)(_selectionIntro * width);
                        for (int x = 0; x < checkeredEnd; x += 32)
                        {
                            for (int y = 0; y < 96; y += 32)
                            {
                                _batch.Draw(_texture, new Rectangle(x + left, y + up, 32, 32), new Rectangle(112, 96, 16, 16),
                                    new Color(255, 255, 255, (int)(80 * _pausedIntro)));
                            }
                        }
                        var arrowEnd = (int)(_selectionIntro * 35) + left + 5;
                        _batch.DrawLine(new Vector2(left + 5, up), new Vector2(arrowEnd, up + 50), c, 5);
                        _batch.DrawLine(new Vector2(arrowEnd, up + 50), new Vector2(left + 5, up + 96), c, 5);
                    }
                    _batch.DrawRectangle(new Rectangle(left, up, width, 4), c);
                    _batch.DrawRectangle(new Rectangle(left, up + 96, width, 4), c);
                    _batch.DrawRectangle(new Rectangle(left, up, 4, 100), c);
                    _batch.DrawRectangle(new Rectangle(left + width - 4, up, 4, 100), c);

                    _batch.DrawString(_font, buttons[i], new Vector2(left + 50, up - 8),
                        c, 0f, Vector2.Zero, 5f, SpriteEffects.None, 0f);
                }
            }
        }

        private void DrawAwardRibbon()
        {
            if (_showAward || _awardIntro > 0f)
            {
                _batch.DrawRectangle(new Rectangle(0, 0, GameController.RENDER_WIDTH, GameController.RENDER_HEIGHT),
                    new Color(0, 0, 0, (int)(_awardIntro * 150)));

                var ribbonAlpha = (int)((_awardIntro * 2) * 255);
                var ribbonSize = (int)Math.Ceiling(_awardIntro * 48);
                if (ribbonAlpha > 255)
                    ribbonAlpha = 255;
                _batch.Draw(_awardRibbonTexture, new Rectangle(GameController.RENDER_WIDTH / 2 - 128, 280, 256, 384),
                    new Rectangle(0, 48 - ribbonSize, 32, ribbonSize), new Color(255, 255, 255, ribbonAlpha));

                var step = MathHelper.ToRadians(30);
                var circleTarget = MathHelper.TwoPi * _awardIntro;
                for (float r = 0; r < circleTarget; r += step)
                {
                    _batch.Draw(_awardRibbonTexture, new Rectangle(GameController.RENDER_WIDTH / 2, 210, 128, 128),
                        new Rectangle(32, 32, 16, 16), Color.White,
                        r, new Vector2(-4), SpriteEffects.FlipVertically, 0f);
                }

                var goldAlpha = (int)((_awardIntro * 3) * 255);
                if (goldAlpha > 255)
                    goldAlpha = 255;
                _batch.Draw(_awardRibbonTexture, new Rectangle(GameController.RENDER_WIDTH / 2 - 128, 80, 256, 256),
                    new Rectangle(32, 0, 32, 32), new Color(255, 255, 255, goldAlpha));

                if (_awardTextIntro > 0f)
                {
                    _batch.DrawRectangle(new Rectangle(GameController.RENDER_WIDTH / 2 - 400, 500, 800, 100),
                        new Color(30, 30, 30, (int)(150 * _awardTextIntro)));
                    _batch.DrawString(_font, "Challenge completed", new Vector2(GameController.RENDER_WIDTH / 2 - 330, 500),
                        new Color(239, 221, 50, (int)(255 * _awardTextIntro)),
                        0f, Vector2.Zero, 4f, SpriteEffects.None, 0f);
                }
            }
        }

        private void DrawClosingOverlay()
        {
            if (_closing)
            {
                var i = 0;
                var size = _closingIntro * 140;
                for (int x = 0; x < GameController.RENDER_WIDTH + 100; x += 70)
                {
                    if (i % 2 == 0)
                    {
                        _batch.DrawLine(new Vector2(x - 100, -100),
                            new Vector2(x + GameController.RENDER_HEIGHT + 100, GameController.RENDER_HEIGHT + 100), Color.Black, size);
                    }
                    else
                    {
                        _batch.DrawLine(new Vector2(x + 100, -100),
                            new Vector2(x - GameController.RENDER_HEIGHT - 100, GameController.RENDER_HEIGHT + 100), Color.Black, size);
                    }
                    i++;
                }
            }
        }

        internal override void Update(GameTime gameTime)
        {
            if (_closing)
            {
                UpdateClosing();
                return;
            }

            UpdatePause();

            if (!_isPaused)
            {
                _lifeAnimation += _speed;

                if (_explosionIntro > 0f)
                {
                    _explosionIntro -= 0.05f;
                    if (_explosionIntro <= 0f)
                        _explosionIntro = 0f;
                }

                switch (_stage)
                {
                    case ScreenStage.DisplayScore:
                        UpdateDisplayScore();
                        break;
                    case ScreenStage.PlayGame:
                        UpdatePlayGame(gameTime);
                        break;
                    case ScreenStage.Lost:
                        UpdateLostGame();
                        break;
                    case ScreenStage.LostLife:
                        UpdateLostLife();
                        break;
                }
            }
        }

        private void UpdateDisplayScore()
        {
            if (_speedUp)
            {
                if (_speedUpIntro1 > 0f)
                {
                    _speedUpIntro1 = MathHelper.Lerp(_speedUpIntro1, 0f, 0.2f);
                    if (_speedUpIntro1 <= 0.001f)
                        _speedUpIntro1 = 0f;
                }
                else if (_speedUpIntro2 < 1f)
                {
                    _speedUpIntro2 += 0.04f * _speed;
                    if (_speedUpIntro2 >= 1f)
                    {
                        _speedUpIntro2 = 0f;
                        _speedUp = false;
                    }
                }
            }

            if (_displayScoreDelay > 0)
            {
                if (_displayScoreOutro < 1f)
                {
                    _displayScoreOutro += 0.1f;
                    if (_displayScoreOutro >= 1f)
                    {
                        _displayScoreOutro = 1f;
                    }
                }
                else
                {
                    _displayScoreDelay -= 1;
                    if (_displayScoreDelay == 60)
                    {
                        if (_activeGame != null)
                        {
                            AfterGame();
                        }
                    }
                    if (_displayScoreDelay == 0 && _stage != ScreenStage.Lost)
                        _displayScoreOutro = 1f;
                }
            }
            else
            {
                if (_displayScoreOutro > 0f && !_speedUp)
                {
                    _displayScoreOutro -= 0.1f;
                    if (_displayScoreOutro <= 0)
                    {
                        _displayScoreOutro = 0f;
                        _playGameIntro = 0f;
                        _commandDelay = 100f;
                        _stage = ScreenStage.PlayGame;
                        StartGame();
                    }
                }
            }
        }

        private void UpdatePlayGame(GameTime gameTime)
        {
            if (_commandDelay > 0f)
                _commandDelay -= _speed;

            _activeGame.Update(gameTime);

            if (_completionTime > 0)
            {
                if (_playGameIntro < 1f)
                {
                    _playGameIntro += 0.07f * _speed;
                    if (_playGameIntro > 0.999f)
                        _playGameIntro = 1f;
                }

                _completionTime--;
                if (_completionTime <= 0f)
                {
                    _activeGame.TimeUp();
                    _completionTime = 0f;
                    _explosionIntro = 1f;
                }
            }
            else
            {
                if (_playGameIntro > 0f)
                {
                    _playGameIntro -= 0.07f * _speed;
                    if (_playGameIntro < 0.001f)
                    {
                        _playGameIntro = 0f;
                        _stage = ScreenStage.DisplayScore;
                        _displayScoreDelay = 80;
                        _displayScoreOutro = 0f;
                    }
                }
            }
        }

        private void UpdateLostGame()
        {
            if (_newHighscore)
            {
                _newHighscoreAnimation += 0.01f;
                while (_newHighscoreAnimation > 1f)
                    _newHighscoreAnimation -= 1f;
            }

            if (_gameOverIntro < 1f)
            {
                _gameOverIntro += 0.01f;
                if (_gameOverIntro >= 1f)
                {
                    _gameOverIntro = 1f;
                    if (_isDemo)
                    {
                        var introScreen = new IntroScreen();
                        introScreen.LoadContent();
                        var transitionScreen = new FadeTransitionScreen(this, introScreen);
                        transitionScreen.LoadContent();
                        GetComponent<ScreenManager>().SetScreen(transitionScreen);
                    }
                    else
                        _showAward = CompletedChallenge();
                }
            }
            else
            {
                if (_showAward)
                {
                    if (_awardIntro < 1f)
                    {
                        _awardIntro += 0.01f;
                        if (_awardIntro >= 1f)
                            _awardIntro = 1f;
                    }
                    else
                    {
                        if (_awardTextIntro < 1f)
                        {
                            _awardTextIntro += 0.01f;
                            if (_awardTextIntro >= 1f)
                                _awardTextIntro = 1f;
                        }
                        else
                        {
                            var keyboard = GetComponent<KeyboardHandler>();
                            var gamepad = GetComponent<GamePadHandler>();
                            if (keyboard.KeyPressed(Keys.Enter) || keyboard.KeyPressed(Keys.Space) ||
                                gamepad.ButtonPressed(PlayerIndex.One, Buttons.A))
                            {
                                _showAward = false;
                            }
                        }
                    }
                }
                else if (_awardIntro > 0f || _awardTextIntro > 0f)
                {
                    _awardIntro -= 0.05f;
                    _awardTextIntro -= 0.05f;
                    if (_awardIntro <= 0f)
                    {
                        _awardIntro = 0f;
                        _awardTextIntro = 0f;
                    }
                }
                else
                {
                    if (_lostGameIntro < 1f)
                    {
                        _lostGameIntro = MathHelper.Lerp(_lostGameIntro, 1f, 0.1f);
                        if (_lostGameIntro > 0.999f)
                            _lostGameIntro = 1f;
                    }
                    else
                    {
                        if (_receivedScores)
                        {
                            var controls = GetComponent<ControlsHandler>();
                            if (controls.DownPressed(PlayerIndex.One))
                            {
                                _selectionIntro = 0f;
                                _selection++;
                                if (_selection == 2)
                                    _selection = 0;
                            }
                            if (controls.UpPressed(PlayerIndex.One))
                            {
                                _selectionIntro = 0f;
                                _selection--;
                                if (_selection == -1)
                                    _selection = 1;
                            }
                            if (_selectionIntro < 1f)
                            {
                                _selectionIntro = MathHelper.Lerp(_selectionIntro, 1f, 0.2f);
                                if (_selectionIntro > 0.999f)
                                    _selectionIntro = 1f;
                            }

                            var keyboard = GetComponent<KeyboardHandler>();
                            var gamepad = GetComponent<GamePadHandler>();
                            if (keyboard.KeyPressed(Keys.Escape) || keyboard.KeyPressed(Keys.E) ||
                                gamepad.ButtonPressed(PlayerIndex.One, Buttons.B))
                            {
                                _closing = true;
                            }
                            if (keyboard.KeyPressed(Keys.Enter) || keyboard.KeyPressed(Keys.Space) ||
                                gamepad.ButtonPressed(PlayerIndex.One, Buttons.A))
                            {
                                switch (_selection)
                                {
                                    case 0:
                                        Restart();
                                        break;
                                    case 1:
                                        _closing = true;
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void UpdateClosing()
        {
            if (_closingIntro < 1f)
            {
                _closingIntro += 0.04f;
                if (_closingIntro >= 1f)
                {
                    Quit();
                }
            }
        }

        private void UpdateLostLife()
        {
            if (_lostLifeDelay < 1f)
            {
                _lostLifeDelay += 0.05f;
                if (_lostLifeDelay >= 1f)
                    _lostLifeDelay = 1f;
            }
            else
            {
                if (_lostLifeShakeIntro < 1f)
                {
                    _lostLifeShakeIntro += 0.04f;
                    if (_lostLifeShakeIntro >= 1f)
                        _lostLifeShakeIntro = 1f;

                    for (int i = 0; i < _lostLifeData.Length; i++)
                    {
                        var m = new Vector2((float)(_random.NextDouble() - 0.5),
                            (float)(_random.NextDouble() - 0.5)) * _lostLifeShakeIntro * 15f;
                        _lostLifeData[i].Position += m;
                    }
                }
                else
                {
                    if (_lostLifeData.Any(b => b.Position.Y < GameController.RENDER_HEIGHT + 200))
                    {
                        _lostLifeOutro += 0.7f;

                        for (int i = 0; i < _lostLifeData.Length; i++)
                        {
                            var m = new Vector2(((_lostLifeData[i].TextureIndex % 3) - 1) * 6f, _lostLifeOutro);
                            _lostLifeData[i].Position += m;
                        }
                    }
                    else
                    {
                        if (_lifes == 0)
                        {
                            _stage = ScreenStage.Lost;
                            PostScore();
                        }
                        else
                        {
                            _stage = ScreenStage.DisplayScore;
                        }
                    }
                }
            }
        }

        private void UpdatePause()
        {
            if (_lifes > 0)
            {
                if (!_isPaused)
                {
                    if (_pausedIntro > 0f)
                    {
                        _pausedIntro -= 0.15f;
                        if (_pausedIntro <= 0f)
                            _pausedIntro = 0f;
                    }
                    var keyboard = GetComponent<KeyboardHandler>();
                    var gamepad = GetComponent<GamePadHandler>();
                    if (keyboard.KeyPressed(Keys.Escape) ||
                        gamepad.ButtonPressed(PlayerIndex.One, Buttons.Start))
                    {
                        _isPaused = true;
                    }
                }
                else
                {
                    _pausedAnimation += 3;
                    _pausedAnimation = _pausedAnimation % GameController.RENDER_HEIGHT;

                    if (_pausedIntro < 1f)
                    {
                        _pausedIntro += 0.1f;
                        if (_pausedIntro >= 1f)
                            _pausedIntro = 1f;
                    }
                    else
                    {
                        var controls = GetComponent<ControlsHandler>();
                        var buttons = _isDemo ?
                            new[] { "Continue", "Bail" } :
                            new[] { "Continue", "Retry", "Bail" };
                        if (controls.DownPressed(PlayerIndex.One))
                        {
                            _selectionIntro = 0f;
                            _selection++;
                            if (_selection == buttons.Length)
                                _selection = 0;
                        }
                        if (controls.UpPressed(PlayerIndex.One))
                        {
                            _selectionIntro = 0f;
                            _selection--;
                            if (_selection == -1)
                                _selection = buttons.Length - 1;
                        }
                        if (_selectionIntro < 1f)
                        {
                            _selectionIntro = MathHelper.Lerp(_selectionIntro, 1f, 0.2f);
                            if (_selectionIntro > 0.999f)
                                _selectionIntro = 1f;
                        }

                        var keyboard = GetComponent<KeyboardHandler>();
                        var gamepad = GetComponent<GamePadHandler>();
                        if (keyboard.KeyPressed(Keys.Escape) || keyboard.KeyPressed(Keys.E) ||
                            gamepad.ButtonPressed(PlayerIndex.One, Buttons.B))
                        {
                            _isPaused = false;
                            _selectionIntro = 0f;
                            _selection = 0;
                        }
                        if (keyboard.KeyPressed(Keys.Enter) || keyboard.KeyPressed(Keys.Space) ||
                            gamepad.ButtonPressed(PlayerIndex.One, Buttons.A))
                        {
                            switch (buttons[_selection])
                            {
                                case "Continue":
                                    _isPaused = false;
                                    _selectionIntro = 0f;
                                    _selection = 0;
                                    break;
                                case "Retry":
                                    Restart();
                                    break;
                                case "Bail":
                                    _closing = true;
                                    break;
                            }
                        }
                    }
                }
            }
        }

        protected void SpeedUp(float increase)
        {
            _speed += increase;
            _speedUp = true;
            _speedUpIntro1 = 1f;
            _speedUpIntro2 = 0f;
        }

        protected void LoseLife()
        {
            _lifes--;
            _stage = ScreenStage.LostLife;
            _lostLifeDelay = 0f;
            _lostLifeShakeIntro = 0f;
            _lostLifeOutro = 0f;
            LoadLifeData();
        }

        protected abstract Microgame GetNextGame();
        protected abstract void AfterGame();
        protected abstract void PostScore();
        protected abstract void Restart();
        protected abstract bool CompletedChallenge();

        private void StartGame()
        {
            _activeGame?.UnloadContent();
            _activeGame = GetNextGame();
            _activeGame.Speed = _speed;
            _activeGame.Difficulty = _difficulty;
            _activeGame.LoadContent();
            _completionTime = (_activeGame.CompletionTime / _speed) * 60;
            _totalCompletionTime = _completionTime;
            _activeGame.GameStart();
        }

        protected virtual void Quit()
        {
            var transitionScreen = new FadeTransitionScreen(this, _preScreen);
            transitionScreen.LoadContent();
            GetComponent<ScreenManager>().SetScreen(transitionScreen);
        }

        internal override void DeselectedWindow()
        {
            if (_lifes > 0)
            {
                _isPaused = true;
            }
        }
    }
}
