﻿using JoltNet;
using PlayBolt.Scenes.InGame.Games;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Core;

namespace PlayBolt.Scenes.InGame.Playlists
{
    abstract class Playlist
    {
        private const string COMPLETED_PLAYLISTS_KEY = "completed_playlists";
        private static List<int> _completedPlaylists;
        private static Playlist[] _playlists;

        public static async Task LoadCompletedPlaylists()
        {
            var storageRequest = RequestProvider.Storage.Fetch(COMPLETED_PLAYLISTS_KEY, Controller.GameJoltUsername, Controller.GameJoltToken);
            await Controller.GameJoltClient.ExecuteRequestAsync(storageRequest);

            if (!storageRequest.Response.Success)
                _completedPlaylists = new List<int>();
            else
            {
                _completedPlaylists = storageRequest.Response.Data
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(str => Convert.ToInt32(str)).ToList();
            }
        }

        private static void LoadPlaylistBuffer()
        {
            if (_playlists == null)
            {
                var playlistType = typeof(Playlist);
                _playlists = playlistType.Assembly.GetTypes()
                    .Where(t => t.IsSubclassOf(playlistType))
                    .Select(t => (Playlist)Activator.CreateInstance(t)).ToArray();
            }
        }

        public static Playlist GetPlaylist(string name)
        {
            LoadPlaylistBuffer();
            return _playlists.First(p => p.Name == name);
        }

        public static Playlist[] GetAvailablePlaylists()
        {
            LoadPlaylistBuffer();
            return _playlists.Take(_completedPlaylists.Count + 1).ToArray();
        }

        public async Task CompletedPlaylist()
        {
            if (!_completedPlaylists.Contains(Id))
            {
                IGameJoltRequest request;
                if (_completedPlaylists.Count == 0)
                {
                    request = RequestProvider.Storage.Set(COMPLETED_PLAYLISTS_KEY, Id.ToString(), Controller.GameJoltUsername, Controller.GameJoltToken);
                }
                else
                {
                    request = RequestProvider.Storage.Update(COMPLETED_PLAYLISTS_KEY, StorageUpdateOperation.Append,
                        "," + Id.ToString(), Controller.GameJoltUsername, Controller.GameJoltToken);
                }
                await Controller.GameJoltClient.ExecuteRequestAsync(request);

                // update local table
                _completedPlaylists.Add(Id);
            }
        }

        public bool IsCompleted => _completedPlaylists.Contains(Id);

        public abstract int Id { get; }
        public abstract string Name { get; }
        public abstract string Description { get; }
        public abstract string Scoreboard { get; }

        public Microgame[] GetGames()
        {
            return GetType().Assembly.GetTypes()
                .Where(t => Attribute.GetCustomAttribute(t, typeof(MicrogameDescriptionAttribute)) != null)
                .Select(t => (Microgame)Activator.CreateInstance(t))
                .Where(g => g.Playlist == Name).ToArray();
        }
    }
}
