﻿using GameDevCommon.Drawing;
using GameDevCommon.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace PlayBolt.Scenes.InGame.Games.Intro
{
    [MicrogameDescription(8, "Vacuum", "Intro", "Use the vacuum to clean all the dirt off the floor.")]
    class Vacuum : Microgame
    {
        public override float CompletionTime => 5;
        public override string Command => "Clean!";
        public override string Scoretable => "289541";
        public override int TargetScore => 20;

        class Dirt
        {
            private int _x, _y, _size;
            private Rectangle _textureRectangle;

            public Rectangle Bounds
                => new Rectangle(_x, _y, _size, _size);

            public float Life;

            public Dirt(Difficulty difficulty)
            {
                int max = 2;
                if (difficulty == Difficulty.Medium)
                    max = 3;
                if (difficulty == Difficulty.Hard)
                    max = 4;
                switch (_random.Next(0, max))
                {
                    case 0:
                        _size = _random.Next(16, 24);
                        _textureRectangle = new Rectangle(32, 0, 16, 16);
                        Life = 1f;
                        break;
                    case 1:
                        _size = _random.Next(16, 24);
                        _textureRectangle = new Rectangle(32, 16, 16, 16);
                        Life = 1.5f;
                        break;
                    case 2:
                        _size = _random.Next(32, 48);
                        _textureRectangle = new Rectangle(0, 0, 32, 32);
                        Life = 2f;
                        break;
                    case 3:
                        _size = _random.Next(32, 48);
                        _textureRectangle = new Rectangle(0, 32, 32, 32);
                        Life = 3f;
                        break;
                }

                _x = (int)(_random.Next(50, HEIGHT - 50 + 1) + (WIDTH - HEIGHT) / 2f);
                _y = _random.Next(50, HEIGHT - 50 + 1);
            }

            public void Draw(SpriteBatch batch, Texture2D texture)
            {
                batch.Draw(texture, Bounds, _textureRectangle, Color.White);
            }
        }

        private List<Dirt> _dirt = new List<Dirt>();
        private Vector2 _position;
        private bool _offset = false;

        public override void GameStart()
        {
            _position = new Vector2(HEIGHT / 2f + (WIDTH - HEIGHT) / 2f - 64, HEIGHT / 2f - 32);

            var amount = 2;
            if (Difficulty == Difficulty.Medium)
                amount = 3;
            else if (Difficulty == Difficulty.Hard)
                amount = 4;

            for (int i = 0; i < amount; i++)
            {
                _dirt.Add(new Dirt(Difficulty));
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteBatchUsage.Default);

            _batch.DrawRectangle(new Rectangle(0, 0, WIDTH, HEIGHT), new Color(100, 100, 100));

            for (int x = 0; x < HEIGHT; x += 32)
            {
                for (int y = 0; y < HEIGHT; y += 32)
                {
                    _batch.Draw(_texture, new Rectangle(x + (WIDTH - HEIGHT) / 2, y, 32, 32), new Rectangle(32, 32, 16, 16), Color.White);
                }
            }

            foreach (var dirt in _dirt)
            {
                dirt.Draw(_batch, _texture);
            }

            var offset = new Vector2();
            if (_offset)
                offset = new Vector2(_random.Next(-2, 3), _random.Next(-2, 3));

            var playerRect = new Rectangle((int)(_position.X + offset.X), (int)(_position.Y + offset.Y), 128, 128);
            _batch.Draw(_texture, playerRect, new Rectangle(48, 0, 64, 64), Color.White);
            _batch.Draw(_texture, new Rectangle((int)(_position.X + 50 + offset.X), (int)(_position.Y + 128 + offset.Y), 28, 500),
                new Rectangle(73, 63, 14, 1), Color.White);

            _batch.End();
        }

        public override void Update(GameTime gameTime)
        {
            _offset = false;

            var playerRect = new Rectangle((int)(_position.X), (int)(_position.Y), 128, 64);
            for (int i = 0; i < _dirt.Count; i++)
            {
                var dirt = _dirt[i];
                if (playerRect.Contains(dirt.Bounds))
                {
                    _offset = true;
                    dirt.Life -= 0.1f * Speed;
                    if (dirt.Life <= 0f)
                    {
                        _dirt.Remove(dirt);
                        i--;
                    }
                }
            }

            if (!_offset)
            {
                if (DirectionDown(InputDirection.Left))
                {
                    _position.X -= 9f * Speed;
                    if (_position.X <= (WIDTH - HEIGHT) / 2)
                        _position.X = (WIDTH - HEIGHT) / 2;
                }
                if (DirectionDown(InputDirection.Right))
                {
                    _position.X += 9f * Speed;
                    if (_position.X >= (WIDTH - HEIGHT) / 2 + HEIGHT - 128)
                        _position.X = (WIDTH - HEIGHT) / 2 + HEIGHT - 128;
                }
                if (DirectionDown(InputDirection.Up))
                {
                    _position.Y -= 9f * Speed;
                    if (_position.Y <= 0)
                        _position.Y = 0;
                }
                if (DirectionDown(InputDirection.Down))
                {
                    _position.Y += 9f * Speed;
                    if (_position.Y >= HEIGHT - 64)
                        _position.Y = HEIGHT - 64;
                }
            }
        }

        public override void TimeUp()
        {
            if (_dirt.Count > 0)
            {
                WinState = WinState.Lost;
            }
            else
            {
                WinState = WinState.Won;
            }
        }
    }
}
