﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;
using System.Linq;

namespace PlayBolt.World.Objects.Ferris
{
    class FerrisCabin : WorldObject
    {
        public static FerrisCabin[] Generate(WorldScreen screen, Vector3 position)
        {
            return Enumerable.Range(0, 8)
                .Select(i => new FerrisCabin(screen, position, MathHelper.PiOver4 * i)).ToArray();
        }

        private float _circlePos = 0f;

        public FerrisCabin(WorldScreen screen, Vector3 position, float offset)
            : base(screen, "ferris_cabin", position)
        {
            _circlePos = offset;
            IsOptimizable = false;
        }

        public override void Update()
        {
            _circlePos -= 0.002f;
            CreateWorld();

            base.Update();
        }

        protected override void CreateWorld()
        {
            World = Matrix.CreateFromYawPitchRoll(_rotation.Y, _rotation.X, _rotation.Z - _circlePos)
                * Matrix.CreateTranslation(new Vector3(33, 0, 0))
                * Matrix.CreateRotationZ(_circlePos)
                * Matrix.CreateTranslation(_position);
        }

        protected override void CreateGeometryInternal()
        {
            // body
            {
                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                    new TextureRectangle(new Rectangle(32, 133, 32, 19), Texture));
                texture.AddSide(new[] { CuboidSide.Top },
                    new TextureRectangle(new Rectangle(80, 136, 32, 16), Texture));
                texture.AddSide(new[] { CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(80, 120, 32, 16), Texture));
                texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                    new TextureRectangle(new Rectangle(64, 133, 16, 19), Texture));
                var vertices = CuboidComposer.Create(4f, 2f, 2f, texture);
                Geometry.AddVertices(vertices);
            }
            // bars
            {
                ITextureDefintion getTexture()
                {
                    var texture = new TextureCuboidWrapper();
                    texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back, CuboidSide.Left, CuboidSide.Right },
                        new TextureRectangle(new Rectangle(32, 120, 2, 13), Texture));
                    texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                        new TextureRectangle(new Rectangle(32, 120, 2, 2), Texture));
                    return texture;
                }
                // bar1
                {
                    var vertices = CuboidComposer.Create(0.2f, 2f, 0.2f, getTexture());
                    VertexTransformer.Offset(vertices, new Vector3(-1.9f, 2f, -0.9f));
                    Geometry.AddVertices(vertices);
                }
                // bar2
                {
                    var vertices = CuboidComposer.Create(0.2f, 2f, 0.2f, getTexture());
                    VertexTransformer.Offset(vertices, new Vector3(1.9f, 2f, -0.9f));
                    Geometry.AddVertices(vertices);
                }
                // bar3
                {
                    var vertices = CuboidComposer.Create(0.2f, 2f, 0.2f, getTexture());
                    VertexTransformer.Offset(vertices, new Vector3(-1.9f, 2f, 0.9f));
                    Geometry.AddVertices(vertices);
                }
                // bar4
                {
                    var vertices = CuboidComposer.Create(0.2f, 2f, 0.2f, getTexture());
                    VertexTransformer.Offset(vertices, new Vector3(1.9f, 2f, 0.9f));
                    Geometry.AddVertices(vertices);
                }
            }
            // roof
            {
                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                    new TextureRectangle(new Rectangle(48, 120, 32, 4), Texture));
                texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(48, 120, 32, 11), Texture));
                texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                    new TextureRectangle(new Rectangle(48, 120, 1, 11), Texture));
                var vertices = CuboidComposer.Create(4f, 0.2f, 2f, texture);
                VertexTransformer.Offset(vertices, new Vector3(0f, 3.1f, 0f));
                Geometry.AddVertices(vertices);
            }
            // connectors
            {
                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back, CuboidSide.Left, CuboidSide.Right },
                    new TextureRectangle(new Rectangle(32, 120, 2, 13), Texture));
                texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(32, 120, 2, 2), Texture));
                var vertices = CuboidComposer.Create(0.2f, 0.2f, 6f, texture);
                VertexTransformer.Offset(vertices, new Vector3(0, 3.2f, 0));
                Geometry.AddVertices(vertices);
            }
        }
    }
}
