﻿using GameDevCommon.Drawing;
using JoltNet;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayBolt.Content;
using PlayBolt.Scenes.InGame.Games;
using System.Threading.Tasks;
using static Core;

namespace PlayBolt.Scenes.InGame
{
    class SingleInGameScreen : InGameScreen
    {
        private Microgame _game;
        private Texture2D _background;
        private float _backgroundOffset = 0f;

        public SingleInGameScreen(Screen preScreen, Microgame game)
            : base(preScreen)
        {
            _lifes = 4;
            _speed = 1f;
            _game = game;
        }

        internal override void LoadContent()
        {
            base.LoadContent();
            _background = GetComponent<Resources>().LoadTexture("Textures/Games/Background0.png");
        }

        internal override void UnloadContent()
        {
            base.UnloadContent();
            _background = null;
        }

        protected override void AfterGame()
        {
            if (_activeGame.WinState == WinState.Lost)
            {
                LoseLife();
            }
            if (_lifes > 0)
            {
                switch (_difficulty)
                {
                    case Difficulty.Easy:
                        _difficulty = Difficulty.Medium;
                        break;
                    case Difficulty.Medium:
                        _difficulty = Difficulty.Hard;
                        break;
                    case Difficulty.Hard:
                        _difficulty = Difficulty.Easy;
                        SpeedUp(0.2f);
                        break;
                }
                _score++;
            }
        }

        protected override Microgame GetNextGame()
        {
            return _game.Create();
        }

        protected override bool CompletedChallenge()
        {
            if (_score >= _game.TargetScore && !_game.IsCompleted)
            {
                Task.Run(async () => await _game.CompletedGame());
                return true;
            }
            else
            {
                return false;
            }
        }

        protected override void PostScore()
        {
            Task.Run(async () =>
            {
                var tableId = _game.Scoretable;
                var oldScoreRequest = RequestProvider.Scores.Fetch(tableId, Controller.GameJoltUsername, Controller.GameJoltToken);
                var oldRank = -1;
                await Controller.GameJoltClient.ExecuteRequestAsync(oldScoreRequest);
                if (oldScoreRequest.Response.Scores.Length > 0)
                {
                    var oldRankRequest = RequestProvider.Scores.FetchRank(oldScoreRequest.Response.Scores[0].Sort);
                    await Controller.GameJoltClient.ExecuteRequestAsync(oldRankRequest);
                    oldRank = oldRankRequest.Response.Rank;
                }

                if ((oldScoreRequest.Response.Scores.Length > 0 &&
                    oldScoreRequest.Response.Scores[0].Sort < _score) ||
                    oldScoreRequest.Response.Scores.Length == 0)
                {
                    var timeRequest = RequestProvider.Time.Get();
                    await Controller.GameJoltClient.ExecuteRequestAsync(timeRequest);
                    var addScoreRequest = RequestProvider.Scores.Add(tableId, _score, _score.ToString(),
                        Controller.GameJoltUsername, Controller.GameJoltToken,
                        timeRequest.Response.Timestamp.ToString());
                    await Controller.GameJoltClient.ExecuteRequestAsync(addScoreRequest);
                }

                _highscoreData = await HighscoreProvider.GetFullScoreData(_game.Scoretable, 3);

                if (oldRank == -1 || oldScoreRequest.Response.Scores[0].Sort < _score)
                {
                    _newHighscore = true;
                }
                else if (oldRank > _highscoreData.OwnRank)
                {
                    _newHighscore = true;
                    _rankDifference = oldRank - _highscoreData.OwnRank;
                }

                _receivedScores = true;
            });
        }

        protected override void Restart()
        {
            // unload current game screen, then load new one.
            UnloadContent();

            var screen = new SingleInGameScreen(_preScreen, _game);
            screen.LoadContent();
            GetComponent<ScreenManager>().SetScreen(screen);
        }

        internal override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (!_isPaused)
                _backgroundOffset = MathHelper.Lerp(_backgroundOffset, _score * 50f, 0.05f);
        }

        protected override void DrawBackground()
        {
            _batch.DrawRectangle(new Rectangle(0, 0, GameController.RENDER_WIDTH, GameController.RENDER_HEIGHT), new Color(41, 41, 41));
            _batch.Draw(_background, new Rectangle(0, (int)(-_background.Height * 6 + _backgroundOffset + 800),
                _background.Width * 6, _background.Height * 6), new Color(255, 255, 255, 100 + (int)(_gameOverIntro * 155)));
        }
    }
}
