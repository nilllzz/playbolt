﻿using Microsoft.Xna.Framework;

namespace PlayBolt.World
{
    public struct DirectionalLightConfiguration
    {
        public Vector3 Direction;
        public Color Color;
        public float Intensity;
    }

}
