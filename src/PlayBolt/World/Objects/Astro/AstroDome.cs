﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;
using System;

namespace PlayBolt.World.Objects.Astro
{
    class AstroDome : WorldObject
    {
        private static Random _random = new Random();
        private float _targetRotation = 0f;

        public AstroDome(WorldScreen screen, Vector3 position)
            : base(screen, "astro_astrodome", position)
        {
            IsOptimizable = false;
        }

        public override void Update()
        {
            if (_targetRotation != _rotation.Y)
            {
                if (_rotation.Y > _targetRotation)
                {
                    _rotation.Y = MathHelper.Lerp(_rotation.Y, _targetRotation, 0.002f);
                    if (_rotation.Y - 0.005f <= _targetRotation)
                    {
                        _rotation.Y = _targetRotation;
                    }
                    CreateWorld();
                }
                else
                {
                    _rotation.Y = MathHelper.Lerp(_rotation.Y, _targetRotation, 0.002f);
                    if (_rotation.Y + 0.005f >= _targetRotation)
                    {
                        _rotation.Y = _targetRotation;
                    }
                    CreateWorld();
                }
            }
            else
            {
                if (_random.Next(0, 80) == 0)
                {
                    _targetRotation = (float)(_random.NextDouble() * MathHelper.TwoPi);
                }
            }

            base.Update();
        }

        protected override void CreateGeometryInternal()
        {
            // dome
            {
                var textures = new[]
                {
                    new TextureRectangle(new Rectangle(288, 0, 16, 64), Texture),
                    new TextureRectangle(new Rectangle(288, 0, 16, 64), Texture),
                    new TextureRectangle(new Rectangle(304, 0, 16, 48), Texture),
                    new TextureRectangle(new Rectangle(304, 0, 16, 48), Texture),
                    new TextureRectangle(new Rectangle(320, 0, 16, 32), Texture),
                    new TextureRectangle(new Rectangle(320, 0, 16, 32), Texture)
                };
                var vertices = DomeComposer.Create(24, 15, 14, 6, textures);
                VertexTransformer.Rotate(vertices, new Vector3(0, 0, MathHelper.PiOver2));
                Geometry.AddVertices(vertices);
            }
            // telescope
            {
                var vertices = CylinderComposer.Create(3, 15, 10,
                    new TextureRectangle(new Rectangle(304, 48, 32, 8), Texture),
                    new TextureRectangle(new Rectangle(368, 0, 32, 32), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(0, 0, 0.7f));
                VertexTransformer.Offset(vertices, new Vector3(20, 7, 0));
                Geometry.AddVertices(vertices);
            }
        }
    }
}
