﻿using GameDevCommon.Rendering;
using Microsoft.Xna.Framework;
using System;
using static Core;

namespace PlayBolt.Scenes.Start
{
    class StartCamera : PerspectiveCamera
    {
        private float _targetYaw, _targetPitch, _targetRoll;
        private Random _random = new Random();

        public StartCamera()
        {
            Yaw = -0.2f;
            Pitch = 0.1f;
            FOV = 90;
            FarPlane = 2000;
            NearPlane = 0.1f;

            Position = new Vector3(-2.429613f, 2.221995f, 63.50832f);

            CreateProjection();
            CreateView();

            FOVChanged += CreateProjection;

            _targetYaw = Yaw;
            _targetPitch = Pitch;
            _targetRoll = Roll;
        }

        public override void Update()
        {
            if (_random.Next(0, 50) == 0)
            {
                switch (_random.Next(0, 3))
                {
                    case 0:
                        _targetYaw = _random.Next(-10, 10) / 1000f - 0.2f;
                        break;
                    case 1:
                        _targetPitch = _random.Next(-10, 10) / 1000f + 0.1f;
                        break;
                    case 2:
                        _targetRoll = _random.Next(-10, 10) / 1000f;
                        break;
                }
            }

            Yaw = MathHelper.Lerp(Yaw, _targetYaw, 0.01f);
            Pitch = MathHelper.Lerp(Pitch, _targetPitch, 0.01f);
            Roll = MathHelper.Lerp(Roll, _targetRoll, 0.01f);

            CreateView();
        }
    }
}
