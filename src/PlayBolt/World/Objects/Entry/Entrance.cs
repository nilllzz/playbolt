﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Entry
{
    class Entrance : WorldObject
    {
        public Entrance(WorldScreen screen, Vector3 position)
            : base(screen, "entry_entrance", position)
        { }

        protected override void CreateGeometryInternal()
        {
            var poleVertices = CylinderComposer.Create(1f, 8f, 10,
                new TextureTubeWrapper(new Rectangle(0, 0, 64, 48), Texture.Bounds, 10),
                new TextureRectangle(new Rectangle(64, 0, 16, 16), Texture));
            VertexTransformer.Rotate(poleVertices, new Vector3(0, 0, MathHelper.PiOver2));
            VertexTransformer.Offset(poleVertices, new Vector3(-5, 4.5f, 0));
            Geometry.AddVertices(poleVertices);
            VertexTransformer.Offset(poleVertices, new Vector3(10, 0, 0));
            Geometry.AddVertices(poleVertices);

            var signTexture = new TextureCuboidWrapper();
            signTexture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                new TextureRectangle(new Rectangle(64, 16, 192, 32), Texture));
            signTexture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom, CuboidSide.Left, CuboidSide.Right },
                new TextureRectangle(new Rectangle(64, 16, 1, 1), Texture));
            var signVertices = CuboidComposer.Create(11f, 2f, 0.2f, signTexture);
            VertexTransformer.Rotate(poleVertices, new Vector3(0, 0, 0.1f));
            VertexTransformer.Offset(signVertices, new Vector3(0, 7, 0.9f));
            Geometry.AddVertices(signVertices);

            // block
            {
                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(0, 48, 1, 1), Texture));
                texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right, CuboidSide.Front, CuboidSide.Back },
                    new TextureRectangle(new Rectangle(0, 48, 16, 16), Texture));
                var vertices = CuboidComposer.Create(1f, 2f, 1f, texture);
                VertexTransformer.Offset(vertices, new Vector3(-3f, 1.3f, 0));
                Geometry.AddVertices(vertices);
            }
        }
    }
}
