﻿using Microsoft.Xna.Framework;
using PlayBolt.Scenes.Park;

namespace PlayBolt
{
    class ScreenManager : IGameComponent
    {
        internal Screen ActiveScreen { get; private set; }

        public void Initialize()
        {
            //var screen = new Scenes.Intro.IntroScreen();
            var screen = new Scenes.Menu.GameJoltLoginScreen();
            //var screen = new ParkScreen();
            SetScreen(screen);
        }

        internal void SetScreen(Screen screen)
        {
            ActiveScreen?.Deactivate();
            ActiveScreen = screen;
            screen.Activate();
        }
    }
}
