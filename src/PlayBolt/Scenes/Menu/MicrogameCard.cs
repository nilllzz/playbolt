﻿using GameDevCommon.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayBolt.Content;
using PlayBolt.Scenes.InGame.Games;
using static Core;

namespace PlayBolt.Scenes.Menu
{
    class MicrogameCard
    {
        private int _x, _y;
        private float _selectionIntro = 0f;
        private float _descriptionOffset = -70f;
        private Microgame _game;
        private Texture2D _menuTexture;

        public MicrogameCard(Microgame game, Texture2D menuTexture, int x, int y)
        {
            _game = game;
            _x = x;
            _y = y;
            _menuTexture = menuTexture;
        }

        public void Draw(SpriteBatch batch, SpriteFont font, SpriteBatch fontBatch, RenderTarget2D target, bool selected)
        {
            var thumbTexture = GetComponent<Resources>().LoadTexture("Textures/Games/Thumbnails/" + _game.Thumbnail);

            if (selected)
            {
                batch.DrawRectangle(new Rectangle(_x - 4, _y - 4, 278, 204), new Color(41, 41, 41, (int)(255 * _selectionIntro)));
                batch.DrawRectangle(new Rectangle(_x - 2, _y - 2, 274, 154), new Color(204, 255, 0, (int)(255 * _selectionIntro)));
                batch.DrawString(font, _game.Name, new Vector2(_x + 40, _y + 172), new Color(0, 0, 0, 255 - (int)(255 * _selectionIntro)));
                batch.DrawString(font, _game.Name, new Vector2(_x + 40, _y + 172), new Color(255, 255, 255, (int)(255 * _selectionIntro)));
            }
            else
            {
                batch.DrawRectangle(new Rectangle(_x - 2, _y - 2, 274, 154), new Color(41, 41, 41));
                batch.DrawString(font, _game.Name, new Vector2(_x + 40, _y + 172), Color.Black);
            }

            batch.Draw(thumbTexture, new Rectangle(_x, _y, 270, 150), Color.White);
            batch.DrawString(font, _game.Playlist, new Vector2(_x + 40, _y + 154), new Color(180, 180, 180));

            if (_game.IsCompleted)
            {
                batch.Draw(_menuTexture, new Rectangle(_x + 234, _y + 114, 32, 32), new Rectangle(64, 0, 16, 16), Color.White);
            }

            if (selected)
            {
                var currentTarget = (RenderTarget2D)Controller.GraphicsDevice.GetRenderTargets()[0].RenderTarget;
                Controller.GraphicsDevice.SetRenderTarget(target);
                Controller.GraphicsDevice.Clear(Color.Transparent);

                fontBatch.Begin(SpriteBatchUsage.Default);
                fontBatch.DrawRectangle(new Rectangle(0, 0, 270, 20), new Color(41, 41, 41, (int)(200 * _selectionIntro)));
                fontBatch.DrawString(font, _game.Description, new Vector2(2 - _descriptionOffset, 0), new Color(255, 255, 255, (int)(255 * _selectionIntro)));
                if (!_game.IsCompleted)
                {
                    var descSize = font.MeasureString(_game.Description).X;
                    fontBatch.DrawString(font, " (earn " + _game.TargetScore + " points)", new Vector2(2 - _descriptionOffset + descSize, 0),
                        new Color(74, 205, 255, (int)(255 * _selectionIntro)));
                }
                
                fontBatch.End();

                Controller.GraphicsDevice.SetRenderTarget(currentTarget);

                batch.Draw(target, new Vector2(_x, _y), Color.White);
            }
        }

        public void Update(bool selected, SpriteFont font)
        {
            if (selected)
            {
                if (_selectionIntro < 1f)
                {
                    _selectionIntro = MathHelper.Lerp(_selectionIntro, 1f, 0.2f);
                    if (_selectionIntro > 0.999f)
                        _selectionIntro = 1f;
                }

                var desc = _game.Description;
                if (!_game.IsCompleted)
                    desc += " (earn " + _game.TargetScore + " points)";

                var descSize = font.MeasureString(desc).X;
                if (descSize > 270)
                {
                    _descriptionOffset += 1f;
                    if (_descriptionOffset >= descSize)
                    {
                        _descriptionOffset = -270;
                    }
                }
                else
                {
                    _descriptionOffset = 0f;
                }
            }
            else
            {
                if (_selectionIntro > 0f)
                {
                    _selectionIntro = MathHelper.Lerp(_selectionIntro, 0f, 0.2f);
                    if (_selectionIntro < 0.001f)
                        _selectionIntro = 0f;
                }
            }
        }
    }
}
