﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Gift
{
    class GiftShop : WorldObject
    {
        public GiftShop(WorldScreen screen, Vector3 position)
            : base(screen, "gift_shop", position)
        { }

        protected override void CreateGeometryInternal()
        {
            // back wall
            {
                var vertices = RectangleComposer.Create(10, 10,
                    new TextureRectangle(new Rectangle(0, 160, 64, 64), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, 0, 0));
                VertexTransformer.Offset(vertices, new Vector3(-5, 5, 0));
                Geometry.AddVertices(vertices);
                VertexTransformer.Offset(vertices, new Vector3(10, 0, 0));
                Geometry.AddVertices(vertices);
            }
            // right wall
            {
                var vertices = RectangleComposer.Create(15, 10,
                    new TextureRectangle(new Rectangle(0, 160, 96, 64), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, MathHelper.PiOver2, 0));
                VertexTransformer.Offset(vertices, new Vector3(10, 5, -7.5f));
                Geometry.AddVertices(vertices);
            }
            // left wall
            {
                var vertices = RectangleComposer.Create(15, 10,
                    new TextureRectangle(new Rectangle(0, 160, 96, 64), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, -MathHelper.PiOver2, 0));
                VertexTransformer.Offset(vertices, new Vector3(-10, 5, -7.5f));
                Geometry.AddVertices(vertices);
            }
            // floor
            {
                var vertices = RectangleComposer.Create(20, 15,
                    new TextureRectangle(new Rectangle(0, 224, 48, 32), Texture));
                VertexTransformer.Offset(vertices, new Vector3(0, 0.1f, -7.5f));
                Geometry.AddVertices(vertices);
            }
            // roof
            {
                var vertices = PyramidComposer.Create(14, 6, 4, 1, 
                    new TextureRectangle(new Rectangle(48, 224, 32, 32), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver4, 0, MathHelper.PiOver2));
                VertexTransformer.Scale(vertices, new Vector3(1.01f, 1f, 0.76f));
                VertexTransformer.Offset(vertices, new Vector3(0f, 13f, -7.5f));
                Geometry.AddVertices(vertices);
            }
            // storefront
            {
                // bottom
                {
                    var vertices = RectangleComposer.Create(10, 3.75f,
                        new TextureRectangle(new Rectangle(0, 200, 64, 24), Texture));
                    VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, MathHelper.Pi, 0));
                    VertexTransformer.Offset(vertices, new Vector3(-5, 1.875f, -15));
                    Geometry.AddVertices(vertices);
                    VertexTransformer.Offset(vertices, new Vector3(10, 0, 0));
                    Geometry.AddVertices(vertices);
                }
                // top
                {
                    var vertices = RectangleComposer.Create(10, 2.5f,
                        new TextureRectangle(new Rectangle(0, 160, 64, 16), Texture));
                    VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, MathHelper.Pi, 0));
                    VertexTransformer.Offset(vertices, new Vector3(-5, 8.75f, -15));
                    Geometry.AddVertices(vertices);
                    VertexTransformer.Offset(vertices, new Vector3(10, 0, 0));
                    Geometry.AddVertices(vertices);
                }
                // left
                {
                    var vertices = RectangleComposer.Create(2.5f, 3.75f,
                        new TextureRectangle(new Rectangle(0, 176, 16, 24), Texture));
                    VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, MathHelper.Pi, 0));
                    VertexTransformer.Offset(vertices, new Vector3(-10 + 1.25f, 5.625f, -15));
                    Geometry.AddVertices(vertices);
                }
                // right
                {
                    var vertices = RectangleComposer.Create(2.5f, 3.75f,
                        new TextureRectangle(new Rectangle(0, 176, 16, 24), Texture));
                    VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, MathHelper.Pi, 0));
                    VertexTransformer.Offset(vertices, new Vector3(10 - 1.25f, 5.625f, -15));
                    Geometry.AddVertices(vertices);
                }
            }
            // inner
            {
                // back wall
                {
                    var vertices = RectangleComposer.Create(20, 10,
                        new TextureRectangle(new Rectangle(96, 192, 48, 32), Texture));
                    VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, 0, 0));
                    VertexTransformer.Offset(vertices, new Vector3(0, 5, -9));
                    Geometry.AddVertices(vertices);
                }
            }
            // sign
            {
                // post
                {
                    var texture = new TextureCuboidWrapper();
                    texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                        new TextureRectangle(new Rectangle(96, 160, 12, 32), Texture));
                    texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                        new TextureRectangle(new Rectangle(96, 160, 12, 32), Texture));
                    texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                        new TextureRectangle(new Rectangle(96, 160, 1, 1), Texture));
                    var vertices = CuboidComposer.Create(2.5f, 18f, 3f, texture);
                    VertexTransformer.Rotate(vertices, new Vector3(-0.5f, 0, 0));
                    VertexTransformer.Offset(vertices, new Vector3(13, 6, -14));
                    Geometry.AddVertices(vertices);
                }
                // front
                {
                    var texture = new TextureCuboidWrapper();
                    texture.AddSide(new[] { CuboidSide.Back },
                        new TextureRectangle(new Rectangle(108, 160, 48, 16), Texture));
                    texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom, CuboidSide.Front },
                        new TextureRectangle(new Rectangle(108, 176, 48, 16), Texture));
                    texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                        new TextureRectangle(new Rectangle(108, 160, 1, 1), Texture));
                    var vertices = CuboidComposer.Create(20f, 4f, 1f, texture);
                    VertexTransformer.Rotate(vertices, new Vector3(0f, MathHelper.Pi, 0.1f));
                    VertexTransformer.Offset(vertices, new Vector3(2, 11f, -17.4f));
                    Geometry.AddVertices(vertices);
                }
            }
        }
    }
}
