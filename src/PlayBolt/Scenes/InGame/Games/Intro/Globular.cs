﻿using GameDevCommon.Drawing;
using GameDevCommon.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using static Core;

namespace PlayBolt.Scenes.InGame.Games.Intro
{
    [MicrogameDescription(7, "Globular", "Intro", "Look him straight in the eyes, no fake outs.")]
    class Globular : Microgame
    {
        private const float TARGET_ACCURACY = 0.925f;

        public override float CompletionTime => 4f;
        public override string Command => _reverseMode ? "No Face!" : "Show Face!";
        public override string Scoretable => "287773";
        public override int TargetScore => 15;
        public override string Thumbnail => "7.png";

        private Model _fern;
        private Matrix _world, _view, _projection;
        private float _yaw, _pitch = -MathHelper.PiOver2;
        private bool _reverseMode = false; // in reverse mode, the back of the head is the target.

        public override void GameStart()
        {
            _projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(70), WIDTH / (float)HEIGHT, 0.1f, 1000f);
            _view = Matrix.CreateLookAt(new Vector3(0, 0, 3f), Vector3.Zero, Vector3.Up);
            _world = Matrix.CreateScale(0.01f);

            var reverseChance = 0;
            switch (Difficulty)
            {
                case Difficulty.Easy:
                    if (_random.Next(0, 2) == 0)
                    {
                        _yaw += MathHelper.PiOver2 + (float)(MathHelper.Pi * _random.NextDouble());
                    }
                    else
                    {
                        _pitch += MathHelper.PiOver2 + (float)(MathHelper.Pi * _random.NextDouble());
                    }
                    reverseChance = 5;
                    break;
                case Difficulty.Medium:
                    _yaw += MathHelper.PiOver2 + (float)(MathHelper.Pi * _random.NextDouble());
                    _pitch += MathHelper.PiOver2 + (float)(MathHelper.Pi * _random.NextDouble());
                    reverseChance = 3;
                    break;
                case Difficulty.Hard:
                    if (_random.Next(0, 2) == 0)
                    {
                        _yaw += MathHelper.PiOver2 + (float)(MathHelper.Pi * _random.NextDouble());
                        _pitch += MathHelper.Pi;
                    }
                    else
                    {
                        _yaw += MathHelper.Pi;
                        _pitch += MathHelper.PiOver2 + (float)(MathHelper.Pi * _random.NextDouble());
                    }
                    reverseChance = 2;
                    break;
            }

            _reverseMode = _random.Next(0, reverseChance) == 0;
        }

        public override void LoadContent()
        {
            base.LoadContent();
            _fern = Controller.Content.Load<Model>("Models/Games/7/Fern/fern");
        }

        public override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteBatchUsage.Default);

            _batch.Draw(_texture, new Rectangle(0, 0, WIDTH, HEIGHT), null, Color.White,
                0f, Vector2.Zero, SpriteEffects.None, 1f);

            var accuracy = GetAccuracy();
            _batch.DrawRectangle(new Rectangle(WIDTH / 2 - 100, HEIGHT - 50, 200, 20), new Color(160, 160, 160));

            var barColor = accuracy >= TARGET_ACCURACY ? new Color(120, 255, 120) : new Color(120, 120, 120);
            _batch.DrawRectangle(new Rectangle(WIDTH / 2 - 99, HEIGHT - 49, (int)(198 * accuracy), 18), barColor);
            _batch.DrawRectangle(new Rectangle(WIDTH / 2 + (int)((1f - (2f - (TARGET_ACCURACY * 2f))) * 100) - 1, HEIGHT - 55, 2, 30),
                new Color(255, 100, 0));

            _batch.End();

            Controller.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            Controller.GraphicsDevice.BlendState = BlendState.Opaque;
            _fern.Draw(_world, _view, _projection);
        }

        public override void Update(GameTime gameTime)
        {
            _world = Matrix.CreateScale(0.01f)
                * Matrix.CreateRotationX(_pitch)
                * Matrix.CreateRotationY(_yaw);

            float offset = 0.05f * Speed;
            if (DirectionDown(InputDirection.Left))
            {
                _yaw -= offset;
            }
            if (DirectionDown(InputDirection.Right))
            {
                _yaw += offset;
            }
            if (DirectionDown(InputDirection.Up))
            {
                _pitch -= offset;
            }
            if (DirectionDown(InputDirection.Down))
            {
                _pitch += offset;
            }
        }

        public override void TimeUp()
        {
            if (GetAccuracy() >= TARGET_ACCURACY)
            {
                WinState = WinState.Won;
            }
            else
            {
                WinState = WinState.Lost;
            }
        }

        private float GetAccuracy()
        {
            var y = Math.Abs(MathHelper.WrapAngle(_yaw + (_reverseMode ? MathHelper.Pi : 0)));
            var p = Math.Abs(MathHelper.WrapAngle(_pitch + MathHelper.PiOver2));

            var t = (y + p) / MathHelper.TwoPi;
            t = Math.Abs(t - 0.5f) * 2f;
            return t;
        }
    }
}
