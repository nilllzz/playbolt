﻿using Microsoft.Xna.Framework;

namespace PlayBolt.Scenes.Menu
{
    abstract class SubScreen
    {
        protected static readonly Rectangle SubScreenBounds = new Rectangle(0, 0, GameController.RENDER_WIDTH - 220, GameController.RENDER_HEIGHT - 50);
        protected MenuContainerScreen _containerScreen;

        public virtual void LoadContent(MenuContainerScreen containerScreen)
        {
            _containerScreen = containerScreen;
        }

        public virtual void Activate() { }
        public abstract void Draw(GameTime gameTime);
        public abstract void Update(GameTime gameTime);
    }
}
