﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.ParkingLot
{
    class BlueCar : WorldObject
    {
        public BlueCar(WorldScreen screen, Vector3 position, float rotation)
            : base(screen, "parkinglot_bluecar", position)
        { }
        
        protected override void CreateGeometryInternal()
        {
            // engine/front
            {
                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                    new TextureRectangle(new Rectangle(0, 256, 48, 16), Texture));
                texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right },
                    new TextureRectangle(new Rectangle(0, 256, 1, 1), Texture));
                texture.AddSide(new[] { CuboidSide.Top, CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(48, 256, 48, 32), Texture));
                var vertices = CuboidComposer.Create(new[]
                {
                    new Vector3(1, 1, -1),
                    new Vector3(1, 1, -2),
                    new Vector3(-1, 1, -2),
                    new Vector3(-1, 1, -1),

                    new Vector3(1, 0, -1),
                    new Vector3(1, 0, -2),
                    new Vector3(-1, 0, -2),
                    new Vector3(-1, 0, -1),

                }, texture);
                Geometry.AddVertices(vertices);
            }
        }
    }
}
