﻿using GameDevCommon;
using GameDevCommon.Drawing;
using GameDevCommon.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PlayBolt.Content;
using System;
using static Core;

namespace PlayBolt.Scenes.Menu
{
    class MenuContainerScreen : Screen
    {
        private struct MenuEntry
        {
            public string Text;
            public Rectangle TextureRectangle;
            public Type SubScreenType;
        }

        private Texture2D _texture, _selectTexture, _actionTexture, _moveTexture, _scribbleTexture;
        private SpriteBatch _batch;
        private SpriteFont _font;
        private MenuEntry[] _menuEntries;
        private int _menuIndex = 0, _selectedMenuIndex = 0;
        private float _menuIndexSelectedIntro = 0f;
        private SubScreen _subScreen;
        private RenderTarget2D _subScreenTarget;
        private bool _subScreenActive = false;
        private float _subScreenIntro = 0f;

        public MenuContainerScreen()
        {
            _menuEntries = new[]
            {
                new MenuEntry { Text = "Featured", TextureRectangle = new Rectangle(0, 16, 18, 16), SubScreenType = typeof(FeaturedSubScreen) },
                new MenuEntry { Text = "Browse Games", TextureRectangle = new Rectangle(16, 0, 16, 16), SubScreenType = typeof(BrowseGamesSubScreen) }
            };
        }

        internal override void LoadContent()
        {
            _actionTexture = GetComponent<Resources>().LoadTexture("Textures/Menu/Action.png");
            _moveTexture = GetComponent<Resources>().LoadTexture("Textures/Menu/Move.png");
            _selectTexture = GetComponent<Resources>().LoadTexture("Textures/Menu/Select.png");
            _scribbleTexture = GetComponent<Resources>().LoadTexture("Textures/Menu/Scribble.png");
            _texture = GetComponent<Resources>().LoadTexture("Textures/MenuTiles.png");
            _batch = new SpriteBatch(Controller.GraphicsDevice);
            _font = Controller.Content.Load<SpriteFont>("Fonts/Font");
            _subScreenTarget = new RenderTarget2D(Controller.GraphicsDevice, GameController.RENDER_WIDTH - 220, GameController.RENDER_HEIGHT - 50);
        }

        internal override void Activate()
        {
            _subScreen?.Activate();
        }

        internal override void Draw(GameTime gameTime)
        {
            RenderTargetManager.BeginRenderToTarget(_subScreenTarget);
            Controller.GraphicsDevice.Clear(Color.Black);

            _subScreen?.Draw(gameTime);

            RenderTargetManager.EndRenderToTarget();

            _batch.Begin(SpriteBatchUsage.Default);
            _batch.DrawRectangle(new Rectangle(0, 0, 220, GameController.RENDER_HEIGHT), new Color(25, 25, 25));
            _batch.DrawRectangle(new Rectangle(220, 0, GameController.RENDER_WIDTH - 220, 50), new Color(25, 25, 25));

            _batch.Draw(_texture, new Rectangle(16, 16, 71, 17), new Rectangle(0, 32, 71, 17), Color.White);
            _batch.Draw(_texture, new Rectangle(92, 16, 70, 17), new Rectangle(0, 49, 70, 17), Color.White);

            for (int i = 0; i < _menuEntries.Length; i++)
            {
                var menuEntry = _menuEntries[i];
                var color = Color.White;
                if (i == _selectedMenuIndex && !_subScreenActive)
                {
                    _batch.DrawRectangle(new Rectangle(0, 90 + 50 * i, 220, 50), new Color(41, 41, 41));
                    _batch.DrawRectangle(new Rectangle(0, 90 + 50 * i, 5, 50), new Color(204, 255, 0));

                    if (_menuIndexSelectedIntro < 1f)
                    {
                        _batch.DrawRectangle(new Rectangle(5, 90 + 50 * i, 5 + (int)(210 * _menuIndexSelectedIntro), 50), new Color(204, 255, 0, 255 - (int)(255 * _menuIndexSelectedIntro)));
                    }
                }
                if (i == _menuIndex && _subScreen != null)
                {
                    color = new Color(204, 255, 0);
                }
                _batch.DrawString(_font, menuEntry.Text, new Vector2(50, 104 + 50 * i), color);
                _batch.Draw(_texture, new Rectangle(17, 107 + 50 * i, menuEntry.TextureRectangle.Width, menuEntry.TextureRectangle.Height), menuEntry.TextureRectangle, color);
            }

            if (_subScreen == null || _subScreenIntro < 1f)
            {
                _batch.DrawRectangle(new Rectangle(220, 50, _subScreenTarget.Width, _subScreenTarget.Height), Color.Black);
                _batch.Draw(_selectTexture, new Rectangle(300, 100, 256, 256), Color.White);
                _batch.Draw(_moveTexture, new Rectangle(400, 400, 256, 256), Color.White);
                _batch.Draw(_actionTexture, new Rectangle(800, 250, 256, 256), Color.White);
                _batch.Draw(_scribbleTexture, new Rectangle(650, 150, 128, 128), Color.White);
            }
            
            if (_subScreen != null)
            {
                _batch.Draw(_subScreenTarget, new Rectangle(220, 150 - (int)(100 * _subScreenIntro), _subScreenTarget.Width, _subScreenTarget.Height),
                    new Color(255, 255, 255, (int)(_subScreenIntro * 255)));

                if (!_subScreenActive)
                {
                    for (int x = 0; x < _subScreenTarget.Width; x += 32)
                    {
                        for (int y = 0; y < _subScreenTarget.Height; y += 32)
                        {
                            _batch.Draw(_texture, new Rectangle(x + 220, y + 50, 32, 32), new Rectangle(112, 112, 16, 16), Color.White);
                        }
                    }
                }
            }


            _batch.End();
        }

        internal override void Update(GameTime gameTime)
        {
            if (_subScreenActive)
            {
                _subScreen?.Update(gameTime);
            }
            else
            {
                var controls = GetComponent<ControlsHandler>();
                var keyboard = GetComponent<KeyboardHandler>();
                var gamepad = GetComponent<GamePadHandler>();
                if (keyboard.KeyPressed(Keys.Enter) || keyboard.KeyPressed(Keys.Space) ||
                    gamepad.ButtonPressed(PlayerIndex.One, Buttons.A))
                {
                    if (_selectedMenuIndex != _menuIndex || _subScreen == null)
                    {
                        _selectedMenuIndex = _menuIndex;
                        SetSubScreen();
                    }

                    _subScreenActive = true;
                }
                else
                {
                    if (controls.UpPressed(PlayerIndex.One))
                    {
                        _menuIndexSelectedIntro = 0f;
                        _selectedMenuIndex--;
                        if (_selectedMenuIndex == -1)
                            _selectedMenuIndex = _menuEntries.Length - 1;
                    }
                    if (controls.DownPressed(PlayerIndex.One))
                    {
                        _menuIndexSelectedIntro = 0f;
                        _selectedMenuIndex++;
                        if (_selectedMenuIndex == _menuEntries.Length)
                            _selectedMenuIndex = 0;
                    }
                }
            }

            if (_subScreen != null && _subScreenIntro < 1f)
            {
                _subScreenIntro = MathHelper.Lerp(_subScreenIntro, 1f, 0.2f);
                if (_subScreenIntro > 0.999f)
                    _subScreenIntro = 1f;
            }
            if (_menuIndexSelectedIntro < 1f)
            {
                _menuIndexSelectedIntro = MathHelper.Lerp(_menuIndexSelectedIntro, 1f, 0.2f);
                if (_menuIndexSelectedIntro > 0.999f)
                    _menuIndexSelectedIntro = 1f;
            }
        }

        public void ExitSubScreen()
        {
            _subScreenActive = false;
            _menuIndexSelectedIntro = 0f;
        }

        private void SetSubScreen()
        {
            var menuEntry = _menuEntries[_menuIndex];
            var subScreen = (SubScreen)Activator.CreateInstance(menuEntry.SubScreenType);
            subScreen.LoadContent(this);
            _subScreen = subScreen;
            _subScreen.Activate();
        }
    }
}
