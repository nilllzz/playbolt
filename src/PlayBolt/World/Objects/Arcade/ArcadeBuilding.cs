﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Arcade
{
    class ArcadeBuilding : WorldObject
    {
        public ArcadeBuilding(WorldScreen screen, Vector3 position)
            : base(screen, "arcade_building", position)
        { }

        protected override void CreateGeometryInternal()
        {
            // left back wall
            {
                var vertices = RectangleComposer.Create(new[]
                {
                    new Vector3(0, 5, -24),
                    new Vector3(-4, 5, -20),
                    new Vector3(0, -5, -24),
                    new Vector3(-4, -5, -20),
                }, new TextureRectangle(new Rectangle(640, 64, 32, 64), Texture));
                Geometry.AddVertices(vertices);
            }
            // left front
            {
                var vertices = RectangleComposer.Create(12, 10,
                    new TextureRectangle(new Rectangle(624, 0, 80, 64), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, -MathHelper.PiOver2, 0));
                VertexTransformer.Offset(vertices, new Vector3(-4, 0, -14));
                Geometry.AddVertices(vertices);
            }
            // left to door
            {
                var vertices = RectangleComposer.Create(new[]
                {
                    new Vector3(-4, 5, -8),
                    new Vector3(0, 5, -3.5f),
                    new Vector3(-4, -5, -8),
                    new Vector3(0, -5, -3.5f)
                }, new TextureRectangle(new Rectangle(640, 64, 48, 64), Texture));
                Geometry.AddVertices(vertices);
            }
            // roof left
            {
                var vertices = RectangleComposer.Create(new[]
                {
                    new Vector3(-4, 5, -20),
                    new Vector3(0, 5, -24),
                    new Vector3(-4, 5, -8),
                    new Vector3(0, 5, -3.5f),
                }, new TextureRectangle(new Rectangle(640, 64, 1, 1), Texture));
                Geometry.AddVertices(vertices);
            }
            // left wall
            {
                var vertices = RectangleComposer.Create(7, 10,
                    new TextureRectangle(new Rectangle(640, 64, 48, 64), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, MathHelper.Pi, 0));
                VertexTransformer.Offset(vertices, new Vector3(3.5f, 0, -24));
                Geometry.AddVertices(vertices);
                VertexTransformer.Offset(vertices, new Vector3(7, 0, 0));
                Geometry.AddVertices(vertices);
            }
            // door
            {
                var vertices = RectangleComposer.Create(7, 10,
                    new TextureRectangle(new Rectangle(592, 64, 48, 64), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, -MathHelper.PiOver2, 0));
                Geometry.AddVertices(vertices);
            }
            // right to door
            {
                var vertices = RectangleComposer.Create(new[]
                {
                    new Vector3(0, 5, 3.5f),
                    new Vector3(-4, 5, 8),
                    new Vector3(0, -5, 3.5f),
                    new Vector3(-4, -5, 8),
                }, new TextureRectangle(new Rectangle(640, 64, 48, 64), Texture));
                Geometry.AddVertices(vertices);
            }
            // right front
            {
                var vertices = RectangleComposer.Create(12, 10,
                    new TextureRectangle(new Rectangle(624, 0, 80, 64), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, -MathHelper.PiOver2, 0));
                VertexTransformer.Offset(vertices, new Vector3(-4, 0, 14));
                Geometry.AddVertices(vertices);
            }
            // right back wall
            {
                var vertices = RectangleComposer.Create(new[]
                {
                    new Vector3(-4, 5, 20),
                    new Vector3(0, 5, 24),
                    new Vector3(-4, -5, 20),
                    new Vector3(0, -5, 24),
                }, new TextureRectangle(new Rectangle(640, 64, 32, 64), Texture));
                Geometry.AddVertices(vertices);
            }
            // roof right
            {
                var vertices = RectangleComposer.Create(new[]
                {
                    new Vector3(-4, 5, 20),
                    new Vector3(0, 5, 24),
                    new Vector3(-4, 5, 8),
                    new Vector3(0, 5, 3.5f),
                }, new TextureRectangle(new Rectangle(640, 64, 1, 1), Texture));
                Geometry.AddVertices(vertices);
            }
            // right wall
            {
                var vertices = RectangleComposer.Create(7, 10,
                    new TextureRectangle(new Rectangle(640, 64, 48, 64), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, 0, 0));
                VertexTransformer.Offset(vertices, new Vector3(3.5f, 0, 24));
                Geometry.AddVertices(vertices);
                VertexTransformer.Offset(vertices, new Vector3(7, 0, 0));
                Geometry.AddVertices(vertices);
            }
            // back wall
            {
                var vertices = RectangleComposer.Create(48, 10,
                    new TextureRectangle(new Rectangle(640, 64, 1, 1), Texture));
                VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, MathHelper.PiOver2, 0));
                VertexTransformer.Offset(vertices, new Vector3(14, 0, 0));
                Geometry.AddVertices(vertices);
            }
            // ceiling
            {
                var vertices = RectangleComposer.Create(14, 48,
                    new TextureRectangle(new Rectangle(688, 64, 32, 96), Texture));
                VertexTransformer.Offset(vertices, new Vector3(7, 5, 0));
                Geometry.AddVertices(vertices);
            }
        }
    }
}
