﻿using GameDevCommon;
using GameDevCommon.Drawing;
using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayBolt.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using static Core;

namespace PlayBolt.World.Objects.Arcade
{
    class BloxGame : WorldObject
    {
        private static Random _random = new Random();
        private const int FIELD_WIDTH = 12, FIELD_HEIGHT = 16;

        private SpriteFont _font;
        private Texture2D _texture;
        private SpriteBatch _batch;
        private RenderTarget2D _target;

        private class Block
        {
            public int X, Y;
            public Color Color;
        }

        private List<Block> _blocks = new List<Block>();
        private List<Block> _fallingBlocks = new List<Block>();
        private int _tickDelay = 10;
        private bool _gameOver = false;

        public BloxGame(WorldScreen screen, Vector3 position)
            : base(screen, "arcade_blox", position)
        {
            IsOptimizable = false;
        }

        public override void LoadContent()
        {
            _font = Controller.Content.Load<SpriteFont>("Fonts/Font");
            _texture = GetComponent<Resources>().LoadTexture("Textures/World/main.png");
            _target = RenderTargetManager.CreateRenderTarget(FIELD_WIDTH * 8 + 16, FIELD_HEIGHT * 8 + 8);
            _batch = new SpriteBatch(Controller.GraphicsDevice);

            LoadTexture(new Color(170, 170, 170));
        }

        public override void Update()
        {
            if (_cameraDistance < 100f)
            {
                // update
                if (_fallingBlocks.Count > 0 && _random.Next(0, 15) == 0 && !_gameOver)
                {
                    var xOffset = _random.Next(-1, 2);
                    if (_fallingBlocks.Min(b => b.X + xOffset) >= 0 &&
                        _fallingBlocks.Max(b => b.X + xOffset) < FIELD_WIDTH &&
                        !_fallingBlocks.Any(fb => _blocks.Any(b => b.X == fb.X + xOffset && fb.Y == b.Y)))
                    {
                        foreach (var block in _fallingBlocks)
                            block.X += xOffset;
                    }
                }

                _tickDelay--;
                if (_tickDelay == 0)
                {
                    // game tick
                    _blocks = _blocks.OrderByDescending(b => b.Y).ToList();
                    if (_gameOver)
                    {
                        _tickDelay = 5;
                        if (_blocks.Count > 0)
                            _blocks.RemoveAt(0);
                        else
                            _gameOver = false;
                    }
                    else
                    {
                        _tickDelay = 10;
                        // gravity:
                        if (_fallingBlocks.Count > 0)
                        {
                            if (_fallingBlocks.Any(fb => fb.Y == FIELD_HEIGHT - 1 || _blocks.Any(b => b.X == fb.X && b.Y == fb.Y + 1)))
                            {
                                if (_fallingBlocks.Any(b => b.Y < 0))
                                {
                                    _gameOver = true;
                                }
                                _blocks.AddRange(_fallingBlocks);
                                _fallingBlocks.Clear();
                                if (!_gameOver)
                                {
                                    SpawnBlocks();
                                }
                            }
                            else
                            {
                                foreach (var block in _fallingBlocks)
                                {
                                    block.Y++;
                                }
                            }
                        }
                        else
                        {
                            SpawnBlocks();
                        }
                    }
                }

                // draw
                RenderTargetManager.BeginRenderToTarget(_target);
                Controller.GraphicsDevice.Clear(Color.Transparent);
                _batch.Begin(SpriteBatchUsage.Default);

                for (int y = 0; y < FIELD_HEIGHT + 2; y++)
                {
                    for (int x = 0; x < FIELD_WIDTH + 2; x++)
                    {
                        if (x == 0 || x == FIELD_WIDTH + 1 || y == FIELD_HEIGHT)
                        {
                            _batch.Draw(_texture, new Rectangle(x * 8, y * 8, 8, 8), new Rectangle(576, 0, 8, 8), Color.White);
                        }
                        else
                        {
                            _batch.Draw(_texture, new Rectangle(x * 8, y * 8, 8, 8), new Rectangle(576, 0, 8, 8), new Color(120, 120, 120));
                        }
                    }
                }

                foreach (var block in _blocks)
                {
                    _batch.Draw(_texture, new Rectangle(block.X * 8 + 8, block.Y * 8, 8, 8), new Rectangle(576, 0, 8, 8), block.Color);
                }
                foreach (var block in _fallingBlocks)
                {
                    _batch.Draw(_texture, new Rectangle(block.X * 8 + 8, block.Y * 8, 8, 8), new Rectangle(576, 0, 8, 8), block.Color);
                }

                if (_gameOver)
                {
                    _batch.DrawString(_font, "GAME OVER", new Vector2(8, 60), Color.Red);
                }
                
                _batch.End();
                RenderTargetManager.EndRenderToTarget();
                Texture = _target;
            }
            base.Update();
        }

        private void SpawnBlocks()
        {
            switch (_random.Next(0, 7))
            {
                // block
                case 0:
                    AddBlocks(Color.LightBlue,
                        new Point(4, -1),
                        new Point(5, -1),
                        new Point(4, -2),
                        new Point(5, -2));
                    break;
                // long
                case 1:
                    switch (_random.Next(0, 2))
                    {
                        // vertical
                        case 0:
                            AddBlocks(Color.LightYellow,
                                new Point(5, -1),
                                new Point(5, -2),
                                new Point(5, -3),
                                new Point(5, -4));
                            break;
                        // horizontal
                        case 1:
                            AddBlocks(Color.LightYellow,
                                new Point(4, -1),
                                new Point(5, -1),
                                new Point(6, -1),
                                new Point(7, -1));
                            break;
                    }
                    break;
                // t
                case 2:
                    switch (_random.Next(0, 4))
                    {
                        case 0:
                            AddBlocks(Color.LightPink,
                                new Point(4, -1),
                                new Point(5, -1),
                                new Point(6, -1),
                                new Point(5, -2));
                            break;
                        case 1:
                            AddBlocks(Color.LightPink,
                                new Point(4, -2),
                                new Point(5, -2),
                                new Point(6, -2),
                                new Point(5, -1));
                            break;
                        case 2:
                            AddBlocks(Color.LightPink,
                                new Point(4, -2),
                                new Point(5, -3),
                                new Point(5, -2),
                                new Point(5, -1));
                            break;
                        case 3:
                            AddBlocks(Color.LightPink,
                                new Point(6, -2),
                                new Point(5, -3),
                                new Point(5, -2),
                                new Point(5, -1));
                            break;
                    }
                    break;
                // step 1
                case 3:
                    switch (_random.Next(0, 2))
                    {
                        // vertical
                        case 0:
                            AddBlocks(Color.LightGreen,
                                new Point(4, -3),
                                new Point(4, -2),
                                new Point(5, -2),
                                new Point(5, -1));
                            break;
                        // horizontal
                        case 1:
                            AddBlocks(Color.LightGreen,
                                new Point(4, -1),
                                new Point(5, -1),
                                new Point(5, -2),
                                new Point(6, -2));
                            break;
                    }
                    break;
                // step 2
                case 4:
                    switch (_random.Next(0, 2))
                    {
                        // vertical
                        case 0:
                            AddBlocks(new Color(255, 128, 255),
                                new Point(4, -1),
                                new Point(4, -2),
                                new Point(5, -2),
                                new Point(5, -3));
                            break;
                        // horizontal
                        case 1:
                            AddBlocks(new Color(255, 128, 255),
                                new Point(4, -2),
                                new Point(5, -2),
                                new Point(5, -1),
                                new Point(6, -1));
                            break;
                    }
                    break;
                // l1
                case 5:
                    switch (_random.Next(0, 4))
                    {
                        case 0:
                            AddBlocks(Color.Orange,
                                new Point(4, -2),
                                new Point(4, -1),
                                new Point(5, -1),
                                new Point(6, -1));
                            break;
                        case 1:
                            AddBlocks(Color.Orange,
                                new Point(6, -3),
                                new Point(5, -3),
                                new Point(5, -2),
                                new Point(5, -1));
                            break;
                        case 2:
                            AddBlocks(Color.Orange,
                                new Point(4, -2),
                                new Point(5, -2),
                                new Point(6, -2),
                                new Point(6, -1));
                            break;
                        case 3:
                            AddBlocks(Color.Orange,
                                new Point(5, -1),
                                new Point(6, -1),
                                new Point(6, -2),
                                new Point(6, -3));
                            break;
                    }
                    break;
                // l2
                case 6:
                    switch (_random.Next(0, 4))
                    {
                        case 0:
                            AddBlocks(new Color(130, 130, 255),
                                new Point(6, -2),
                                new Point(4, -1),
                                new Point(5, -1),
                                new Point(6, -1));
                            break;
                        case 1:
                            AddBlocks(new Color(130, 130, 255),
                                new Point(5, -3),
                                new Point(6, -3),
                                new Point(6, -2),
                                new Point(6, -1));
                            break;
                        case 2:
                            AddBlocks(new Color(130, 130, 255),
                                new Point(6, -2),
                                new Point(5, -2),
                                new Point(4, -2),
                                new Point(4, -1));
                            break;
                        case 3:
                            AddBlocks(new Color(130, 130, 255),
                                new Point(6, -1),
                                new Point(5, -1),
                                new Point(5, -2),
                                new Point(5, -3));
                            break;
                    }
                    break;
            }
        }

        private void AddBlocks(Color color, params Point[] positions)
        {
            _fallingBlocks.AddRange(positions.Select(p => new Block
            {
                Color = color,
                X = p.X,
                Y = p.Y
            }));
        }

        protected override void CreateGeometryInternal()
        {
            var vertices = RectangleComposer.Create(6.4f, 9.6f, new TextureRectangle(0, 0, 1, 1));
            VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, -MathHelper.PiOver2, 0));
            Geometry.AddVertices(vertices);
        }
    }
}
