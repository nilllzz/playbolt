﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.Gift
{
    class SpinningLogo : WorldObject
    {
        public SpinningLogo(WorldScreen screen, Vector3 position)
            : base(screen, "gift_spinningLogo", position)
        {
            IsOptimizable = false;
        }

        public override void Update()
        {
            _rotation.Y -= 0.015f;
            CreateWorld();
            base.Update();
        }

        private struct PixelLine
        {
            public int startX;
            public int length;
        }

        protected override void CreateGeometryInternal()
        {
            var lines = new[]
            {
                new PixelLine { startX = 4, length = 14 },
                new PixelLine { startX = 4, length = 14 },
                new PixelLine { startX = 3, length = 15 },
                new PixelLine { startX = 3, length = 14 },
                new PixelLine { startX = 2, length = 14 },
                new PixelLine { startX = 2, length = 15 },
                new PixelLine { startX = 1, length = 16 },
                new PixelLine { startX = 1, length = 16 },
                new PixelLine { startX = 0, length = 16 },
                new PixelLine { startX = 0, length = 15 },
                new PixelLine { startX = 0, length = 14 },
                new PixelLine { startX = 0, length = 13 },
                new PixelLine { startX = 5, length = 7 },
                new PixelLine { startX = 5, length = 6 },
                new PixelLine { startX = 4, length = 6 },
                new PixelLine { startX = 4, length = 5 },
                new PixelLine { startX = 3, length = 5 },
                new PixelLine { startX = 3, length = 4 },
                new PixelLine { startX = 3, length = 3 },
            };

            for (int y = 0; y < 19; y++)
            {
                var line = lines[y];

                var texture = new TextureCuboidWrapper();
                texture.AddSide(new[] { CuboidSide.Left, CuboidSide.Right, CuboidSide.Top, CuboidSide.Bottom },
                    new TextureRectangle(new Rectangle(112 + line.startX, 122 + y, 1, 1), Texture));
                texture.AddSide(new[] { CuboidSide.Front, CuboidSide.Back },
                    new TextureRectangle(new Rectangle(112 + line.startX, 122 + y, line.length, 1), Texture));

                var vertices = CuboidComposer.Create(line.length, 1, 1, texture);
                VertexTransformer.Offset(vertices, new Vector3(line.length / 2f + line.startX - 4.5f, 9.5f-y, 0));
                VertexTransformer.Scale(vertices, new Vector3(0.5f));
                Geometry.AddVertices(vertices);
            }
        }
    }
}
