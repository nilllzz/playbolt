﻿using GameDevCommon.Rendering;
using GameDevCommon.Rendering.Composers;
using GameDevCommon.Rendering.Texture;
using Microsoft.Xna.Framework;

namespace PlayBolt.World.Objects.MotionSim
{
    class MotionSimBackPanel : WorldObject
    {
        public MotionSimBackPanel(WorldScreen screen, Vector3 position)
            : base(screen, "motionsim_backpanel", position)
        {
            IsOpaque = false;
        }

        protected override void CreateGeometryInternal()
        {
            var vertices = RectangleComposer.Create(32, 24,
                new TextureRectangle(new Rectangle(504, 0, 64, 48), Texture));
            VertexTransformer.Rotate(vertices, new Vector3(MathHelper.PiOver2, -MathHelper.PiOver2, 0));
            Geometry.AddVertices(vertices);
        }
    }
}
